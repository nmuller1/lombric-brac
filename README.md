Format fichier map:

    (1) #lignes #colonnes
    (2  matrice lignes x colonnes avec les caractères ' '=AIR, '#'=DIRT, '='=ROCK.
    ...
    #lignes+1)

Controls:
    q = quitter le programme
    Flèches = déplacer le curseur dans la fenêtre de jeu.
    ijkl = bouger la zone affichée de la map (pour des map plus grandes que la zone d'affichage).