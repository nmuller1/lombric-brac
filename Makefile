CC = g++
########### Flags ###########
CFLAGS = -std=c++11 -Wall -Wextra -Wpedantic #todo remettre Werror à un moment
LDFLAGS = -lncurses
SQLFLAGS = -pthread -lsqlite3
########### Source  Files ###########
SRC = source
GameSrc = $(SRC)/gameSource
SrvSrc = $(SRC)/srvSource
CliSrc = $(SRC)/clientSource
UtiSrc = $(SRC)/utilsSource
DBSrc = $(SRC)/dBSource
########### Object Files ###########
OBJ = objfiles
GameObjFiles = $(OBJ)/Cell.o $(OBJ)/Crate.o $(OBJ)/Database.o $(OBJ)/Game.o $(OBJ)/GunCrate.o $(OBJ)/Inventory.o $(OBJ)/InventorySlot.o $(OBJ)/Map.o $(OBJ)/Medkit.o $(OBJ)/Object.o $(OBJ)/Physics.o $(OBJ)/Player.o $(OBJ)/Projectile.o $(OBJ)/Ranking.o $(OBJ)/Renderer.o $(OBJ)/StraightWeapon.o $(OBJ)/Team.o $(OBJ)/ThrownWeapon.o $(OBJ)/Utility.o $(OBJ)/Vector2d.o $(OBJ)/Worm.o
SrvObjFiles = $(OBJ)/ProtocolServer.o $(OBJ)/DataExchange.o $(OBJ)/Interface.o
ClientObjFiles = $(OBJ)/DataExchange.o $(OBJ)/Interface.o $(OBJ)/ProtocolClient.o
########### Prerequisites ###########
SrvPrq = DataExchange ProtocolServer Interface
ClientPrq = DataExchange Interface ProtocolClient
GamePrq = Cell Crate Game GunCrate Inventory InventorySlot Map Medkit Object Physics Player Projectile Ranking Renderer StraightWeapon Team ThrownWeapon Utility Vector2d Worm Database
# Aloïs : Je propose de bien mettre les fichiers dans des variables pour rendre cela plus lisible.
# Mettons les aussi par ordre alphabétique, ce sera plus simple pour les retrouver


all: createOBJ Client Server

####################### Partie fonctionnalités #######################

createOBJ:
	[ -d $(OBJ) ] || mkdir $(OBJ)

Cell:
	$(CC) $(CFLAGS) -c $(GameSrc)/Cell.cpp -o $(OBJ)/Cell.o

Crate:
	$(CC) $(CFLAGS) -c $(GameSrc)/Crate.cpp -o $(OBJ)/Crate.o

Game:
	$(CC) $(CFLAGS) -c $(GameSrc)/Game.cpp -o $(OBJ)/Game.o

GunCrate:
	$(CC) $(CFLAGS) -c $(GameSrc)/GunCrate.cpp -o $(OBJ)/GunCrate.o

Inventory:
	$(CC) $(CFLAGS) -c $(GameSrc)/Inventory.cpp -o $(OBJ)/Inventory.o

InventorySlot:
	$(CC) $(CFLAGS) -c $(GameSrc)/InventorySlot.cpp -o $(OBJ)/InventorySlot.o

Map:
	$(CC) $(CFLAGS) -c $(GameSrc)/Map.cpp -o $(OBJ)/Map.o

Medkit:
	$(CC) $(CFLAGS) -c $(GameSrc)/Medkit.cpp -o $(OBJ)/Medkit.o

Object:
	$(CC) $(CFLAGS) -c $(GameSrc)/Object.cpp -o $(OBJ)/Object.o

Physics:
	$(CC) $(CFLAGS) -c $(GameSrc)/Physics.cpp -o $(OBJ)/Physics.o

Player:
	$(CC) $(CFLAGS) -c $(GameSrc)/Player.cpp -o $(OBJ)/Player.o

Projectile:
	$(CC) $(CFLAGS) -c $(GameSrc)/Projectile.cpp -o $(OBJ)/Projectile.o

StraightWeapon:
	$(CC) $(CFLAGS) -c $(GameSrc)/StraightWeapon.cpp -o $(OBJ)/StraightWeapon.o

ThrownWeapon:
	$(CC) $(CFLAGS) -c $(GameSrc)/ThrownWeapon.cpp -o $(OBJ)/ThrownWeapon.o

Utility:
	$(CC) $(CFLAGS) -c $(GameSrc)/Utility.cpp -o $(OBJ)/Utility.o

Weapon:
	$(CC) $(CFLAGS) -c $(GameSrc)/Weapon.cpp -o $(OBJ)/Weapon.o

Worm:
	$(CC) $(CFLAGS) -c $(GameSrc)/Worm.cpp -o $(OBJ)/Worm.o

Main:
	$(CC) $(CFLAGS) -c $(SRC)/Main.cpp -o $(OBJ)/Main.o

#lombric: $(OBJFILES)
#	$(CC) $(CFLAGS) $(OBJFILES) -o lombric $(LDFLAGS)


####################### PARTIE UTILITAIRES #########################


Interface:
	$(CC) $(CFLAGS) -c $(UtiSrc)/Interface.cpp -o $(OBJ)/Interface.o

Ranking:
	$(CC) $(CFLAGS) -c $(UtiSrc)/Ranking.cpp -o $(OBJ)/Ranking.o

Renderer:
	$(CC) $(CFLAGS) -c $(UtiSrc)/Renderer.cpp -o $(OBJ)/Renderer.o

Team:
	$(CC) $(CFLAGS) -c $(UtiSrc)/Team.cpp -o $(OBJ)/Team.o

Vector2d:
	$(CC) $(CFLAGS) -c $(UtiSrc)/Vector2d.cpp -o $(OBJ)/Vector2d.o

DataExchange:
	$(CC) $(CFLAGS) -c $(UtiSrc)/DataExchange.c -o $(OBJ)/DataExchange.o


####################### PARTIE CLIENT #########################


Client: $(ClientPrq) $(GamePrq)
	$(CC) $(CFLAGS) -o Client $(CliSrc)/Client.cpp $(ClientObjFiles) $(GameObjFiles) -pthread -lncurses -lsqlite3

ProtocolClient :
	$(CC) $(CFLAGS) -c $(CliSrc)/ProtocolClient.cpp -o $(OBJ)/ProtocolClient.o -lncurses

####################### PARTIE SERVEUR #########################


Server: $(SrvPrq) $(GamePrq)
	$(CC) $(CFLAGS) -o Server $(SrvSrc)/Server.cpp $(SrvObjFiles) $(GameObjFiles) $(SQLFLAGS) -lncurses

ProtocolServer :
	$(CC) $(CFLAGS) -c $(SrvSrc)/ProtocolServer.cpp -o $(OBJ)/ProtocolServer.o -lsqlite3

####################### PARTIE DB #########################

Database:
	$(CC) $(CFLAGS) $(SQLFLAGS) -c $(DBSrc)/Database.cpp -o $(OBJ)/Database.o

####################### PARTIE Mr Propre #########################

clear:
	rm $(OBJ)/*.o
