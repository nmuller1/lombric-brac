#ifndef GROUPE_8_PROTOCOL_CLIENT_H
#define GROUPE_8_PROTOCOL_CLIENT_H


#include "../utilsSource/DataExchange.h"
#include "../gameSource/Game.h"
#include "../utilsSource/Interface.h"
#include "../utilsSource/CONST.h"
#include "../utilsSource/DataExchange.h"
#include "../gameSource/GunCrate.h"
#include "../gameSource/Medkit.h"
#include "../gameSource/StraightWeapon.h"
#include "../gameSource/ThrownWeapon.h"
#include "../dBSource/Database.h"
#include <vector>

typedef struct{
    char username[USERNAME_SIZE]{};
    //char *buffer = (char *) malloc(TAILLE_BUFFER * sizeof(buffer));
    char *buffer{};
    int taille_buffer = INIT_TAILLE ;
}MessageData;


int connectServ(Interface inter, int socket_cible, char client_name[]);

int registerServ(Interface inter, int socket_cible);

int playGame(Interface inter, int socket_cible, char client_name[]);

//int joinGame(Interface inter, int socket_cible);

int joinGame(Interface inter, int socket_cible, char client_name[]);

int createGame(Interface inter, int socket_cible);

int askAccess(Interface inter, int socket_cible);

int sendRankingFilters(Interface inter, int socket_cible);

int recvRanking(Interface inter, int socket_cible);

int gameIsRunningWhatYouGonnaDo(int socket_cible, Interface inter);

int gameIsRunningWhatYouGonnaDo(int socket_cible, Interface inter, bool replayOn);

void recvShootInfo(int &x, int &y, int &modifier, char weaponType[3], int socket_cible);

void spawnCrate(Game *game, int socket_cible, int medkitHP);

int storeMemory(char* message, MessageData* messageHistory, int nbOfFriends);

int getFriendIndex(char username[USERNAME_SIZE], MessageData* messageHistory, int nbOfFriends);

int recvFriendList(Interface inter, int local_socket);

int recvFriendRequestList(Interface inter, int local_socket);

#endif //GROUPE_8_PROTOCOL_CLIENT_H
