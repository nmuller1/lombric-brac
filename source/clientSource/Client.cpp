//TODO: ajouté un écran "vous avez été déconnecté du serveur" quand ça arrive.
//FIXME: signaler la déconnexion sur un ctrl+c dans le terminal client (plutot urgent)

#define _DEFAULT_SOURCE

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <zconf.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <pthread.h>
#include "ProtocolClient.h"

typedef struct {

    int socket_client;
    Interface *inter;
    pthread_mutex_t *mutex_chat;
    //char sender[USERNAME_SIZE];
    char receiver[USERNAME_SIZE];
} writing_thread_data;

typedef struct {

    int socket_client;
    Interface *inter;
    pthread_mutex_t *mutex_chat;
} listening_thread_data;


void *writingThread(void *data) {

    writing_thread_data pdata = *((writing_thread_data *) data);
    int local_socket_chat = pdata.socket_client;
    Interface *inter = pdata.inter;
    pthread_mutex_t *mutex_chat = pdata.mutex_chat;

    pthread_mutex_lock(mutex_chat);
    char buff[] = "Vous pouvez ecrire un message ('/quit' pour terminer la conversation)";
    inter->testPrint(buff);
    pthread_mutex_unlock(mutex_chat);
    char receiver[USERNAME_SIZE];
    strcpy(receiver, pdata.receiver);

    int res;
    char str_buffer[INIT_TAILLE];


    while (1) {


        //input message
        inter->getInput(str_buffer);
        if (!strcmp(str_buffer, "/quit")) pthread_exit(nullptr);

        res = sendData(receiver, local_socket_chat);
        if (res == -1) {
            printf("Erreur à l'envoi du message.\n");
            break;
        }
        res = sendData(str_buffer, local_socket_chat);
        if (res == -1) {
            printf("Erreur à l'envoi du message.\n");
            break;
        }
    }

    close(local_socket_chat);
    pthread_exit(nullptr);
    return nullptr;
}

void *listeningThread(void *data) {

    int res;
    listening_thread_data pdata = *((listening_thread_data *) data);
    int local_socket_chat = pdata.socket_client;
    Interface *inter = pdata.inter;
    pthread_mutex_t *mutex_chat = pdata.mutex_chat;


    errno = 0;
    char str_buffer[INIT_TAILLE];
    inter->clearWin();
    pthread_mutex_lock(mutex_chat);
    strcpy(str_buffer, "Listening thread activated");
    inter->testPrint(str_buffer);
    pthread_mutex_unlock(mutex_chat);
    while (1) {

        res = recvData(str_buffer, local_socket_chat);
        if (res == -1) {
            printf("Erreur à la reception du message.\n");
            break;
        }


        pthread_mutex_lock(mutex_chat);
        inter->testPrint(str_buffer);
        pthread_mutex_unlock(mutex_chat);

    }
    close(local_socket_chat);
    pthread_exit(nullptr);
    return nullptr;
}


int main(int argc, char **argv) {


    // ====================== Déclaration de toutes nos variables =================================

    struct sockaddr_in adresse_client{};
    struct sockaddr_in adresse_serveur{};

    int res;
    int port;
    int local_socket;


    // ========================== initialisation des structures ============================
    memset(&adresse_client, 0, sizeof(struct sockaddr_in));
    memset(&adresse_serveur, 0, sizeof(struct sockaddr_in));

    // ========================== Processing des paramètres ==============================

    // test du nombre de paramètre (2 paramètres effectifs)
    //    - argv[1] est l'ip serveur
    //    - argv[2] est le port du serveur
    if (argc != 3) {
        fprintf(stderr, "Paramètres nécessaire: IP, port.\n");
        return EXIT_FAILURE;
    }

    // ~~ Adresse ip ~~
    res = inet_aton(argv[1], &adresse_serveur.sin_addr);
    if (!res) {
        fprintf(stderr, "Impossible de convertir l'adresse <%s>\n", argv[1]);
        return EXIT_FAILURE;
    }

    // ~~ Port ~~
    errno = 0;
    port = (int) strtol(argv[2], nullptr, 10);
    if (errno != 0 && port == 0) {
        perror("Impossible de convertir le port :");
        return EXIT_FAILURE;
    }


    adresse_serveur.sin_port = htons(port);
    adresse_serveur.sin_family = AF_INET;


    // ===================================== Préparation socket local ====================================

    // 1. Création socket
    errno = 0;
    local_socket = socket(PF_INET, SOCK_STREAM, 0);
    if (local_socket == -1) {
        perror("Impossible d'ouvrir le socket: ");
        return EXIT_FAILURE;
    }


    // 2. Préparation de la structure d'adresse de socket locale
    adresse_client.sin_family = AF_INET;
    adresse_client.sin_port = htons(0);
    adresse_client.sin_addr.s_addr = htonl(INADDR_ANY);

    // 3. Lien entre le descripteur de fichier et la structure
    errno = 0;
    res = bind(local_socket, (struct sockaddr *) &adresse_client, sizeof(struct sockaddr_in));
    if (res == -1) {
        perror("Impossible de lier le socket et la structure d'adresse: ");
        close(local_socket);   // On ferme le socket avant de quitter
        return EXIT_FAILURE;
    }

    // =============================== Connexion ===========================================

    Interface inter;
    char buff[INIT_TAILLE];

    errno = 0;
    res = connect(local_socket, (struct sockaddr *) &adresse_serveur, sizeof(struct sockaddr_in));
    if (res == -1) {
        perror("Impossible de se connecter au serveur.\n");
        close(local_socket);
        return EXIT_FAILURE;
    }

    strcpy(buff, "connected");
    inter.testPrint(buff);



// ============================= Envoi du mode de transmission ========================



    while (true) {
        short unsigned mode;
        char str_mode[10];
        bool notDone = true;
        char client_name[TAILLE_BUFFER];
        while (notDone) {
            mode = inter.welcomeMenu(); // 1 pour login, 2 pour register, 3 pour déco

            // envoi du mode au serveur
            sprintf(str_mode, "%d", mode);
            res = sendData(str_mode, local_socket);
            if (res == -1) {
                close(local_socket);
                return EXIT_FAILURE;
            }
            if (mode == 1) {
                if ((res = connectServ(inter, local_socket, client_name)) != -25) {
                    if (res == -1) {
                        close(local_socket);
                        return EXIT_FAILURE;
                    }

                    res = askAccess(inter, local_socket);
                    if (res == -1) {
                        close(local_socket);
                        return EXIT_FAILURE;
                    }
                    if (res == 0) {
                        inter.ConnectionError(); // signifie au client qu'il est refusé
                        close(local_socket);
                        return EXIT_FAILURE;
                    }
                    notDone = false;
                }


            }
            if (mode == 2) {
                // Dans le register, il n'y a pas de demande particulière du serveur, si l'utilisateur se trompe bah c'est son problème TODO: améliorer ça
                // enregistrement du compte dans la base de donnée
                if ((res = registerServ(inter, local_socket)) != -25) {
                    if (res == -1) {
                        close(local_socket);
                        return EXIT_FAILURE;
                    }
                    // connexion au jeu
                    res = connectServ(inter, local_socket, client_name);
                    if (res == -1) {
                        close(local_socket);
                        return EXIT_FAILURE;
                    }

                    res = askAccess(inter, local_socket);
                    if (res == -1) {
                        printf("Erreur lors de askAccess.\n");
                        close(local_socket);
                        return EXIT_FAILURE;
                    }
                    if (res == 0) {
                        close(local_socket);
                        return EXIT_FAILURE;
                    }
                    notDone = false;
                }
            }


            if (mode == 3) {
                close(local_socket);
                return EXIT_SUCCESS;
            }
        }
        // =====================================================
        //
        //     ON EST MAINTENANT DANS LE MENU PRINCIPAL
        //
        // =====================================================

        bool disconnect = false;
        while (not disconnect) {

            mode = inter.principalMenu();

            // envoi du mode au serveur
            sprintf(str_mode, "%d", mode);
            res = sendData(str_mode, local_socket);
            if (res == -1) {
                close(local_socket);
                return EXIT_FAILURE;
            }

            if (mode == 1) { // JOUER UNE PARTIE
                res = playGame(inter, local_socket, client_name);
                if (res == -1) {
                    close(local_socket);
                    return EXIT_FAILURE;
                }
            }
            if (mode == 2) { // CONSULTER CLASSEMENT

                /*

                        // ETAPE 1 : envoyer la liste des filtres à appliquer




                        res = sendRankingFilters(inter, local_socket);
                        if (res == -1){
                            printf("Impossible d'envoyer les filtres de classement voulus.\n");
                            close(local_socket);
                            return EXIT_FAILURE;
                        }
        */
                //TODO: le ranking ça me fait chier
                res = recvRanking(inter, local_socket);
                if (res == -1) {
                    printf("Imposssible de recevoir le classement\n");
                    close(local_socket);
                    return EXIT_FAILURE;
                }

            }
            if (mode == 3) { // CONSULTER PROFIL //TODO: laisser l'oportunité à l'utilisateur de quitter à tout moment
                char str_buffer[TAILLE_BUFFER];
                char username[USERNAME_SIZE];

                bool error = false;
                while (1) {
                    inter.searchPlayer(username, error);
                    res = sendData(username, local_socket);
                    if (res == -1) {
                        printf("Impossible d'envoyer le pseudo dont on recherche le profil.\n");
                        close(local_socket);
                        return EXIT_FAILURE;
                    }

                    // réponse : est-ce que le pseudo existe ?
                    res = recvData(str_buffer, local_socket);
                    if (res == -1) {
                        printf("Dans la consultation de profil : impossible de recevoir la réponse sur l'existance du pseudo.\n");
                        close(local_socket);
                        return EXIT_FAILURE;
                    }
                    // PSEUDO EXISTE : on quitte le while
                    if (!strcmp(str_buffer, "1")) break;
                        // PSEUDO N'EXISTE PAS : keep going
                    else error = true;

                }

                char worms[8][USERNAME_SIZE];
                for (int i = 0; i < 8; i++) {
                    res = recvData(worms[i], local_socket);
                    if (res == -1) {
                        printf("Impossible de recevoir le nom des vers du profil qu'on souhaite afficher.\n");
                        close(local_socket);
                        return EXIT_FAILURE;
                    }
                }

                inter.displayProfil(username, worms);
            }


            if (mode == 4) { // GERER PROFIL (modification profil)

                /*
                mode pourra prendre les valeurs :
                    1 -> modifier infos (du coup pseudo/mdp) //TODO: quand un joueur décide de modifier son pseudo, il faut repasser par toute la phase de vérification de disponibilité + s'assurer de modifier la liste des joueurs en ligne (remplacer l'ancien nom par le nouveau dans la liste des joueurs connectés)
                    2 -> gérer lombrics (ça ça va, c'est juste une petite interaction bdd)
                    3 -> gérer liste d'ami
                */

                mode = inter.manageProfileMenu();
                // envoi du mode au serveur
                sprintf(str_mode, "%d", mode);
                inter.testPrint(str_mode);
                res = sendData(str_mode, local_socket);
                if (res == -1) {
                    close(local_socket);
                    return EXIT_FAILURE;
                }
                if (mode == 1) {
                    //TODO: ça va être un peu chiant (cfr TODO plus haut)
                }
                if (mode == 2) {
                    // il manquerait pas ENCORE un menu par hasard ?
                }
                if (mode == 3) {
                    mode = inter.manageFLMenu();
                    sprintf(str_mode, "%d", mode);
                    res = sendData(str_mode, local_socket);
                    if (res == -1) {
                        close(local_socket);
                        return EXIT_FAILURE;
                    }

                    if (mode == 1) {
                        //Consulter la liste d'amis: (< supprimer un amis >)
                        res = recvFriendList(inter, local_socket);
                        if (res == -1) {
                            close(local_socket);
                            return EXIT_FAILURE;
                        }
                    } else if (mode == 2) {
                        bool error = false;
                        char username[USERNAME_SIZE];
                        char str_buffer[TAILLE_BUFFER];

                        while (1) {
                            inter.searchPlayer(username, error);
                            //Send username
                            res = sendData(username, local_socket);
                            if (res == -1) {
                                close(local_socket);
                                return EXIT_FAILURE;
                            }
                            //Rcv reponse: 1:Friend request sent -1: friend request pas envoyé
                            res = recvData(str_buffer, local_socket);
                            if (res == -1) {
                                close(local_socket);
                                return EXIT_FAILURE;
                            }
                            if (!strcmp(str_buffer, "0")) {
                                strcpy(str_buffer, "La demande d'ami a été envoyé");
                                inter.clearWin();
                                inter.testPrint(str_buffer);
                                inter.wait();
                                break;
                            }
                            error = true;
                        }

                    } else if (mode == 3) {
                        res = recvFriendRequestList(inter, local_socket);
                        if (res == -1) {
                            close(local_socket);
                            return EXIT_FAILURE;
                        }
                    }
                }


            }


            if (mode == 5) { // CHAT
                //TODO: Phase 3.

                inter.clearWin();
                pthread_mutex_t mutex_chat;
                int usernameListSize;
                pthread_t listening_thread;
                pthread_t writing_thread;
                uint32_t packet_size;
                char str_buffer[USERNAME_SIZE];


                // recv nbre de joueurs connectés
                errno = 0;
                res = recv(local_socket, &packet_size, sizeof(uint32_t), 0);
                if (res == -1) {
                    perror("Erreur à la réception.");
                    close(local_socket);
                    return EXIT_FAILURE;
                }
                usernameListSize = ntohl(packet_size);

                MessageData *messagesHistory;//FIXME these should need a delete somewhere
                messagesHistory = new MessageData[usernameListSize];

                char **usernameList;//FIXME these should need a delete somewhere
                usernameList = new char *[usernameListSize];
                for (int list = 0; list < usernameListSize; list++) {
                    usernameList[list] = new char[USERNAME_SIZE];
                }
                for (int i = 0; i < usernameListSize; i++) {

                    res = recvData(str_buffer, local_socket);
                    if (res == -1) {
                        printf("Erreur à la réception de l'username\n");
                        close(local_socket);
                        return EXIT_FAILURE;
                    }
                    strcpy(usernameList[i], str_buffer);
                    MessageData friend_data;
                    strcpy(friend_data.username, str_buffer);
                    messagesHistory[i] = friend_data;
                    //inter.testPrint(str_buffer);
                }


                listening_thread_data data1 = {local_socket, &inter, &mutex_chat};
                pthread_create(&listening_thread, nullptr, listeningThread, &data1);

                writing_thread_data data2;
                data2.socket_client = local_socket;
                data2.inter = &inter;
                data2.mutex_chat = &mutex_chat;

                while (1) {

                    pthread_mutex_lock(&mutex_chat);
                    res = inter.manageFriendList((char **) usernameList, usernameListSize);
                    pthread_mutex_unlock(&mutex_chat);

                    inter.clearWin();
                    strcpy(data2.receiver, usernameList[res - 1]);

                    //TODO: envoyer au serveur l'ami choisi
                    //TODO: recevoir l'historique

                    pthread_create(&writing_thread, nullptr, writingThread, &data2);
                    pthread_join(writing_thread, nullptr);
                    inter.clearWin();
                }
                delete[] messagesHistory;
                for (int list=0;list < usernameListSize;list++){
                    delete usernameList[list];
                }
                delete[] usernameList;
            }
            if (mode == 6) { //return to connection menu
                disconnect = true;
            }
            if (mode == 7) { // QUIT PROGRAM
                // joueur déconnecté, le serveur ferme correctement de son côté
                close(local_socket);
                return EXIT_SUCCESS;
            }

            // ----------- END OF EXECUTION ---------------

            close(local_socket); //TODO: à terme, signaler au serveur que le client s'est déco ?
            return EXIT_SUCCESS;
        }
    }
}