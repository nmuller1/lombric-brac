#include "ProtocolClient.h"


int connectServ(Interface inter, int socket_cible, char client_name[]) {
    char str_buffer[TAILLE_BUFFER];
    int res;

    // ~~~ Questionne l'utilisateur sur ses identifiants ~~~

    /*TODO: faire des tests éventuellement sur la taille des données entrées par l'utilisateur
            cohérence (par exemple si on interdit des caractères spéciaux ( au quel cas il faut le signaler quelque part))
    */

    int error = 0;
    User_input inp;
    while (true) { // on en sort quand l'authentification est validée par le serveur TODO: laisser l'oportunité de quit à tout moment

        inp = inter.loginMenu(error);

        if (inp.ret){strcpy(str_buffer, "$ret");}
        else{strcpy(str_buffer, "$ctn");}

		res = sendData(str_buffer, socket_cible);
		if (res == -1) {
			printf("Erreur à l'envoi du message.(pseudo)\n");
			return -1;
		}
		if (inp.ret){return -25;}

        strcpy(str_buffer, inp.username.data()); // .data nécessaire pour convertir le std::string en const char *
        strcpy(client_name, str_buffer);

        res = sendData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Erreur à l'envoi du message.(pseudo)\n");
            return -1;
        }

        strcpy(str_buffer, inp.password.data());
        res = sendData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Erreur à l'envoi du message.(mdp)\n");
            return -1;
        }

        // -------------- REPONSE DU SERVEUR SUR L'AUTHENTIFICATION -----------------

        res = recvData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Erreur à la réception du résultat de la connexion.\n");
            return -1;
        }

        if (!strcmp(str_buffer, "1")) { //cas validé
            break; // on peut sortir du while(true) d'authentification
        } else error = 1;
    } // fin authentification

    return 1;
}

int registerServ(Interface inter, int socket_cible) {
    char str_buffer[TAILLE_BUFFER];
    int res;

    // show la fenêtre de register
    bool error = false;
    User_input inp;
    while (true) {

        inp = inter.registrationMenu(error); // inp contient toutes les données entrées par le joueru
		if (inp.ret){strcpy(str_buffer, "$ret");}
		else{strcpy(str_buffer, "$ctn");}

		res = sendData(str_buffer, socket_cible);
		if (res == -1) {
			printf("Erreur à l'envoi du message.(pseudo)\n");
			return -1;
		}
		if (inp.ret){return -25;}
        // ============== ETAPE 1 : check si le pseudo est disponible ===============

        // envoi de la demande de pseudo
        strcpy(str_buffer, inp.username.data()); // .data nécessaire pour convertir le std::string en const char *
        res = sendData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Erreur à l'envoi du message.(pseudo)\n");
            return -1;
        }

        // résultat de la demande de pseudo
        res = recvData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Erreur à la réception du message.(confirmation disponibilité pseudo)\n");
            return -1;
        }

        if (!strcmp(str_buffer, "1")) {
            break; // pseudo dispo
        } else error = true; // pour affichage d'écran d'erreur (Username already exists)
    }

    // ============ ETAPE 2 : envoi du mot de passe ==================

    strcpy(str_buffer, inp.password.data());
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Erreur à l'envoi du message.(pseudo)\n");
        return -1;
    }

    // ============= ETAPE 3 : nom des lombrics ==================

    for (unsigned short i = 0; i != 8; i++) {
        strcpy(str_buffer, inp.worms[i].data());
        res = sendData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Erreur à l'envoi du message.(nom worm %d)\n", i);
            return -1;
        }
    }
    return 1;
}

int playGame(Interface inter, int socket_cible, char *client_name) {
    char str_buffer[TAILLE_BUFFER];
    int res, mode;
    int replayID = -1;
    std::cout << replayID << std::endl;// debug pour ne pas avoir d'unused parameter

    mode = inter.manageRoundMenu();

    // envoi du mode au serveur
    sprintf(str_buffer, "%d", mode);
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer le mode (playGame).\n");
        return -1;
    }

    if (mode == 1) {

        res = createGame(inter, socket_cible);
        if (res == -1) {
            printf("Erreur lié au menu createGame.\n");
            return -1;
        }

        // va-t-on se baser sur un mode replay pour continuer une partie en cours ?
        bool replayOn = inter.replayModeChoice();

        res = gameIsRunningWhatYouGonnaDo(socket_cible, inter, replayOn);
        if (res == -1) {
            printf("J'en ai marre je veux que tout ça s'arrête.\n");
            return -1;
        }
        inter.displayGameResult(res);
    }
    if (mode == 2) {
        res = joinGame(inter, socket_cible, client_name);
        if (res == -1) {
            printf("Erreur lié au menu joinGame.\n");
            return -1;
        }


        res = gameIsRunningWhatYouGonnaDo(socket_cible, inter);
        if (res == -1) {
            printf("J'en ai marre je veux que tout ça s'arrête.\n");
            return -1;
        }
        inter.displayGameResult(res);
    }
    return 1;
}

int joinGame(Interface inter, int socket_cible, char *client_name) {

    char str_buffer[TAILLE_BUFFER];
    int res, mode;
    Database db("bdd.db");
    std::vector<std::vector<std::string>> friends;

    mode = inter.joinGameMenu();
    sprintf(str_buffer, "%d", mode);
    res = sendData(str_buffer, socket_cible);
    if (res == -1){
        printf("Impossible d'envoyer le mode de joinGame.\n");
        return -1;
    }
    if (mode == 1){
        inter.displaySearchingRandom();
    }
    if (mode == 2){
        int roomID;
        inter.displaySearchingFriends();
        res = db.getAvailableFriendsMatchs(client_name, friends);
        if (res == -1){
            inter.displayNoFriendRoom();
            return 1;
        }
        else{
            for (unsigned int i=0;i<friends.size();i++){
                for (unsigned int j=0;j<friends[i].size();j++){
                    roomID = inter.manageFriendOpenRooms(friends, j);
                }
            }
        }
        sprintf(str_buffer, "%d", roomID);
        res = sendData(str_buffer, socket_cible);
        if (res == -1){
            std::cout << "Erreur à l'envoi de la room choisie (joinGame)" << std::endl;
        }

    }

    return 1;
}

int askAccess(Interface inter, int socket_cible) { //FIXME: utiliser l'interface
    /* Valeurs de retour :
            1 : accès authorisé
            0 : accès refusé
            -1 : erreur d'exécution
    */


    char str_buffer[TAILLE_BUFFER];
    int res;

    // RECEPTION DE LA REPONSE DE "grantAccess" ou "denyAccess"
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir la réponse de grantAccess/denyAccess.\n");
        return -1;
    }

    // CAS 1 : accès authorisé
    if (!strcmp(str_buffer, "1")) {
        return 1; // TODO: potentiellement plus à faire ici
    }

    // CAS 2 : accès refusé
    if (!strcmp(str_buffer, "0")) {
        inter.ConnectionError();
        return 0;
    }
    return 1;
}

bool isDeadPlayer(Team **allTeams, int current_player, int worm_playing, int nbre_worm) {
    int rip = 0; // compteur du nombre de vers morts du joueur qu'on traite
    bool dead = false;
    Worm **playersWorms = allTeams[current_player]->getWorms();
    while (!dead && !playersWorms[(worm_playing + rip) % nbre_worm]->isAlive()) {
        rip++;
        if (rip == nbre_worm) dead = true;
    }
    return dead;
}

int createGame(Interface inter, int socket_cible) {

    int res;
    char str_buffer[TAILLE_BUFFER], str_int[10];
    GameParameters GP;
    GP = inter.chooseGameParameters();

    // ENVOI DE PARAMETRE : friend game ? (FRIENDS) AJOUTER LE CHOIX DANS LES MENUS
    sprintf(str_int, "%d", GP.friend_game);
    strcpy(str_buffer, str_int);
    res = sendData(str_buffer, socket_cible);
    if (res == -1){
        printf("Dans manageCreateGame : Erreur à l'envoi du paramètre : friend_game.\n");
        GP.error = -1;
        return -1;
    }


    // ENVOI DE PARAMETRE : mode de jeu
    sprintf(str_int, "%d", GP.game_mode);
    strcpy(str_buffer, str_int);
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer le paramètre de partie : gamemode.\n");
        return -1;
    }


    // ENVOI DE PARAMETRE : nombre de joueurs dans la partie
    sprintf(str_int, "%d", GP.no_of_players);
    strcpy(str_buffer, str_int);
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer le paramètre de partie : nbre joueurs.\n");
        return -1;
    }

    // ENVOI DE PARAMETRE : nombre de vers par joueur
    sprintf(str_int, "%d", GP.no_of_worms);
    strcpy(str_buffer, str_int);
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer le paramètre de partie : nbre worms par équipe.\n");
        return -1;
    }

    // ENVOI DE PARAMETRE : temps d'un tour
    sprintf(str_int, "%d", GP.max_round_time);
    strcpy(str_buffer, str_int);
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer le paramètre de partie : tmp d'un tour.\n");
        return -1;
    }

    // ENVOI DE PARAMETRE : points de vie initiaux
    sprintf(str_int, "%d", GP.init_life_pts);
    strcpy(str_buffer, str_int);
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer le paramètre de partie : points de vie initiaux.\n");
        return -1;
    }

    // ENVOI DE PARAMETRE : crate spawnrate
    sprintf(str_int, "%d", GP.packs_frequence);
    strcpy(str_buffer, str_int);
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer le paramètre de partie : crate spawnrate.\n");
        return -1;
    }

    // ENVOI DE PARAMETRE : points de vie par medkit
    sprintf(str_int, "%d", GP.pts_per_life_pack);
    strcpy(str_buffer, str_int);
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer le paramètre de partie : points de vie par medkit.\n");
        return -1;
    }

    return 1;
}

int gameIsRunningWhatYouGonnaDo(int socket_cible, Interface inter){
    return gameIsRunningWhatYouGonnaDo(socket_cible, inter, false);
}

int gameIsRunningWhatYouGonnaDo(int socket_cible, Interface inter, bool replayOn) {

    /*
    Fonction principale ou se déroule la partie en cours pour chaque client

    Valeur de retour de la fonction :
        * -1 en cas d'erreur quelconque
        *  0 en cas de défaite
        *  1 en cas de victoire
    */

    int gameRes = 0;
    int res;
    char str_buffer[TAILLE_BUFFER];
    int personnal_id;
    int current_player = 0;

    GameParameters GP;

    // Réception du nombre de joueurs
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir le nombre de joueurs de la partie.\n");
        return -1;
    }
    GP.no_of_players = (int) strtol(str_buffer, nullptr, 10);

    // Réception du id
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir le nombre de joueurs de la partie.\n");
        return -1;
    }
    personnal_id = (int) strtol(str_buffer, nullptr, 10);

    // Réception du nombre de vers
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir le nombre de worms par joueur.\n");
        return -1;
    }
    GP.no_of_worms = (int) strtol(str_buffer, nullptr, 10);

    // Réception du nombre de hp par medkit
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir le nombre de hp par medkit.\n");
        return -1;
    }
    GP.pts_per_life_pack = (int) strtol(str_buffer, nullptr, 10);

    //réception nombre hp initial d'un vers
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir le nombre de hp d'un vers.\n");
        return -1;
    }
    GP.init_life_pts = (int) strtol(str_buffer, nullptr, 10);


    // Reception du temps d'un tour
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir le nombre de hp par medkit.\n");
        return -1;
    }
    GP.max_round_time = (int) strtol(str_buffer, nullptr, 10);

    //Réception du mode de jeu
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir le mode de jeu.\n");
        return -1;
    }
    GP.game_mode = (int) strtol(str_buffer, nullptr, 10);




    // notifie au serveur si on est en mode replay
    if (personnal_id == 0){ // seul le créateur de la partie doit le faire
        if (replayOn) {

            strcpy(str_buffer, "REPLAY");
            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Impossible d'envoyer REPLAY/NO REPLAY\n");
                return -1;
            }

            int column, line;

            res = recvData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Erreur réception nombre de ligne de matchs (replay)\n");
                return -1;
            }
            line = (int) strtol(str_buffer,nullptr,10);

            res = recvData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Erreur réception nombre de colonne de matchs (replay)\n");
                return -1;
            }
            column = (int) strtol(str_buffer,nullptr,10);

            std::vector<std::vector<std::string>> matchs;
            for (int i=0; i<line; i++){
                std::vector<std::string> tmp;
                for (int j=0; j<column; j++){
                    res = recvData(str_buffer, socket_cible);
                    if (res == -1){
                        printf("De toute façon les print on les verra pas à cause de curse. \n");
                        return -1;
                    }
                    tmp.push_back(std::string(str_buffer));
                }
                matchs.push_back(tmp);
            }


            // seulement si on est en replay, nécessite l'envoi de l'id du replay
            int replayID = inter.chooseReplayID(matchs);
            sprintf(str_buffer, "%d", replayID);
            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Impossible d'envoyer le replayID\n");
                return -1;
            }
        }

        else{
            strcpy(str_buffer, "0"); // message sans importance

            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Impossible d'envoyer REPLAY/NO REPLAY\n");
                return -1;
            }
        }


    }

    if (replayOn) { //pour tous les joueurs cette fois
        res = recvData(str_buffer, socket_cible);
        if (res == -1){
            printf("Erreur à la réception d'un parametre de partie (mode replay)\n");
            return -1;
        }
        GP.no_of_worms = (int) strtol(str_buffer, nullptr, 10);

        res = recvData(str_buffer, socket_cible);
        if (res == -1){
            printf("Erreur à la réception d'un parametre de partie (mode replay)\n");
            return -1;
        }
        GP.init_life_pts = (int) strtol(str_buffer, nullptr, 10);

        res = recvData(str_buffer, socket_cible);
        if (res == -1){
            printf("Erreur à la réception d'un parametre de partie (mode replay)\n");
            return -1;
        }
        GP.pts_per_life_pack = (int) strtol(str_buffer, nullptr, 10);

        res = recvData(str_buffer, socket_cible);
        if (res == -1){
            printf("Erreur à la réception d'un parametre de partie (mode replay)\n");
            return -1;
        }
        GP.max_round_time = (int) strtol(str_buffer, nullptr, 10);

        res = recvData(str_buffer, socket_cible);
        if (res == -1){
            printf("Erreur à la réception d'un parametre de partie (mode replay)\n");
            return -1;
        }
        GP.game_mode = (int) strtol(str_buffer, nullptr, 10);
    }




    // ========== ON ATTEND DE RECEVOIR UN MESSAGE DU SERVEUR : C'est bon tu peux jouer =========

    std::cout << "Recv top départ.." << std::endl;

    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Erreur réception de message : 'la partie se lance' (dans playGame)\n");
    }

    std::cout << "Bien reçu chef" << str_buffer << std::endl;


    if (!strcmp(str_buffer, "1")) { //TOP DÉPART

        int wormCoord[32]; //au maximum il y a 8vers x 4joueurs == 32 vers

        // ================ ETAPE 1 : envoyer noms des lombrics ==================
        // TODO: la retrouver via la bdd

        //DEBUG: en attendant la bdd

        strcpy(str_buffer, "Michel");
        for (int i = 0; i < 8; i++) {
            sendChar(str_buffer,socket_cible, const_cast<char*>("Envoi des noms de ver"));
        }

        // ================ ETAPE 2 : recevoir la position des vers ==================

        int i = 0;
        while (true) {
            wormCoord[i] = receiveInt(socket_cible, const_cast<char*>("Position des vers"));
            if (wormCoord[i] == -1 && i != 32) {
                printf("Totalité des positions de vers reçue.\n");
                break;
            }
            i++;
        }

        // ================ ETAPE 3 : recevoir les teams de chaque joueur ==================


        char current_worm_number[2];
        Team **allTeams = new Team *[GP.no_of_players]; // allTeams contient toutes les team de vers, pas les players directement

        // initialise une liste dynamique bi-dimentionnelle contenant des pointeurs vers les vers de tous les joueurs
        Worm ***wormsOfPlayer = new Worm **[GP.no_of_players];
        for (int l = 0; l < GP.no_of_worms; l++) {
            wormsOfPlayer[l] = new Worm *[GP.no_of_worms];
        }

        std::string tmp = "default"; // TODO: recevoir le nom de team + link la putain de bdd de ses morts
        for (int k = 0; k < GP.no_of_players; k++) {
            for (int j = 0; j < GP.no_of_worms; j++) {
                receiveChar(str_buffer, socket_cible, const_cast<char *>("Nom de worm dans girwygd"));

                sprintf(current_worm_number, "%d", j);
                if (GP.game_mode == 1) {
                    wormsOfPlayer[k][j] = new Worm(str_buffer, current_worm_number[0], k + 2, GP.init_life_pts, k);
                } else if (GP.game_mode == 2) {
                    char name[TAILLE_BUFFER];
                    strcpy(name, str_buffer);

                    res = recvData(str_buffer, socket_cible);
                    if (res == -1) {
                        printf("Impossible de recevoir un nom de worm (dans girwygd)\n");
                        return -1;
                    }

                    wormsOfPlayer[k][j] = new Worm(name, current_worm_number[0], (int) strtol(str_buffer, nullptr, 10),
                                                   GP.init_life_pts, k);
                    std::cout << "Couleurs reçues : (index) - (colour) " << k << " - "
                              << (int) strtol(str_buffer, nullptr, 10) << std::endl;
                }
            }

            allTeams[k] = new Team(GP.no_of_worms, tmp, wormsOfPlayer[k]);
        }

        // ============== ETAPE 4 : afficher la position des vers chez les joueurs ================

        std::string map = "default.map";
        Game current_game = Game(map);

        for (int nb = 0; nb < GP.no_of_players; nb++) {
            current_game.placeWorms(GP.no_of_worms, nb, wormCoord, allTeams[nb]);
        }

        initscr();

        if (has_colors() == FALSE) {
            endwin();
            std::cout << "Your terminal does not support colors.\n" << std::endl;
            exit(1);
        }

        cbreak();
        keypad(stdscr, true);
        scrollok(stdscr, FALSE);
        noecho();
        start_color();
        init_pair(COLOR_AIR, COLOR_WHITE, COLOR_CYAN);
        init_pair(COLOR_RED_PLAYER, COLOR_RED, COLOR_CYAN);
        init_pair(COLOR_YELLOW_PLAYER, COLOR_YELLOW, COLOR_CYAN);
        init_pair(COLOR_BLACK_PLAYER, COLOR_BLACK, COLOR_CYAN);
        init_pair(COLOR_BLUE_PLAYER, COLOR_BLUE, COLOR_CYAN);
        init_pair(COLOR_DIRT, COLOR_WHITE, COLOR_GREEN);
        init_pair(COLOR_ROCK, COLOR_WHITE, COLOR_BLACK);
        init_pair(COLOR_RED_TEAM, COLOR_BLACK, COLOR_RED);
        init_pair(COLOR_YELLOW_TEAM, COLOR_BLACK, COLOR_YELLOW);
        init_pair(COLOR_GREEN_TEAM, COLOR_BLACK, COLOR_GREEN);
        init_pair(COLOR_BLUE_TEAM, COLOR_BLACK, COLOR_BLUE);
        init_pair(COLOR_WATER, COLOR_BLUE, COLOR_BLUE);
        current_game.StartGame(GP.no_of_players, GP.no_of_worms, allTeams);


        // ##########################################################################################################
        //
        //                                      LA PARTIE EN ELLE-MEME
        //
        // ##########################################################################################################

        int deadWormsOffset = 0, worm_playing = 0;
        current_game.setPlayerId(personnal_id);
        bool dead, startTurnDead; // pour empêcher le spawn d'une crate au lancement de la partie
        while (true) { // condition d'arrêt : le serveur envoie le message "END"

            spawnCrate(&current_game, socket_cible, GP.pts_per_life_pack);

            dead = false; //reset changement de joueur
            // TEST DE MORT POUR SKIP LE JOUEUR S'IL EST MORT (plus simple)
            startTurnDead = isDeadPlayer(allTeams, current_player, worm_playing, GP.no_of_worms);
            if (startTurnDead && personnal_id == current_player) { // un seul client doit envoyé l'état "mort"
                strcpy(str_buffer, "SKIP");
                res = sendData(str_buffer, socket_cible);
                if (res == -1) {
                    printf("Impossible d'envoyer skip\n");
                    return -1;
                }
            }

            // cas où le joueur courant n'est pas mort
            if (!startTurnDead) {

                if (personnal_id == current_player) {
                    current_game.setActor(true);
                    current_game.RunGame(
                            allTeams[current_player]->getWorms()[(worm_playing + deadWormsOffset) % GP.no_of_worms],
                            socket_cible, GP.max_round_time);
                    current_game.UpdateInfo(GP.no_of_players, GP.no_of_worms, allTeams,
                                            0); // TODO: le 4eme paramètre doit gérer le temps
                } else {
                    // Réception des input
                    current_game.setActor(false);
                    while (true) { // while de réception des inputs d'un joueur
                        res = recvData(str_buffer, socket_cible);
                        if (res == -1) {
                            printf("Impossible de recevoir une input à interpréter.\n");
                            return -1;
                        }
                        if (!strcmp(str_buffer, "TIME")) break;

                        if (!strcmp(str_buffer, SHOOT)) {
                            int x, y, modifier;
                            char objectType[3];
                            recvShootInfo(x, y, modifier, objectType, socket_cible);
                            Object *object = Game::constructObject(objectType);
                            current_game.useObjectOnSpectatorsScreens(
                                    allTeams[current_player]->getWorms()[(worm_playing + deadWormsOffset) %
                                                                         GP.no_of_worms], object, x, y, modifier);
                            delete object;
                        } else { //exécute toutes les actions autre que les tirs
                            current_game.ExecuteInput(
                                    allTeams[current_player]->getWorms()[(worm_playing + deadWormsOffset) %
                                                                         GP.no_of_worms],
                                    (int) strtol(str_buffer, nullptr, 10));
                        }
                        current_game.UpdateInfo(GP.no_of_players, GP.no_of_worms, allTeams,
                                                0); // TODO: le 4eme paramètre doit gérer le temps
                    }
                }
                dead = isDeadPlayer(allTeams, current_player, worm_playing, GP.no_of_worms);

            }

            if ((dead || startTurnDead) && personnal_id == current_player) { // un seul client doit envoyé l'état "mort"
                strcpy(str_buffer, "DEAD");
                res = sendData(str_buffer, socket_cible);
                if (res == -1) {
                    printf("Impossible d'envoyer la mort d'un joueur au server\n");
                    return -1;
                }
            } else if (!dead && personnal_id == current_player) { // un seul client envoie l'état "vie"
                strcpy(str_buffer, "0"); //message sans intérêt, juste pour pas rester bloquer
                res = sendData(str_buffer, socket_cible);
                if (res == -1) {
                    printf("Impossible d'envoyer la mort d'un joueur au server\n");
                    return -1;
                }
            }

            // le serveur nous signale si la partie est terminée
            res = recvData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Impossible de recevoir l'état de la partie (continue/finie)\n");
                return -1;
            }

            if (!strcmp(str_buffer, "END")) break; // LA PARTIE S'ACHEVE

            if (!strcmp(str_buffer, "FLOOD")) current_game.flood();

            // on se positionne sur le prochain joueur
            // évolution théorique (sans compter les worms mourrant)
            current_player++;
            if (current_player == GP.no_of_players) {
                current_player = 0;
                worm_playing++;
                if (worm_playing == GP.no_of_worms) worm_playing = 0;
            }

            // pour se positionner sur le prochain vers en vie
            deadWormsOffset = 0; //reset de l'offset car on change de joueur
            Worm **playerWorms = allTeams[current_player]->getWorms();
            while (deadWormsOffset < 3 && !playerWorms[(worm_playing + deadWormsOffset) %
                                                       GP.no_of_worms]->isAlive()) {
                deadWormsOffset++;
            }

            wrefresh(stdscr); // double sécurité

        } //endGame

        // on recoit l'information de si on a gagné ou perdu

        res = recvData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Impossible de recevoir le résultat WIN/LOSE en fin de partie.\n");
            return -1;
        }
        if (!strcmp(str_buffer, "WIN")) {
            gameRes = 1;
            inter.displayGameResult(gameRes);
        }
        else if (!strcmp(str_buffer, "LOSE")){
            gameRes = 0;
            inter.displayGameResult(gameRes);
        }


        //FIXME: double free or corruption si on fait les delete
        // ========================================================
        // DELETE DES LISTES DYNAMIQUES

        /*
        for (int i = 0; i < GP.no_of_worms; i++) {
            delete *wormsOfPlayer[i];
        }
        delete[] *wormsOfPlayer;

        delete[] allTeams;
        */

    } else {
        //TODO: gérer le cas où le top départ n'est pas validé par le serveur
    }
    return gameRes;
}

void recvShootInfo(int &x, int &y, int &modifier, char weaponType[3], int socket_cible) {
    //RCV X
    x = receiveInt(socket_cible, const_cast<char *>( "x dans les armes"));
    //RCV Y
    y = receiveInt(socket_cible, const_cast<char *>( "y dans les armes"));
    //RCV Modifier
    modifier = receiveInt(socket_cible, const_cast<char *>( "modifier dans les armes"));
    //RCV weaponType
    receiveChar(weaponType, socket_cible, const_cast<char *>( "type dans les armes"));
}

void spawnCrate(Game *game, int socket_cible, int medkitHP) {

    char str_buffer[TAILLE_BUFFER];
    int res, spawnX = 0, type = 0, crateType = 0;
    Crate *crate;

    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("erreur réception spawnX spawnCrate");
    }
    spawnX = (int) strtol(str_buffer, nullptr, 10);


    if (spawnX >= 0) {
        res = recvData(str_buffer, socket_cible);
        if (res == -1) {
            printf("erreur réception medkit/guncrate spawnCrate");
        }
        type = (int) strtol(str_buffer, nullptr, 10);

        if (type) { //guncrate
            res = recvData(str_buffer, socket_cible);
            if (res == -1) {
                printf("erreur réception typeGunCrate spawnCrate");
            }
            crateType = (int) strtol(str_buffer, nullptr, 10);

            crate = new GunCrate(static_cast<Content>(crateType));

        } else { //medkit
            crate = new Medkit(medkitHP);
        }

        game->placeCrate(spawnX, crate);
    }
}


int storeMemory(char* message, MessageData* messageHistory, int nbOfFriends){
    char sourceName[USERNAME_SIZE];
    int i;
    for(i = 0; i < USERNAME_SIZE; i++){
        if(message[i] == ':') break;
        sourceName[i] = message[i];
    }

    for(i = 0; i < nbOfFriends; i++){
        if(!strcmp(sourceName, messageHistory[i].username)){
            int sumSize = strlen(messageHistory[i].buffer) + strlen(message);
            if(sumSize >= messageHistory[i].taille_buffer){
                char *temp_buffer;

                temp_buffer = (char *)realloc(messageHistory[i].buffer, (sumSize + 1) * sizeof(char *));
                if(!temp_buffer){
                    strcpy(messageHistory[i].buffer, message);
                    return 1;
                }

                messageHistory[i].taille_buffer = sumSize + 1;
            }
            strcat(messageHistory[i].buffer, "\n");
            strcat(messageHistory[i].buffer, message);
            return 1;
        }
    }
    return -1;
}

int getFriendIndex(char username[USERNAME_SIZE], MessageData* messageHistory, int nbOfFriends){

    int i;
    for(i = 0; i < nbOfFriends; i++){
        if(!strcmp(messageHistory[i].username, username)){
            return i;
        }
    }
    return -1;
}



int recvRanking(Interface inter, int socket_cible){
    int res;
    int lines, rows;
    char size_str[10];
    char buffer[USERNAME_SIZE];
    std::vector <std::vector<std::string>> ranking;
    errno = 0;
    res = recvData(size_str, socket_cible);
    if(res == -1){
        printf("Erreur de reception du nombre de ligne du classement\n");
        return -1;
    }
    lines = (int) strtol(size_str, nullptr, 10);
    if(errno != 0){
        perror("Impossible de convertir le nombre de ligne");
        return -1;
    }

    res = recvData(size_str, socket_cible);
    if(res == -1){
        printf("Erreur de reception du nombre de ligne du classement\n");
        return -1;
    }
    rows = (int) strtol(size_str, nullptr, 10);
    if(errno != 0){
        perror("Impossible de convertir le nombre de colonnes");
        return -1;
    }
    for(int i = 0; i < lines; i++){
        std::vector<std::string> user;
        for(int j = 0; j < rows; j++){
            user.clear();
            res = recvData(buffer, socket_cible);
            if(res == -1){
                printf("Erreur de reception du classement\n");
                return -1;
            }
            user.push_back(buffer); //Conversion de char* en std::string
            inter.testPrint(buffer);
        }
        ranking.push_back(user);
    }

    //Display Ranking
    inter.manageRanking(ranking);



    return 1;
}

int recvFriendList(Interface inter, int local_socket){
    int res;
    int nb_friends;
    uint32_t packet_size;
    char str_buffer[USERNAME_SIZE];

    //Recv le nombre d'ami
    res = recv(local_socket, &packet_size, sizeof(uint32_t), 0);
    if(res == -1){
        return -1;
    }
    nb_friends = ntohl(packet_size);//todo will create errors
    char** friendList = new char*[packet_size];
    for (int size=0; size < nb_friends;size++){
        friendList[size] = new char[USERNAME_SIZE];
    }

    //Recv les amis
    for(int i = 0; i < nb_friends; i++){
        res = recvData(str_buffer, local_socket);
        if(res == -1){
            return -1;
        }
        strcpy(friendList[i], str_buffer);
    }
    //Affiche liste d'amis
    inter.manageFriendList((char**)friendList, nb_friends);
    for (int size=0; size < nb_friends;size++){
        delete friendList[size];
    }
    delete[] friendList;

    return 1;
}

int recvFriendRequestList(Interface inter, int local_socket){
    int res;
    int N;
    int chosen;
    char size_str[10];
    uint32_t packet_size;
    char str_buffer[USERNAME_SIZE];

    //Reception de la taille
    res = recv(local_socket, &packet_size, sizeof(uint32_t), 0);
    if(res == -1){
        return -1;
    }

    N = ntohl(packet_size);

    char** requestList = new char*[N];
    for (int size=0;size<N;size++){
        requestList[size]=new char[USERNAME_SIZE];
    }

    for(int i = 0; i < N; i++){
        res = recvData(str_buffer, local_socket);
        if(res == -1){
            return -1;
        }
        strcpy(requestList[i], str_buffer);
    }
    chosen = inter.manageFriendList((char**)requestList, N, true) - 1;

    res = inter.friendRequestResponse();
    if(res != -1){
        sprintf(size_str, "%d", res);
        res = sendData(size_str, local_socket);
        if(res == -1){
            return -1;
        }

        res = sendData(requestList[chosen], local_socket);
        if(res == -1){
            return -1;
        }
    }
    for (int size=0;size<N;size++){
        delete requestList[size];
    }
    delete[] requestList;

    return 1;
}