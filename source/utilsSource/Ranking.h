#ifndef RANKING_H
#define RANKING_H

class Ranking {
    //TODO : revoir les attributs par rapport à la BDD
    unsigned wins;
    unsigned losses;
    unsigned ties;
    unsigned totalPlayed;
    unsigned elo;

public:
    Ranking();

    Ranking(unsigned wins, unsigned losses, unsigned ties, unsigned totalPlayed, unsigned elo);

    ~Ranking();

    unsigned getWins();

    unsigned getlosses();

    unsigned getTies();

    unsigned getTotalPlayed();

    unsigned getElo();

    void incWins();

    void incLosses();

    void incTies();

    void incTotalPlayed();

    void setElo(unsigned newElo);
};

#endif //RANKING_H