#include "Vector2d.h"

Vector2d::Vector2d() {
    this->x = 0;
    this->y = 0;
}

Vector2d::Vector2d(double x, double y) {
    this->x = x;
    this->y = y;
}

Vector2d *Vector2d::operator-(Vector2d subtractor) {
    double subbedX, subbedY;
    subbedX = this->x;
    subbedY = this->y;
    auto ret = new Vector2d(subbedX - subtractor.x, subbedY - subtractor.y);
    return ret;
}

void Vector2d::operator*=(int multiplicator) {
    x *= multiplicator;
    y *= multiplicator;
}

void Vector2d::operator*=(double multiplicator) {
    x *= multiplicator;
    y *= multiplicator;
}

void Vector2d::normalizeTo(int ceiling) {
    double maximum, ratio;
    maximum = std::max(std::abs(x), std::abs(y));
    ratio = maximum / ceiling;
    x /= ratio;
    y /= ratio;
}

