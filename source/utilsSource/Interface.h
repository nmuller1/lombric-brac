#ifndef INTERFACE_CPP
#define INTERFACE_CPP

#include <iostream>
#include <cstring>
#include "ncurses.h"
#include "GameParameters.h"
#include <vector>

#define INIT_TAILLE 1024
#define ENTER 10
#define USERNAME_SIZE 16

typedef struct {
	bool ret = false;
    std::string username;
    std::string password;
    std::string worms[8];
} User_input;

typedef struct {
    int Time_filter; // depuis toujours = 1, ce moi-ci = 2, cette semaine = 3
    int Game_mode_filter; // match à mort = 1, match à mort par équipe = 2
    int Ranking_system_filter; // général = 1, par ratio victoire = 2, par temps de jeu = 3
} Ranking_filter;

class Interface {
private:
    WINDOW *displayBox;

    void displayWelcomeMenu(int);

    void displayPrincipalMenu(int);

    void displayManageGameMenu(int);

    void displayEnemyChoiceMenu(int);

    void displayJoinGameMenu(int);

    void displayManageFLMenu(int);

    void displayManageProfileMenu(int);

	void displayFriendGameMenu(int);

    void displayGameModeMenu(int);

    void displayTimeFilterMenu(int);

    void displayGameModeFilterMenu(int);

    void displaySystemFilterMenu(int);

    void displayNoOfPlayers(int);

    void displayNoOfWorms(int);

    void displayInitialLifePts(int);

    void displayRoundTimeLimit(int);

    void displayPacksFrequence(int);

    void displayPtsPerLifePack(int);

	void displayReplayChoiceMenu(int);

    void displayFriendList(char** friends, int noOfFriends, int highlighted);

    void displayRanking(std::vector<std::vector<std::string>>&, unsigned int);

	int managechooseReplayID(std::vector<std::vector<std::string>> matches);

	void displayLastServerGames(long unsigned highlighted, std::vector<std::vector<std::string>>);

    void displayFriendRequestResponse(int);

    int manageDisplay(int, int);

public:
    Interface();

    ~Interface();

    int welcomeMenu();

    User_input loginMenu(bool = false);

    User_input registrationMenu(bool = false);

    int principalMenu();

    int manageRoundMenu();

    int enemyChoiceMenu();

    int joinGameMenu();

    int manageProfileMenu();

    Ranking_filter rankingMenu();

    void searchPlayer(char[USERNAME_SIZE], bool = false);

    void searchGame(char[INIT_TAILLE], bool = false);

    void displayProfil(char username[USERNAME_SIZE], char worms[8][USERNAME_SIZE]);

    GameParameters chooseGameParameters();

    void ConnectionError();

    void loadingGame(); // menu d'attente des joueurs avant que la partie commence

	int manageFriendList(char **friends, int noOfFriends);

    void manageRanking(std::vector<std::vector<std::string>>&);

    void writeMessage(char [100]);

    void testPrint(char *);

    void wait();

    void clearWin();

    void getInput(char *);

    void displayGameResult(int result);

    int manageFLMenu();

    int friendRequestResponse();

    int chooseReplayID(std::vector<std::vector<std::string>> matches);

    int replayModeChoice();

	int manageFriendOpenRooms(std::vector<std::vector<std::string>>, int);

	void displaySearchingRandom();

	void displaySearchingFriends();

	void displayNoFriendRoom();

    void displayFriendList(char **friends, int noOfFriends, int highlighted, bool friendRequest);

    void displayFriendsOpenRooms(std::vector<std::vector<std::string>> friends, int noOfFriends, int highlighted);

    int manageFriendList(char **friends, int noOfFriends, bool friendRequest);
};


#endif
