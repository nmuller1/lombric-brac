#include "Ranking.h"

Ranking::Ranking() {
    this->wins = 0;
    this->losses = 0;
    this->ties = 0;
    this->totalPlayed = 0;
    this->elo = 0.0;
}

Ranking::Ranking(unsigned wins, unsigned losses, unsigned ties, unsigned totalPlayed, unsigned elo) {
    this->wins = wins;
    this->losses = losses;
    this->ties = ties;
    this->totalPlayed = totalPlayed;
    this->elo = elo;
}

Ranking::~Ranking() = default;

unsigned Ranking::getWins() {
    return this->wins;
}

unsigned Ranking::getlosses() {
    return this->losses;
}

unsigned Ranking::getTies() {
    return this->ties;
}

unsigned Ranking::getTotalPlayed() {
    return this->totalPlayed;
}

unsigned Ranking::getElo() {
    return this->elo;
}

void Ranking::incWins() {
    this->wins++;
}

void Ranking::incLosses() {
    this->losses++;
}

void Ranking::incTies() {
    this->ties++;
}

void Ranking::incTotalPlayed() {
    this->totalPlayed++;
}

void Ranking::setElo(unsigned newElo) {
    this->elo = newElo;
}