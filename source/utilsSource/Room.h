//
// Created by Aloïs Glibert (000393192) on 10/03/20 .
//

#ifndef GROUPE_8_ROOM_H
#define GROUPE_8_ROOM_H

#include "../gameSource/Player.h"
#include "GameParameters.h"

typedef struct {
    Player _players_tab[4];
    GameParameters _GP;
    int _current_nbre_player;
    int _sockets_tab[4];
    int _friendRoom = 0;
    int _creatorID;
} Room;

#endif //GROUPE_8_ROOM_H
