#ifndef TEAM_H
#define TEAM_H

#include <string>
#include "../gameSource/Worm.h"

class Worm;

class Team {
    int teamSize{};
    std::string teamName;
    Worm **worms{};

public:
    Team();

    Team(int teamSize, std::string teamName, Worm **worms);

    ~Team();

    Team(Team &&team);

    Team &operator=(Team &&team);


    Worm **getWorms();

    std::string getTeamName();

    void setTeamName(std::string newName);

    void setWroms(Worm **worms);
};

#endif //TEAM_H
