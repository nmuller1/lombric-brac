#include "DataExchange.h"

int recvData(char *str_buffer, int socket_cible) {
    char *str_parser;
    int res, longueur_chaine, taille_recue;
    uint32_t packet_size;

    // recv taille 
    errno = 0;
    res = recv(socket_cible, &packet_size, sizeof(uint32_t), 0);
    if (res == -1) {
        perror("Erreur à la réception.");
        return -1;
    }
    longueur_chaine = ntohl(packet_size);

    //recv réel
    for (str_parser = str_buffer, taille_recue = 0; taille_recue < longueur_chaine;) {
        errno = 0;
        res = recv(socket_cible, str_parser, longueur_chaine - taille_recue, 0);
        if (res == -1) {
            perror("Impossible de recevoir le pseudo: ");
            return -1;

        } else if (res == 0) {
            printf("Fermeture socket cible.\n");
            return -1;
        }
        taille_recue += res;
        str_parser += res;
    }

    str_buffer[taille_recue] = '\0';
    return 1;
}

int sendData(char *str_buffer, int socket_cible) {
    /*
    Nécessitera, utilisée dans les threads, de bien faire un pthread_exit(NULL) quand la valeur de retour == -1
    */

    char *str_parser;
    int res, longueur_chaine, taille_envoyee;
    uint32_t packet_size;

    longueur_chaine = (int) strlen(str_buffer);
    packet_size = htonl(longueur_chaine);

    errno = 0;
    //send taille
    res = send(socket_cible, &packet_size, sizeof(uint32_t), 0);
    if (res == -1) {
        perror("Error.\n");
        return -1;
    }

    //send réel
    for (str_parser = str_buffer, taille_envoyee = 0; taille_envoyee < longueur_chaine;) {
        errno = 0;
        res = send(socket_cible, str_parser, longueur_chaine - taille_envoyee, 0);
        if (res == -1) {
            perror("Error.\n");
            return -1;
        }
        taille_envoyee += res;
        str_parser += res;
    }
    return 1;
}

void fakeNews(int socket_cible) { //FIXME: en phase de test
    /*
    Tentative de résolution d'un bug de désynchro lors du changement de joueurs "actif", l'idée est que
    comme la première input envoyée par un joueur actif est de temps en temps perdu comme première input
    reçue d'un joueur passif - ce qui provoque une désynchronisation des postions des worms sur les différents
    clients - s'il on envoie des inputs sans intérêts qui seront traitées comme des inputs (mais pas influentes
    sur la partie -> tombe dans le case default : break d'interprétation d'input) pour que ces inputs si soient
    perdues plutot qu'une réelle input interprétable
    */
    int res;
    char str_buffer[TAILLE_BUFFER];
    strcpy(str_buffer, "$"); // input sans sens
    for (int i = 0; i < 3; i++) {
        res = sendData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Impossible d'envoyer une fakeNews\n");
            continue;
        }
    }
}

int receiveInt(int targetSocket, char *situation) {
    int res;
    char str_buffer[TAILLE_BUFFER];
    res = recvData(str_buffer, targetSocket);
    if (res == -1) {
        printf("Échec de réception (%s)\n", situation); //TODO: bien traiter ça un jour
    }
    return (int) strtol(str_buffer, NULL, 10);
}

void sendInt(int toSend, int targetSocket, char *situation) {
    int res;
    char str_buffer[TAILLE_BUFFER];
    sprintf(str_buffer, "%d", toSend);
    res = sendData(str_buffer, targetSocket); //TODO: voir si c'est bien envoyé
    if (res == -1) {
        printf("Échec d'envoi (%s)\n", situation);// FIXME: je saurai traiter ces cas un jour ? on y croit
    }
}

void sendChar(char *toSend, int targetSocket, char *situation) {
    int res;
    char str_buffer[TAILLE_BUFFER];
    sprintf(str_buffer, "%s", toSend);
    res = sendData(str_buffer, targetSocket); //TODO: voir si c'est bien envoyé
    if (res == -1) {
        printf("Échec d'envoi (%s)\n", situation);// FIXME: je saurai traiter ces cas un jour ? on y croit
    }
}

void receiveChar(char *charPtr, int targetSocket, char *situation) {
    int res;
    char str_buffer[TAILLE_BUFFER];
    res = recvData(str_buffer, targetSocket);
    if (res == -1) {
        printf("Échec de réception (%s)\n", situation); //TODO: bien traiter ça un jour
    }
    strcpy(charPtr,str_buffer);
}
