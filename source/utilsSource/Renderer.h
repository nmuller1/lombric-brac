#ifndef RENDERER_H
#define RENDERER_H

/*
 * Represents the portion of the map that will be displayed on the screen.
 * Displayed area = width x height, with (start_horiz, start_vert) being the top left corner.
 */

struct Renderer {
    unsigned rWidth, rHeight, start_horiz, start_vert, mapHeight, mapWidth;

    Renderer(unsigned height, unsigned width, unsigned start_horiz, unsigned start_vert);

    Renderer(unsigned height, unsigned width, unsigned start_horiz, unsigned start_vert, unsigned mapHeight,
             unsigned mapWidth);

    ~Renderer() = default;

    void Resize(unsigned height, unsigned width);

    void MoveVert(unsigned startVert);

    void MoveHoriz(unsigned startHoriz);
};

#endif //RENDERER_H