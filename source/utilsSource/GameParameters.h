//
// Created by Aloïs Glibert (000393192) on 10/03/20 .
//

#ifndef GROUPE_8_GAMEPARAMETERS_H
#define GROUPE_8_GAMEPARAMETERS_H

typedef struct {
    int game_mode;
    int no_of_players;
    int no_of_worms;
    int init_life_pts;
    int max_round_time;
    int packs_frequence; //entre 1-100
    int pts_per_life_pack;
    int error;
    int room_index;
    int friend_game;
} GameParameters;

#endif //GROUPE_8_GAMEPARAMETERS_H
