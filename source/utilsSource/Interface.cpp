/*#pragma clang diagnostic push
#pragma ide diagnostic ignored "hicpp-signed-bitwise" //todo : remove, just needed for CLION*/

#include "Interface.h"

Interface::Interface() {
    initscr();
    cbreak();
    noecho();
    printw("Lombrics Online: a Mind-Blowingly Ridiculous and Ironic Competition\n\n\n");
    int xMax, ymax;
    getmaxyx(stdscr, ymax, xMax);
    displayBox = newwin(ymax, xMax, 3, 0);
    keypad(displayBox, true);
    refresh();
}

Interface::~Interface() {
    endwin();
}

void Interface::clearWin() {
    wclear(displayBox);
}

void Interface::testPrint(char *buff) {
    wprintw(displayBox, "%s\n", buff);
    wrefresh(displayBox);
}

void Interface::wait() {
    wgetch(displayBox);
}

void Interface::getInput(char *buffer) {
    curs_set(1);
    echo();
    wrefresh(displayBox);
    wgetstr(displayBox, buffer);
}

void Interface::displayWelcomeMenu(int highlighted) {
    wclear(displayBox);

    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez une option:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Se connecter\n");
            wattroff(displayBox, A_STANDOUT);

            wprintw(displayBox, "        2: Créer un compte\n");
            wprintw(displayBox, "        3: Quitter\n\n\n");
            wprintw(displayBox, "Entrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez une option:\n");
            wprintw(displayBox, "        1: Se connecter\n");

            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Créer un compte\n");
            wattroff(displayBox, A_STANDOUT);

            wprintw(displayBox, "        3: Quitter\n\n\n");
            wprintw(displayBox, "Entrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez une option:\n");
            wprintw(displayBox, "        1: Se connecter\n");
            wprintw(displayBox, "        2: Créer un compte\n");

            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: Quitter\n\n\n");
            wattroff(displayBox, A_STANDOUT);

            wprintw(displayBox, "Entrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayPrincipalMenu(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "MENU PRINCIPAL\n");
            wprintw(displayBox, "       ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1. Jouer une partie\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "       2. Consulter le classement\n");
            wprintw(displayBox, "       3. Consulter un profil\n");
            wprintw(displayBox, "       4. Gérer le profil\n");
            wprintw(displayBox, "       5. Rejoindre le chat\n");
            wprintw(displayBox, "       6. Se déconnecter\n");
            wprintw(displayBox, "       7. Quitter le programme\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "MENU PRINCIPAL\n");
            wprintw(displayBox, "       1. Jouer une partie\n");
            wprintw(displayBox, "       ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2. Consulter le classement\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "       3. Consulter un profil\n");
            wprintw(displayBox, "       4. Gérer le profil\n");
            wprintw(displayBox, "       5. Rejoindre le chat\n");
            wprintw(displayBox, "       6. Se déconnecter\n");
            wprintw(displayBox, "       7. Quitter le programme\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "MENU PRINCIPAL\n");
            wprintw(displayBox, "       1. Jouer une partie\n");
            wprintw(displayBox, "       2. Consulter le classement\n");
            wprintw(displayBox, "       ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3. Consulter un profil\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "       4. Gérer le profil\n");
            wprintw(displayBox, "       5. Rejoindre le chat\n");
            wprintw(displayBox, "       6. Se déconnecter\n");
            wprintw(displayBox, "       7. Quitter le programme\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 4:
            wprintw(displayBox, "MENU PRINCIPAL\n");
            wprintw(displayBox, "       1. Jouer une partie\n");
            wprintw(displayBox, "       2. Consulter le classement\n");
            wprintw(displayBox, "       3. Consulter un profil\n");
            wprintw(displayBox, "       ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 4. Gérer le profil\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "       5. Rejoindre le chat\n");
            wprintw(displayBox, "       6. Se déconnecter\n");
            wprintw(displayBox, "       7. Quitter le programme\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 5:
            wprintw(displayBox, "MENU PRINCIPAL\n");
            wprintw(displayBox, "       1. Jouer une partie\n");
            wprintw(displayBox, "       2. Consulter le classement\n");
            wprintw(displayBox, "       3. Consulter un profil\n");
            wprintw(displayBox, "       4. Gérer le profil\n");
            wprintw(displayBox, "       ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 5. Rejoindre le chat\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "       6. Se déconnecter\n");
            wprintw(displayBox, "       7. Quitter le programme\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 6:
            wprintw(displayBox, "MENU PRINCIPAL\n");
            wprintw(displayBox, "       1. Jouer une partie\n");
            wprintw(displayBox, "       2. Consulter le classement\n");
            wprintw(displayBox, "       3. Consulter un profil\n");
            wprintw(displayBox, "       4. Gérer le profil\n");
            wprintw(displayBox, "       5. Rejoindre le chat\n");
            wprintw(displayBox, "       ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 6. Se déconnecter\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "       7. Quitter le programme\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 7:
            wprintw(displayBox, "MENU PRINCIPAL\n");
            wprintw(displayBox, "       1. Jouer une partie\n");
            wprintw(displayBox, "       2. Consulter le classement\n");
            wprintw(displayBox, "       3. Consulter un profil\n");
            wprintw(displayBox, "       4. Gérer le profil\n");
            wprintw(displayBox, "       5. Rejoindre le chat\n");
            wprintw(displayBox, "       6. Se déconnecter\n");
            wprintw(displayBox, "       ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 7. Quitter le programme\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayManageGameMenu(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Gestion de partie\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Créer une partie\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Rejoindre une partie\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Gestion de partie\n");
            wprintw(displayBox, "        1: Créer une partie\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Rejoindre une partie\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayEnemyChoiceMenu(int highlighted) {

    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez votre adversaires:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Inviter des amis\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Adversaires aléatoires\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez votre adversaires:\n");
            wprintw(displayBox, "        1: Inviter des amis\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Adversaires aléatoires\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayJoinGameMenu(int highlighted) {

    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Rejoindre une partie:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Rejoindre une partie aléatoire\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Rejoindre la partie d'un ami\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Rejoindre une partie:\n");
            wprintw(displayBox, "        1: Rejoindre une partie aléatoire\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Rejoindre la partie d'un ami\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}
void Interface::displayManageFLMenu(int highlighted){
    wclear(displayBox);


    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Gérer la liste d'amis:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Consulter la liste d'amis\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Ajouter un ami\n");
            wprintw(displayBox, "        3: Demandes d'amis en attente\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Gérer la liste d'amis:\n");
            wprintw(displayBox, "        1: Consulter la liste d'amis\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Ajouter un ami\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: Demandes d'amis en attente\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Gérer la liste d'amis:\n");
            wprintw(displayBox, "        1: Consulter la liste d'amis\n");
            wprintw(displayBox, "        2: Ajouter un ami\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: Demandes d'amis en attente\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;

    }
    wrefresh(displayBox);
}

void Interface::displayManageProfileMenu(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Gérer le profil:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Modifier les informations\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Gérer les lombrics\n");
            wprintw(displayBox, "        3: Gérer la liste d'amis\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Gérer le profil\n");
            wprintw(displayBox, "        1: Modifier les informations\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Gérer les lombrics\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: Gérer la liste d'amis\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Gérer le profil\n");
            wprintw(displayBox, "        1: Modifier les informations\n");
            wprintw(displayBox, "        2: Gérer les lombrics\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: Gérer la liste d'amis\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayFriendGameMenu(int highlighted){
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Voulez vous ouvrir une partie en mode ami ?\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Non\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Oui\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Voulez vous ouvrir une partie en mode ami ?\n");
            wprintw(displayBox, "        1: Non\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Oui\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayGameModeMenu(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez le mode de jeu:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Match à mort\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Match à mort par équipe\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez le mode de jeu:\n");
            wprintw(displayBox, "        1: Match à mort\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Match à mort par équipe\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayTimeFilterMenu(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez un filtre sur la durée:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "1: Depuis toujours\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Ce mois-ci\n");
            wprintw(displayBox, "        3: Cette semaine\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez un filtre sur la durée:\n");
            wprintw(displayBox, "        1: Depuis toujours\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "2: Ce mois-ci\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: Cette semaine\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez un filtre sur la durée:\n");
            wprintw(displayBox, "        1: Depuis toujours\n");
            wprintw(displayBox, "        2: Ce mois-ci\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "3: Cette semaine\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
}

void Interface::displayNoOfPlayers(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez le nombre de joueurs:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: 2\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: 3\n");
            wprintw(displayBox, "        3: 4\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez le nombre de joueurs:\n");
            wprintw(displayBox, "        1: 2\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: 3\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: 4\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez le nombre de joueurs:\n");
            wprintw(displayBox, "        1: 2\n");
            wprintw(displayBox, "        2: 3\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: 4\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayNoOfWorms(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez le nombre de lombrics:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: 4\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: 5\n");
            wprintw(displayBox, "        3: 6\n");
            wprintw(displayBox, "        4: 7\n");
            wprintw(displayBox, "        5: 8\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez le nombre de lombrics:\n");
            wprintw(displayBox, "        1: 4\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: 5\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: 6\n");
            wprintw(displayBox, "        4: 7\n");
            wprintw(displayBox, "        5: 8\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez le nombre de lombrics:\n");
            wprintw(displayBox, "        1: 4\n");
            wprintw(displayBox, "        2: 5\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: 6\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        4: 7\n");
            wprintw(displayBox, "        5: 8\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 4:
            wprintw(displayBox, "Choisissez le nombre de lombrics:\n");
            wprintw(displayBox, "        1: 4\n");
            wprintw(displayBox, "        2: 5\n");
            wprintw(displayBox, "        3: 6\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 4: 7\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        5: 8\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 5:
            wprintw(displayBox, "Choisissez le nombre de lombrics:\n");
            wprintw(displayBox, "        1: 4\n");
            wprintw(displayBox, "        2: 5\n");
            wprintw(displayBox, "        3: 6\n");
            wprintw(displayBox, "        4: 7\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 5: 8\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayGameModeFilterMenu(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez un filtre sur le mode de jeu:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "1: Match à mort\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Match à mort par équipe\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez un filtre sur le mode de jeu:\n");
            wprintw(displayBox, "        1: Match à mort\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "2: Match à mort par équipe\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }

    wrefresh(displayBox);

}

void Interface::displayInitialLifePts(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez le nombre de points de vie initiaux:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: 100\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: 125\n");
            wprintw(displayBox, "        3: 150\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez le nombre de points de vie initiaux:\n");
            wprintw(displayBox, "        1: 100\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: 125\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: 150\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez le nombre de points de vie initiaux:\n");
            wprintw(displayBox, "        1: 100\n");
            wprintw(displayBox, "        2: 125\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: 150\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displaySystemFilterMenu(int highlighted) {

    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez un filtre sur le système de classement:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "1: Classement général\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Classement par ratio de victoire\n");
            wprintw(displayBox, "        3: Classement par temps de jeu\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez un filtre sur le système de classement:\n");
            wprintw(displayBox, "        1: Classement général\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "2: Classement par ratio de victoire\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: Classement par temps de jeu\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez un filtre sur le système de classement:\n");
            wprintw(displayBox, "        1: Classement général\n");
            wprintw(displayBox, "        2: Classement par ratio de victoire\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "3: Classement par temps de jeu\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayReplayChoiceMenu(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox,
                    "Voulez vous vous baser sur un replay ? (vos paramètres de partie ne seront pas validés)\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " Oui\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        Non\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox,
                    "Voulez vous vous baser sur un replay ? (vos paramètres de partie ne seront pas validés)\n");
            wprintw(displayBox, "        Oui\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "Non\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayRoundTimeLimit(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez le temps max d'un tour (en seconde):\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1:30\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: 60\n");
            wprintw(displayBox, "        3: 80\n");
            wprintw(displayBox, "        4: 120\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez le temps max d'un tour (en seconde):\n");
            wprintw(displayBox, "        1: 30\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: 60\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: 80\n");
            wprintw(displayBox, "        4: 120\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez le temps max d'un tour (en seconde):\n");
            wprintw(displayBox, "        1: 30\n");
            wprintw(displayBox, "        2: 60\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: 80\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        4: 120\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 4:
            wprintw(displayBox, "Choisissez le temps max d'un tour (en seconde):\n");
            wprintw(displayBox, "        1: 30\n");
            wprintw(displayBox, "        2: 60\n");
            wprintw(displayBox, "        3: 80\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 4: 120\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayPacksFrequence(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez le taux d'apparition des caisses de ravitaillement:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: 0%\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: 25%\n");
            wprintw(displayBox, "        3: 50%\n");
            wprintw(displayBox, "        4: 75%\n");
            wprintw(displayBox, "        5: 100%\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez le taux d'apparition des caisses de ravitaillement:\n");
            wprintw(displayBox, "        1: 0%\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: 25%\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: 50%\n");
            wprintw(displayBox, "        4: 75%\n");
            wprintw(displayBox, "        5: 100%\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez le taux d'apparition des caisses de ravitaillement:\n");
            wprintw(displayBox, "        1: 0%\n");
            wprintw(displayBox, "        2: 25%\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: 50%\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        4: 75%\n");
            wprintw(displayBox, "        5: 100%\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 4:
            wprintw(displayBox, "Choisissez le taux d'apparition des caisses de ravitaillement\n");
            wprintw(displayBox, "        1: 0%\n");
            wprintw(displayBox, "        2: 25%\n");
            wprintw(displayBox, "        3: 50%\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 4: 75%\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        5: 100%\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 5:
            wprintw(displayBox, "Choisissez le taux d'apparition des caisses de ravitaillement\n");
            wprintw(displayBox, "        1: 0%\n");
            wprintw(displayBox, "        2: 25%\n");
            wprintw(displayBox, "        3: 50%\n");
            wprintw(displayBox, "        4: 75%\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 5: 100%\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);
}

void Interface::displayPtsPerLifePack(int highlighted) {
    wclear(displayBox);
    wrefresh(displayBox);
    switch (highlighted) {
        case 1:
            wprintw(displayBox, "Choisissez le nombre de points de vie dans une caisse de soin:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: 25\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: 50\n");
            wprintw(displayBox, "        3: 75\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez le nombre de points de vie dans une caisse de soin:\n");
            wprintw(displayBox, "        1: 25\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: 50\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: 75\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez le nombre de points de vie dans une caisse de soin:\n");
            wprintw(displayBox, "        1: 25\n");
            wprintw(displayBox, "        2: 50\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: 75\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        default:
            break;
    }
    wrefresh(displayBox);

}

void Interface::displayFriendRequestResponse(int highlighted){
    wclear(displayBox);

    switch(highlighted){
        case 1:
            wprintw(displayBox, "Choisissez votre action:\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 1: Accepter\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        2: Refuser\n");
            wprintw(displayBox, "        3: Annuler\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 2:
            wprintw(displayBox, "Choisissez votre action:\n");
            wprintw(displayBox, "        1: Accepter\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 2: Refuser\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "        3: Annuler\n");
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
        case 3:
            wprintw(displayBox, "Choisissez votre action:\n");
            wprintw(displayBox, "        1: Accepter\n");
            wprintw(displayBox, "        2: Refuser\n");
            wprintw(displayBox, "        ");
            wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, " 3: Annuler\n");
            wattroff(displayBox, A_STANDOUT);
            wprintw(displayBox, "\n\n\nEntrer pour confirmer");
            break;
    }
}

void Interface::displayFriendList(char** friends, int noOfFriends, int highlighted, bool friendRequest) {
    wclear(displayBox);
    if(!friendRequest) wprintw(displayBox, "Liste d'amis:\n\n");
    else wprintw(displayBox, "Liste des demandes amis:\n\n");
    for (int i = 0; i < noOfFriends; i++) {
        wprintw(displayBox, "       ");
        if (highlighted - 1 == i) wattron(displayBox, A_STANDOUT);
        wprintw(displayBox, "%d: %s\n", i + 1, friends[i]);
        if (highlighted - 1 == i) wattroff(displayBox, A_STANDOUT);
    }
    wprintw(displayBox, "\n\n\nAppuyer sur une touche pour choisir");
    wrefresh(displayBox);
}

void Interface::displayRanking(std::vector<std::vector<std::string>> &ranking, unsigned int highlighted){

    wclear(displayBox);
    wprintw(displayBox, "Classement:\n\n\n");
    for (unsigned int i=0; i<ranking.size(); i++){
        wprintw(displayBox, "       ");
        if(highlighted-1 == i) wattron(displayBox, A_STANDOUT);

        wprintw(displayBox, "%d: ",i);
        for (unsigned int j=0; j<ranking[i].size(); j++){
            wprintw(displayBox, " %s", ranking[i][j].c_str());
        }
        wprintw(displayBox, "\n");
        if(highlighted-1== i) wattroff(displayBox, A_STANDOUT);

    }
    wprintw(displayBox, "\n\n\nAppuyer sur une touche pour quitter");
    wrefresh(displayBox);
}

void Interface::displayFriendsOpenRooms(std::vector<std::vector<std::string>> friends,int noOfFriends, int highlighted) {
    wclear(displayBox);
    wprintw(displayBox, "Liste de lobby(s) ouvert(s) :\n\n");
    for (int i = 0; i < noOfFriends; i++) {
        for (unsigned int j=0;j<friends.size();j++){
            wprintw(displayBox, "       ");
            if (highlighted - 1 == i) wattron(displayBox, A_STANDOUT);
            wprintw(displayBox, "%d: %s\n", i + 1, friends[i][j].c_str());
            if (highlighted - 1 == i) wattroff(displayBox, A_STANDOUT);
        }
    }
    wprintw(displayBox, "\n\n\nAppuyer sur une touche pour quitter");
    wrefresh(displayBox);
}

int Interface::manageDisplay(int displayMenu, int caseNo) {

    curs_set(0); //hide cursor

    wclear(displayBox);
    wrefresh(displayBox);

    int highlighted = 1;
    int c;

    if (displayMenu == 0) displayWelcomeMenu(highlighted);
    else if (displayMenu == 1) displayPrincipalMenu(highlighted);
    else if (displayMenu == 2) displayManageGameMenu(highlighted);
    else if (displayMenu == 3) displayEnemyChoiceMenu(highlighted);
    else if (displayMenu == 4) displayJoinGameMenu(highlighted);
    else if (displayMenu == 5) displayGameModeMenu(highlighted);
    else if (displayMenu == 6) displayManageProfileMenu(highlighted);
    else if (displayMenu == 7) displayNoOfPlayers(highlighted);
    else if (displayMenu == 8) displayNoOfWorms(highlighted);
    else if (displayMenu == 9) displayInitialLifePts(highlighted);
    else if (displayMenu == 10) displayRoundTimeLimit(highlighted);
    else if (displayMenu == 11) displayPacksFrequence(highlighted);
    else if (displayMenu == 12) displayPtsPerLifePack(highlighted);
    else if (displayMenu == 13) displayTimeFilterMenu(highlighted);
    else if (displayMenu == 14) displayGameModeFilterMenu(highlighted);
    else if (displayMenu == 15) displaySystemFilterMenu(highlighted);
    else if (displayMenu == 16) displayFriendGameMenu(highlighted);
    else if (displayMenu == 17) displayFriendRequestResponse(highlighted);
    else if (displayMenu == 40) displayReplayChoiceMenu(highlighted);


    while ((c = wgetch(displayBox)) != ENTER) {

        if (c == KEY_UP && highlighted > 1) {

            if (displayMenu == 0) displayWelcomeMenu(--highlighted);
            else if (displayMenu == 1) displayPrincipalMenu(--highlighted);
            else if (displayMenu == 2) displayManageGameMenu(--highlighted);
            else if (displayMenu == 3) displayEnemyChoiceMenu(--highlighted);
            else if (displayMenu == 4) displayJoinGameMenu(--highlighted);
            else if (displayMenu == 5) displayGameModeMenu(--highlighted);
            else if (displayMenu == 6) displayManageProfileMenu(--highlighted);
            else if (displayMenu == 7) displayNoOfPlayers(--highlighted);
            else if (displayMenu == 8) displayNoOfWorms(--highlighted);
            else if (displayMenu == 9) displayInitialLifePts(--highlighted);
            else if (displayMenu == 10) displayRoundTimeLimit(--highlighted);
            else if (displayMenu == 11) displayPacksFrequence(--highlighted);
            else if (displayMenu == 12) displayPtsPerLifePack(--highlighted);
            else if (displayMenu == 13) displayTimeFilterMenu(--highlighted);
            else if (displayMenu == 14) displayGameModeFilterMenu(--highlighted);
            else if (displayMenu == 15) displaySystemFilterMenu(--highlighted);
            else if (displayMenu == 16) displayFriendGameMenu(--highlighted);
            else if (displayMenu == 17) displayFriendRequestResponse(highlighted);
            else if (displayMenu == 40) displayReplayChoiceMenu(--highlighted);

        } else if (c == KEY_DOWN && highlighted < caseNo) {

            if (displayMenu == 0) displayWelcomeMenu(++highlighted);
            else if (displayMenu == 1) displayPrincipalMenu(++highlighted);
            else if (displayMenu == 2) displayManageGameMenu(++highlighted);
            else if (displayMenu == 3) displayEnemyChoiceMenu(++highlighted);
            else if (displayMenu == 4) displayJoinGameMenu(++highlighted);
            else if (displayMenu == 5) displayGameModeMenu(++highlighted);
            else if (displayMenu == 6) displayManageProfileMenu(++highlighted);
            else if (displayMenu == 7) displayNoOfPlayers(++highlighted);
            else if (displayMenu == 8) displayNoOfWorms(++highlighted);
            else if (displayMenu == 9) displayInitialLifePts(++highlighted);
            else if (displayMenu == 10) displayRoundTimeLimit(++highlighted);
            else if (displayMenu == 11) displayPacksFrequence(++highlighted);
            else if (displayMenu == 12) displayPtsPerLifePack(++highlighted);
            else if (displayMenu == 13) displayTimeFilterMenu(++highlighted);
            else if (displayMenu == 14) displayGameModeFilterMenu(++highlighted);
            else if (displayMenu == 15) displaySystemFilterMenu(++highlighted);
            else if (displayMenu == 16) displayFriendGameMenu(++highlighted);
            else if (displayMenu == 17) displayFriendRequestResponse(highlighted);
            else if (displayMenu == 40) displayReplayChoiceMenu(++highlighted);
        }
    }
    return highlighted;
}

int Interface::welcomeMenu() {
    return manageDisplay(0, 3);
}

int Interface::chooseReplayID(std::vector<std::vector<std::string>> matchs){
    /*
    Fonction choisissant un ID de replay parmis les 10 derniers disponibles dans la base de données
    */
    return managechooseReplayID(matchs);
}

int Interface::managechooseReplayID(std::vector<std::vector<std::string>> matches){
    curs_set(0);
    wclear(displayBox);
    noecho();
    wrefresh(displayBox);
    int c;
    int highlighted = 0;
    displayLastServerGames(highlighted, matches);
    while ((c = wgetch(displayBox)) != ENTER){
        if(c == KEY_UP && highlighted > 0) displayLastServerGames(--highlighted, matches);

        else if (c == KEY_DOWN && highlighted < 10) displayLastServerGames(++highlighted, matches);
    }
    return highlighted; //+ID_of_oldest_game; // FIXME: modifier ID_of_oldest_game avec une valeur du vector
}


void Interface::displayLastServerGames(long unsigned highlighted, std::vector<std::vector<std::string>> matches){ //TODO: completer le 3 eme argument
    wclear(displayBox);

    wprintw(displayBox, "Choisissez parmis ces parties:\n\n");

    for (long unsigned i = 0; i < matches.size(); i++) {
        wprintw(displayBox, "       ");
        if(highlighted == i) wattron(displayBox, A_STANDOUT);

        for(long unsigned j=0; j<matches[0].size(); j++){
            wprintw(displayBox, matches[i][j].data());
            wprintw(displayBox, " ");
        }
        wprintw(displayBox, "\n");

        if(highlighted == i) wattroff(displayBox, A_STANDOUT);
    }
    wprintw(displayBox, "\n\n\nAppuyez sur Enter pour choisir");
    wrefresh(displayBox);
}

User_input Interface::loginMenu(bool error) {
    curs_set(1); //make cursor visible

    wclear(displayBox);
    wrefresh(displayBox);

    User_input user_input;
    char buffer[1024];


    int x, y, x0, y0;
    wprintw(displayBox, "Se connecter:\n");
    wprintw(displayBox, "      Nom d'utilisateur: ");
    getyx(displayBox, y, x);

    wprintw(displayBox, "\n      Mot de passe: ");
    getyx(displayBox, y0, x0);
    wprintw(displayBox, "\nAppuyez sur Esc ici pour retourner en arrière\n");

    if (error) {
        wattron(displayBox, A_STANDOUT);
        wprintw(displayBox, "\n\nNom d'utilisateur/mot de passe invalide\n");
        wattroff(displayBox, A_STANDOUT);
    }
    wprintw(displayBox, "\n\n\nEntrer pour confirmer\n");
    wrefresh(displayBox);

    echo();
    wmove(displayBox, y, x);
    wgetstr(displayBox, buffer); //get user input (username)

    user_input.username = buffer;

    noecho();
    wmove(displayBox, y0, x0);

    wgetstr(displayBox, buffer); //get user input (password)
    user_input.password = (buffer);

    if (wgetch(displayBox) == 27) { user_input.ret = true; }

    return user_input;
}

User_input Interface::registrationMenu(bool error) {
    curs_set(1); //make cursor visible

    wclear(displayBox);
    wrefresh(displayBox);

    User_input user_input;
    char buffer[INIT_TAILLE];

    int x, y, x0, y0, x1, y1;
    wprintw(displayBox, "Créer un compte:\n");
    wprintw(displayBox, "      Nom d'utilisateur: ");
    getyx(displayBox, y, x);

    wprintw(displayBox, "\n      Mot de passe: ");
    getyx(displayBox, y0, x0);

    wprintw(displayBox, "\n\n Entrer les noms des lombrics:\n");
    wprintw(displayBox, "   Lombric 1: ");
    getyx(displayBox, y1, x1);
    wprintw(displayBox, "\n   Lombric 2: \n");
    wprintw(displayBox, "   Lombric 3: \n");
    wprintw(displayBox, "   Lombric 4: \n");
    wprintw(displayBox, "   Lombric 5: \n");
    wprintw(displayBox, "   Lombric 6: \n");
    wprintw(displayBox, "   Lombric 7: \n");
    wprintw(displayBox, "   Lombric 8: \n");
    wprintw(displayBox, "Appuyez sur Esc ici pour retourner en arrière\n");

    if (error) {
        wattron(displayBox, A_STANDOUT);
        wprintw(displayBox, "\n\nLe nom d'utilisateur existe déjà\n");
        wattroff(displayBox, A_STANDOUT);
    }
    wprintw(displayBox, "\n\n\nEntrer pour confirmer\n");
    wrefresh(displayBox);

    echo();
    wmove(displayBox, y, x);
    wgetstr(displayBox, buffer); //get user input (username)

    user_input.username = buffer;


    noecho();
    wmove(displayBox, y0, x0);

    wgetstr(displayBox, buffer); //get user input (password)

    user_input.password = buffer;

    wmove(displayBox, y1, x1);
    echo();
    for (int i = 0; i < 8; i++) {
        wgetstr(displayBox, buffer);
        user_input.worms[i] = buffer;
        wmove(displayBox, ++y1, x1);
    }
    if (wgetch(displayBox) == 27) { user_input.ret = true; }
    return user_input;

}

int Interface::principalMenu() {

    return manageDisplay(1, 7);
}

int Interface::manageRoundMenu() {

    return manageDisplay(2, 2);

}

int Interface::enemyChoiceMenu() {

    return manageDisplay(3, 2);

}

int Interface::joinGameMenu() {
    return manageDisplay(4, 2);
}

int Interface::manageProfileMenu() {
    return manageDisplay(6, 3);
}

int Interface::replayModeChoice() {
    // %2 pour une conversion en bool simple : 1%2 == true, 2%2 == false
    return manageDisplay(40, 2) % 2;
}

void Interface::searchPlayer(char username[USERNAME_SIZE], bool error) {
    curs_set(1); //make cursor visible
    wclear(displayBox);
    wrefresh(displayBox);
    echo();

    int x, y;
    char buffer[INIT_TAILLE];

    wprintw(displayBox, "Rechercher un profil:\n");
    wprintw(displayBox, "   Entrer le nom: ");
    getyx(displayBox, y, x);

    if (error) {
        wattron(displayBox, A_STANDOUT);
        wprintw(displayBox, "\n\nCe joueur n'existe pas\n");
        wattroff(displayBox, A_STANDOUT);
    }

    wprintw(displayBox, "\n\n\nEnter pour confirmer\n");

    wrefresh(displayBox);
    wmove(displayBox, y, x);
    wgetstr(displayBox, buffer);

    strcpy(username, buffer); // TODO: test que ça marche correctement
}

void Interface::searchGame(char username[INIT_TAILLE], bool error) {

    curs_set(1); //make cursor visible
    wclear(displayBox);
    wrefresh(displayBox);
    echo();

    int x, y;
    char buffer[INIT_TAILLE];

    wprintw(displayBox, "Rechercher une partie:\n");
    wprintw(displayBox, "   Entrer le nom de la partie: ");
    getyx(displayBox, y, x);

    if (error) {
        wattron(displayBox, A_STANDOUT);
        wprintw(displayBox, "\n\nCette partie n'existe pas\n");
        wattroff(displayBox, A_STANDOUT);
    }

    wprintw(displayBox, "\n\n\nEnter pour confirmer\n");

    wrefresh(displayBox);
    wmove(displayBox, y, x);
    wgetstr(displayBox, buffer);

    strcpy(username, buffer); // TODO: test que ça marche correctement
}

void Interface::displayProfil(char username[USERNAME_SIZE], char worms[8][USERNAME_SIZE]) {
    curs_set(0); //make cursor invisible
    wclear(displayBox);
    wrefresh(displayBox);

    wprintw(displayBox, "Le profile du joueur:\n");
    wprintw(displayBox, "       Nom : %s\n", username);

    for (int i = 0; i < 8; i++) {
        wprintw(displayBox, "       Vers n°%d : %s\n", i + 1, worms[i]);
    }

    wprintw(displayBox, "\n\n\nAppuyer sur une touche pour quitter\n");
    wrefresh(displayBox);
    getch();

}

GameParameters Interface::chooseGameParameters() {
    curs_set(1);
    wclear(displayBox);
    wrefresh(displayBox);
    GameParameters parameters;

    parameters.friend_game = manageDisplay(16, 2);
    parameters.game_mode = manageDisplay(5, 2); //Mode de jeux
    parameters.no_of_players = manageDisplay(7, 3) + 1;
    parameters.no_of_worms = manageDisplay(8, 5) + 3;
    parameters.init_life_pts = (manageDisplay(9, 3) - 1) * 25 + 100;
    parameters.max_round_time = manageDisplay(10, 4) * 30;
    parameters.packs_frequence = (manageDisplay(11, 5) - 1) * 25;
    parameters.pts_per_life_pack = manageDisplay(12, 3) * 25;
    return parameters;
}

void Interface::ConnectionError() {

    curs_set(0);
    noecho();
    wclear(displayBox);
    wattron(displayBox, A_STANDOUT);
    wprintw(displayBox, "Connexion échouée\n\n\n");
    wattroff(displayBox, A_STANDOUT);
    wprintw(displayBox, "Appuyer sur une touche pour quitter\n");
    wrefresh(displayBox);
    getch();

}


Ranking_filter Interface::rankingMenu() {

    curs_set(1);
    wclear(displayBox);
    wrefresh(displayBox);
    Ranking_filter filters;

    filters.Time_filter = manageDisplay(13, 3); // filtre de temps
    filters.Game_mode_filter = manageDisplay(14, 2); // filtre de mode de jeu
    filters.Ranking_system_filter = manageDisplay(15, 3); // filtre du système de classement

    return filters;
}

void Interface::loadingGame() {

    curs_set(0);
    noecho();
    wclear(displayBox);
    wprintw(displayBox, "Partie en attente...\n\n");
    wrefresh(displayBox);
    getch();
}


int Interface::manageFriendList(char **friends, int noOfFriends, bool friendRequest){
    curs_set(0);
    wclear(displayBox);
    noecho();
    wrefresh(displayBox);
    int c;
    int highlighted = 1;

    if(noOfFriends == 0){
        if(!friendRequest) wprintw(displayBox, "\n\n\n Vous avez pas d'amis\n");
        else wprintw(displayBox, "\n\n\n Vous avez pas des demandes d'amis\n");
        wprintw(displayBox, "\n\n\nAppuyer sur une touche pour quitter\n");
        wgetch(displayBox);
        wrefresh(displayBox);
        return 0;
    }

    displayFriendList(friends, noOfFriends, highlighted, friendRequest);
    while ((c = wgetch(displayBox)) != ENTER){
        if(c == KEY_UP && highlighted > 1) displayFriendList(friends, noOfFriends, --highlighted, friendRequest);

        else if (c == KEY_DOWN && highlighted < noOfFriends) displayFriendList(friends, noOfFriends, ++highlighted, friendRequest);

    }

    return highlighted;
}

void Interface::manageRanking(std::vector<std::vector<std::string>> &ranking){
    curs_set(0);
    wclear(displayBox);
    noecho();

    int c;
    unsigned int highlighted = 1;
    displayRanking(ranking, highlighted);
    while ((c = wgetch(displayBox)) != ENTER){
        if(c == KEY_UP && highlighted > 1) displayRanking(ranking, --highlighted);

        else if (c == KEY_DOWN && highlighted < ranking.size()) displayRanking(ranking, ++highlighted);

    }
}

void Interface::writeMessage(char message[100]){

    curs_set(1);
    echo();
    wclear(displayBox);
    wprintw(displayBox, "\n\n\nWrite a message: ");
    wrefresh(displayBox);
    wgetstr(displayBox, message);

    wrefresh(displayBox);
}



void Interface::displayGameResult(int result) {
    curs_set(0);
    noecho();
    wclear(displayBox);
    if (result) wprintw(displayBox, "You won\n\n");
    else wprintw(displayBox, "You lost\n\n");
    wprintw(displayBox, "Appuyer sur une touche pour quitter\n");
    wrefresh(displayBox);
    getch();
}

int Interface::manageFLMenu(){

    return manageDisplay(16, 3);
}

int Interface::friendRequestResponse(){
    return manageDisplay(17, 3);
}

int Interface::manageFriendOpenRooms(std::vector<std::vector<std::string>> friends, int noOfFriends) {
    curs_set(0);
    wclear(displayBox);
    noecho();

    int c;
    int highlighted = 1;
    displayFriendsOpenRooms(friends, noOfFriends, highlighted);
    while ((c = wgetch(displayBox)) != ENTER) {
        if (c == KEY_UP && highlighted > 1) displayFriendsOpenRooms(friends, noOfFriends, --highlighted);

        else if (c == KEY_DOWN && highlighted < noOfFriends) displayFriendsOpenRooms(friends, noOfFriends, ++highlighted);

    }
    return highlighted;
}

void Interface::displaySearchingRandom(){
    curs_set(0);
    noecho();
    wclear(displayBox);
    wprintw(displayBox, "Partie aléatoire - recherche des lobbys ouverts...");
    wrefresh(displayBox);
}

void Interface::displaySearchingFriends(){
    curs_set(0);
    noecho();
    wclear(displayBox);
    wprintw(displayBox, "Partie entre amis - recherche des lobbys ouverts...");
    wrefresh(displayBox);
}

void Interface::displayNoFriendRoom(){
    curs_set(0);
    noecho();
    wclear(displayBox);
    wprintw(displayBox, "Aucun de vos amis n'a de partie ouverte.");
    wrefresh(displayBox);
    getch();
}

int Interface::manageFriendList(char **friends, int noOfFriends) {
    return manageFriendList(friends, noOfFriends, false);
}
//#pragma clang diagnostic pop
