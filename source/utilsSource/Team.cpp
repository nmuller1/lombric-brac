#include "Team.h"
#include <string>
#include <utility>

Team::Team() = default;

Team::Team(int teamSize, std::string teamName, Worm **worms) {
    this->teamSize = teamSize;
    this->teamName = std::move(teamName);
    this->worms = worms;
}

Team::~Team() {
    if (this->worms) {
        for (unsigned i = 0; i < 8; i++) {
            delete this->worms[i];
        }
        delete this->worms;
    }
}

Team::Team(Team &&team) { *this = std::move(team); }

Team &Team::operator=(Team &&team) {
    int test = 0;
    if (this != &team) {
        this->teamName = team.teamName;
        for (int i = 0; i < this->teamSize; i++) {
            delete this->worms[i];
            this->worms[i] = team.worms[i];
            test++;
        }
        if (team.teamSize < this->teamSize) {
            for (int i = test; i < this->teamSize; i++) {
                delete this->worms[i];
            }
        }
        delete this->worms;
        this->worms = team.worms;
        this->teamSize = team.teamSize;
        team.worms = nullptr;
    }
    return *this;
}

Worm **Team::getWorms() {
    return this->worms;
}

std::string Team::getTeamName() {
    return this->teamName;
}

void Team::setTeamName(std::string newName) {
    this->teamName = newName;
}

void Team::setWroms(Worm **worms) {
    this->worms = worms;
}
