#include "../gameSource/StraightWeapon.h"

#ifndef CONST_H
#define CONST_H

#define FRAMERATE 60
#define GRAVITY 400.0
#define MAP_DISPLAY_SIZE_VERT 80u
#define MAP_DISPLAY_SIZE_HORIZ 250u

#define INV_WIDTH 30u
#define INFO_HEIGHT 12u

#define SHOOT "32"

#define HANDGUN_ID "HdG"
#define GRENADE_ID "Grn"
#define BLOWTORCH_ID "BlT"

#define WEAPON_DAMAGE 100

#define HdGPostion 0
#define GrnPosition 1
#define BlTPosition 2

#define GAME_TIME_MAX 1200 // 1200s == 20 minutes

#define COLOR_AIR 1
#define COLOR_RED_PLAYER 2
#define COLOR_YELLOW_PLAYER 3
#define COLOR_BLACK_PLAYER 4
#define COLOR_BLUE_PLAYER 5
#define COLOR_DIRT 6
#define COLOR_ROCK 7
#define COLOR_RED_TEAM 8
#define COLOR_YELLOW_TEAM 9
#define COLOR_GREEN_TEAM 10
#define COLOR_BLUE_TEAM 11
#define COLOR_WATER 12
#define COLOR_BULLET 13

#endif //CONST_H