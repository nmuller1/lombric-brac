#ifdef __cplusplus
extern "C" {
#endif


#ifndef DATA_EXCHANGE
#define DATA_EXCHANGE

#include <errno.h>         // Un peu bourrin de tout include ici ?
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define TAILLE_BUFFER 1024
#define USERNAME_SIZE 16
#define PW_SIZE 16
#define MAX_NB_PLAYER 50

/*
Valeurs que peuvent prendre les paramètres de Ranking_filters:
    time : 
        (1) == depuis toujours
        (2) == sur les 30 derniers jours
        (3) == sur les 7 derniers jours
    gamemode : 
        (1) == match à mort
        (2) == match à mort en équipe
    evalutaionSystem : 
        (1) == ELO
        (2) == ratio de victoire
        (3) == temps de jeu
    ____________________________________________________________________

    error : (paramètre pour la gestion des communications client/serveur)
        -1 si problème de DataExchange 
*/

typedef struct {
    unsigned time;
    unsigned gamemode;
    unsigned evaluationSystem;
    int error;
} Ranking_filters;


int recvData(char *str_buffer, int socket_cible);

int sendData(char *str_buffer, int socket_cible);

void fakeNews(int socket_cible);

int receiveInt(int targetSocket, char *situation);

void sendInt(int toSend, int targetSocket, char *situation);

void receiveChar(char *charPtr, int targetSocket, char *situation);

void sendChar(char *toSend, int targetSocket, char *situation);

#endif


#ifdef __cplusplus
}
#endif