#ifndef VECTOR2U_H
#define VECTOR2U_H

#include <algorithm>

struct Vector2d {
    double x, y;

    Vector2d();

    Vector2d(double x, double y);

    ~Vector2d() = default;

    Vector2d *operator-(Vector2d subtractor);

    void operator*=(int multiplicator);

    void operator*=(double multiplicator);

    void normalizeTo(int ceiling);
};

#endif //VECTOR2U_H