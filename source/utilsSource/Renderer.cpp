#include "Renderer.h"
#include "CONST.h"

Renderer::Renderer(unsigned height, unsigned width, unsigned start_horiz, unsigned start_vert) {
    this->rWidth = width;
    this->rHeight = height;
    this->start_horiz = start_horiz;
    this->start_vert = start_vert;
    this->mapHeight = MAP_DISPLAY_SIZE_VERT;
    this->mapWidth = MAP_DISPLAY_SIZE_HORIZ;
}

Renderer::Renderer(unsigned height, unsigned width, unsigned start_horiz, unsigned start_vert, unsigned mapHeight,
                   unsigned mapWidth) {
    this->rWidth = width;
    this->rHeight = height;
    this->start_horiz = start_horiz;
    this->start_vert = start_vert;
    this->mapHeight = mapHeight;
    this->mapWidth = mapWidth;
}

void Renderer::Resize(unsigned height, unsigned width) {
    if (height > this->mapHeight) {
        this->rHeight = mapHeight;
    } else {
        this->rHeight = height;
    }

    if (width > this->mapWidth) {
        this->rWidth = this->mapWidth;
    } else {
        this->rWidth = width;
    }
}

void Renderer::MoveHoriz(unsigned startHoriz) {
    if (startHoriz > (this->mapWidth - this->rWidth)) {
        this->start_horiz = mapWidth - this->rWidth;
    } else {
        this->start_horiz = startHoriz;
    }
}

void Renderer::MoveVert(unsigned startVert) {
    if (startVert > (this->mapHeight - this->rHeight)) {
        this->start_vert = this->mapHeight - this->rHeight;
    } else {
        this->start_vert = startVert;
    }
}