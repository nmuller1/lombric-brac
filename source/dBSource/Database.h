#ifndef database_hpp
#define database_hpp

#include <stdio.h>
#include <string>
#include <sqlite3.h>
#include <vector>
#include <iostream>

class Database {
    std::string filename;

    sqlite3* connectToDatabase(std::string filename);

    void closeDatabase(sqlite3* db);

    void initTables();

    int resultlessQuery(const std::string& sql);

    int singleStringQuery(const std::string& sql, std::string& data);

    int singleNumberQuery(const std::string& sql);

    int multipleResultsQuery(const std::string& sql, std::vector<std::vector<std::string>>& data);

    int singleVectorQuery(const std::string& sql,std::vector<std::string>& vectorData, int flag);

    //int getUsernamesFromID(std::vector<std::string>& users);

    std::string quoteSQL(const std::string& data);

    std::string getFriendshipStatus(int requester1ID, int user2ID, int flag);

    void updateFriendshipStatus(int requesterID, int addresseeID, const std::string& status, int friendshipID=0);

public :

    explicit Database(std::string filename);

    int getID(const std::string& username);

    int getUsername(int userID, std::string& username);

    int newUser(const std::string& username, const std::string& password, std::string teamName, std::vector<std::string> wormsNames);

    int checkLogin(const std::string &username, const std::string &password);

    int getWormsNames(const std::string &username, std::vector<std::string>& wormsNames);

    int getTeamName(const std::string& username, std::string& teamName);

    int updateWormName(const std::string& username, const std::string& worm, const std::string& newName);

    int updateTeamName(const std::string& username, const std::string& newName);

    int getRanking(std::vector<std::vector<std::string>>& ranking,int gamemode=0);

    int newFriendRequest(const std::string& requester, const std::string& addressee);

    int updateFriendRequest(const std::string& requester, const std::string& addressee, const std::string& status);

    int deleteFriendShip(const std::string& requester, const std::string& addressee);

    int getFriendsList(const std::string& username, std::vector<std::string>& friendsList);

    int getReceivedFriendRequests(const std::string& username, std::vector<std::string>& friendRequests);

    int getSentFriendRequests(const std::string& username, std::vector<std::string>& friendRequests);

    int newMatch(int gameMode, int nbWorms, int initLifePoints, int medKitLifePoints, int MedKitFrequency, int maxRoundTime,  const std::string& user1, const std::string& user2);
    int newMatch(int gameMode, int nbWorms, int initLifePoints, int medKitLifePoints, int MedKitFrequency, int maxRoundTime,  const std::string& user1, const std::string& user2, const std::string& user3);
    int newMatch(int gameMode, int nbWorms, int initLifePoints, int medKitLifePoints, int MedKitFrequency, int maxRoundTime,  const std::string& user1, const std::string& user2, const std::string& user3, const std::string& user4);

    int updateMatchStatus(int matchID, int status);

    int getUserStats(int userID, std::vector<std::string>& stats, int mode = 0);

    int updateRanking(int userID, int mode, bool win);

    int getMatchStatus(int matchID, std::string& status);

    int getMatchs(const std::string& username, std::vector<std::vector<std::string>>& matches, int nb = 0, int gameMode = 0);

    int getToStringMatchs(const std::string& username, std::vector<std::vector<std::string>>& matches, int nb = 0, int gameMode = 0);

    int updateUserStatus(const std::string& username, int status, int roomID=0);

    int getAvailableFriendsMatchs(const std::string& username, std::vector<std::vector<std::string>>& friends);

    int getNbPlayers(int matchID);

    int getPlayers(int matchID, std::vector<std::string>& players);

    int getNbWorms(int matchID);

    int getInitlifePoints(int matchID);

    int getMedKitLifePoints(int matchID);

    int getMedKitFrequency(int matchID);

    int getMaxRoundTime(int matchID);

    int getGameMode(int matchID);

    int newMove(int matchID, const std::string& username, int turn, std::string& input);

    int getMove(int matchID, int turn, std::string& input);

    int getFriendshipID(const std::string& user1, const std::string& user2);

    void newMessage(int chatRoom, const std::string& message);

    int getChatMessages(int chatRoom, std::vector<std::string>& messages, int nb);
};

#endif /* database_hpp */