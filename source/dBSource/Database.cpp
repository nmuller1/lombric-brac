#include "Database.h"

Database::Database(std::string filename){
    this->filename = filename;
    initTables();
}

/*
 * \brief  connects to the database or create it if doesn't exist
 * \param  filename - path to database
 * \return db       - pointer to connexion to the database
 * */
sqlite3* Database::connectToDatabase(std::string filename) {
    sqlite3 *db;
    int rc;

    rc = sqlite3_open(filename.c_str(), &db); //ouvre la BDD ou la crée si elle n'existe pas encore
    if (rc) {
        fprintf(stderr, "Impossible d'ouvrir la base de données : %s\n", sqlite3_errmsg(db));
    }
    return db;
}

/*
 * \brief  closes the connexion to the database
 * \param  db - pointer to the connexion to the database
 * \return void
 * */
void Database::closeDatabase(sqlite3* db) {
    int rc;

    rc = sqlite3_close(db);
    if (rc) {
        fprintf(stderr, "Impossible de fermer la base de données : %s\n", sqlite3_errmsg(db));
    }
}

/*
 * \brief  inits the needed tables in the database
 * \param  none
 * \return void
 * */
void Database::initTables() {
    std::string sql;


    //Debug
    sql = "DROP TABLE IF EXISTS Users;";
    resultlessQuery(sql);
    sql = "DROP TABLE IF EXISTS Friendships;";
    resultlessQuery(sql);
    sql = "DROP TABLE IF EXISTS Chats;";
    resultlessQuery(sql);
    sql = "DROP TABLE IF EXISTS Matches;";
    resultlessQuery(sql);
    sql = "DROP TABLE IF EXISTS MatchSettings;";
    resultlessQuery(sql);
    sql = "DROP TABLE IF EXISTS Moves;";
    resultlessQuery(sql);
    sql = "DROP TABLE IF EXISTS Teams;";
    resultlessQuery(sql);
    sql = "DROP TABLE IF EXISTS Worms;";
    resultlessQuery(sql);

    sql = "CREATE TABLE IF NOT EXISTS Users ("
          "UserID INTEGER PRIMARY KEY AUTOINCREMENT,"
          "Username TEXT NOT NULL UNIQUE,"
          "Password TEXT NOT NULL,"
          "CreatedDateTime DATETIME NOT NULL,"
          "Status INTEGER,"
          "Room INTEGER,"
          "MatchesPlayed INTEGER,"
          "MatchesWon INTEGER,"
          "MatchesWinningRate INTEGER,"
          "StandardMatchesPlayed INTEGER,"
          "StandardMatchesWon INTEGER,"
          "StandardMatchesWinningRate INTEGER,"
          "TeamMatchesPlayed INTEGER,"
          "TeamMatchesWon INTEGER,"
          "TeamMatchesWinningRate INTEGER);";
    resultlessQuery(sql);

    sql = "CREATE TABLE IF NOT EXISTS Friendships ("
          "FriendshipID INTEGER PRIMARY KEY AUTOINCREMENT,"
          "RequesterID INTEGER NOT NULL,"
          "AddresseeID INTEGER NOT NULL,"
          "CreatedDateTime DATETIME NOT NULL,"
          "Status TEXT NOT NULL,"
          "CONSTRAINT FriendshipID_AK UNIQUE (RequesterID, AddresseeID),"
          "FOREIGN KEY(RequesterID) REFERENCES Users(UserID) ON DELETE CASCADE,"
          "FOREIGN KEY(AddresseeID) REFERENCES Users(UserID) ON DELETE CASCADE);";
    resultlessQuery(sql);

    sql = "CREATE TABLE IF NOT EXISTS Chats ("
          "MessageID INTEGER PRIMARY KEY AUTOINCREMENT,"
          "RoomID INTEGER NOT NULL,"
          "Message TEXT NOT NULL,"
          "CreatedDateTime DATETIME NOT NULL,"
          "FOREIGN KEY(RoomID) REFERENCES Friendship(FriendshipID));";
    resultlessQuery(sql);

    sql = "CREATE TABLE IF NOT EXISTS Matches ("
          "MatchID INTEGER PRIMARY KEY AUTOINCREMENT,"
          "User1 INTEGER NOT NULL,"
          "User2 INTEGER NOT NULL,"
          "User3 INTEGER,"
          "User4 INTEGER,"
          "Mode INTEGER NOT NULL,"
          "StartedDateTime DATETIME NOT NULL,"
          "Status INTEGER);";
    resultlessQuery(sql);

    sql = "CREATE TABLE IF NOT EXISTS MatchSettings ("
          "MatchID INTEGER PRIMARY KEY NOT NULL,"
          "NbPlayers INTEGER NOT NULL,"
          "NbWorms INTEGER NOT NULL,"
          "InitLifePoints INTEGER NOT NULL,"
          "MedKitLifePoints INTEGER,"
          "MedKitFrequency INTEGER,"
          "MaxRoundTime INTEGER NOT NULL);";
    resultlessQuery(sql);

    sql = "CREATE TABLE IF NOT EXISTS Moves ("
          "MatchID INTEGER,"
          "UserID INTEGER," //TODO : utile de stocker le joueur alors qu'on peut le recalculer facilement sur base de l'ordre de stockage des joueurs ?
          "Turn INTEGER NOT NULL,"
          "CreatedDateTime DATETIME NOT NULL,"
          "Move TEXT NOT NULL,"
          "PRIMARY KEY(MatchID, Turn),"
          "FOREIGN KEY(UserID) REFERENCES Users(UserID),"
          "FOREIGN KEY(MatchID) REFERENCES Matches(MatchID) ON DELETE CASCADE);";
    resultlessQuery(sql);

    sql = "CREATE TABLE IF NOT EXISTS Teams ("
          "TeamID INTEGER PRIMARY KEY AUTOINCREMENT,"
          "Name TEXT NOT NULL,"
          "UserID INTEGER NOT NULL,"
          "CONSTRAINT Teams_AK UNIQUE (Name, UserID));";
    resultlessQuery(sql);

    sql = "CREATE TABLE IF NOT EXISTS Worms ("
          "WormID INTEGER PRIMARY KEY AUTOINCREMENT,"
          "Name TEXT NOT NULL,"
          "TeamID TEXT NOT NULL,"
          "CONSTRAINT Worms_AK UNIQUE (Name, TeamID));";
    resultlessQuery(sql);
}

/*
 * \brief  executes resultless sql instructions and returns ID of the most recent successful insert into a table
 * \param  sql - instruction to execute
 * \return id - ID of the last insert (not to take into account if the sql instruction doesn't involve an insert)
 * */
int Database::resultlessQuery(const std::string& sql) {
    sqlite3* db;
    int rc, id;
    char *errMsg = nullptr;

    db = connectToDatabase(this->filename);
    rc = sqlite3_exec(db,sql.c_str(),nullptr,nullptr,&errMsg);
    id = sqlite3_last_insert_rowid(db);

    if (rc) {
        fprintf(stderr, "Impossible d'exécuter l'instruction : %s\n", errMsg);
        sqlite3_free(errMsg);
    }
    closeDatabase(db);
    return id;
}

/*
 * \brief  executes SQL query with single string result
 * \param  sql  - instruction to execute
 *         data - string meant to store the query's result
 * \return res - 0 if there is a result
 *             - -1 if there is not any result or result is null
 * */
int Database::singleStringQuery(const std::string& sql, std::string& data){
    sqlite3* db;
    sqlite3_stmt *stmt;
    int rc, res;

    db = connectToDatabase(this->filename);
    rc = sqlite3_prepare_v2(db,sql.c_str(),sql.length(), &stmt, nullptr);

    if (rc) {
        fprintf(stderr, "Impossible de préparer le statement : %s\n", sqlite3_errmsg(db));
    }

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
        fprintf(stderr, "Impossible d'effectuer la requête : %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(stmt);
    }
    if (rc == SQLITE_DONE) { //no result
        res = -1;
    }
    else if (sqlite3_column_type(stmt, 0) == SQLITE_NULL) { //result is NULL
        res = -1;
    }
    else { //valid result
        res = 0;
        data = (const char*)sqlite3_column_text(stmt, 0);
    }
    sqlite3_finalize(stmt);
    closeDatabase(db);
    return res;
}

/*
 * \brief  executes SQL query with single number result
 * \param  sql - instruction to execute
 * \return res - single number result
 *             - -1 if there is no result or result is null
 * */
int Database::singleNumberQuery(const std::string& sql) {
    sqlite3* db;
    sqlite3_stmt *stmt;
    int rc, res;

    db = connectToDatabase(this->filename);
    rc = sqlite3_prepare_v2(db,sql.c_str(),sql.length(), &stmt, nullptr);

    if (rc) {
        fprintf(stderr, "Impossible de préparer le statement : %s\n", sqlite3_errmsg(db));
    }

    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
        fprintf(stderr, "Impossible d'effectuer la requête : %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(stmt);
    }
    if (rc == SQLITE_DONE) { //no result
        res = -1;
    }
    else if (sqlite3_column_type(stmt, 0) == SQLITE_NULL) { //result is NULL
        res = -1;
    }
    else { //valid result
        res = sqlite3_column_int(stmt, 0);
    }
    sqlite3_finalize(stmt);
    closeDatabase(db);
    return res;
}

/*
 * \brief  executes SQL query returning multiple results
 * \param  sql - instruction to execute
 *         data - vector of vector of strings, every vector of strings represents a row and every string a column
 * \return res - 0 if there is some results
 *             - -1 if there is not any result or results are null
 * */
int Database::multipleResultsQuery(const std::string& sql, std::vector<std::vector<std::string>>& data){
    sqlite3* db;
    sqlite3_stmt *stmt;
    int rc,res=0;

    db = connectToDatabase(this->filename);
    rc = sqlite3_prepare_v2(db,sql.c_str(),sql.length(), &stmt, nullptr);

    if (rc) {
        fprintf(stderr, "Impossible de préparer le statement : %s\n", sqlite3_errmsg(db));
    }
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
        fprintf(stderr, "Impossible d'effectuer la requête : %s\n", sqlite3_errmsg(db));
    }
    if (rc == SQLITE_DONE) { //no result
        res = -1;
    }
    else if (sqlite3_column_type(stmt, 0) == SQLITE_NULL) { //result is NULL
        res = -1;
    }
    else{
        int cols = sqlite3_column_count(stmt);
        std::vector<std::string> row;
        do {
            for (int i = 0; i < cols; i++) {
                std::string value = (const char*)sqlite3_column_text(stmt,i);
                row.push_back(value);
            }
            data.push_back(row);
            row.clear();
        }
        while (sqlite3_step(stmt) == SQLITE_ROW);
    }
    sqlite3_finalize(stmt);
    closeDatabase(db);
    return res;
}

/*
 * \brief executes SQL query returning multiple results with only one column by row or unique row with many columns
 * \param sql  - instruction to execute
 *        data - vector of strings meant to store data
 *        flag - 0 if many rows with unique column
 *             - 1 if unique row with many columns
 * \return res  - 0 if there is some results
 *              - -1 if there is not any result or results are null
 *              */
int Database::singleVectorQuery(const std::string& sql,std::vector<std::string>& vectorData, int flag){
    std::vector<std::vector<std::string>> data;
    int res = multipleResultsQuery(sql, data);

    if (res == 0){
        int size = (flag == 0) ? data.size() : data[0].size();
        for (int i = 0; i < size; i++){
            std::string val = (flag == 0) ? data[i][0] : data[0][i];
            vectorData.push_back(val);
        }
    }
    return res;
}

/* Méthode non utilisée finalement
 *
 * \brief converts a vector of userID to usernames to get a printable form
 * \param data  - vector of userID (in string format) to convert
 * \return res  - 0 if there is some results
 *              - -1 if userID are invalids
 *
int Database::getUsernamesFromID(std::vector<std::string>& users){
    int res;
    for (unsigned int i = 0; i < users.size();i++){
        res = getUsername(std::stoi(users[i]),users[i]);
        if (res == -1){
            break;
        }
    }
    return res;
}
 */

/*
 * \brief  quotes the string to insert the variable in a sql query
 * \param  sql - string to insert in the sql query
 * \return string with quotes
 * */
std::string Database::quoteSQL(const std::string& sql) {
    return std::string("'") + sql + std::string("'");
}

/*
 * \brief  gets the unique ID of a given user
 * \param  username
 * \return userID - ID of the given user or -1 if username doesn't exist
 * */
int Database::getID(const std::string& username) {
    std::string sql = "SELECT UserID FROM Users WHERE Username = " + quoteSQL(username) + ";";
    int userID = singleNumberQuery(sql);
    return userID;
}

/*
 * \brief  gets the username of a given user
 * \param  userID - ID of the given user
 *         username - string meant to store the query's result
 * \return res - 0 if there is a result
 *             - -1 if there is not any result or result is null
 * */
int Database::getUsername(int userID, std::string& username) {
    int res;
    std::string sql = "SELECT Username FROM Users WHERE UserID = " + quoteSQL(std::to_string(userID)) + ";";
    res = singleStringQuery(sql,username);
    return res;
}

/*
 * \brief  inserts new row in Users table
 * \param  username, password - user's connexion data
 *         teamName           - team's name choosed by user
 *         wormsNames         - worms' names choosed by user
 * \return res - userID of it has been correctly inserted
 *             - -1 if username already exists
 *             */
int Database::newUser(const std::string& username, const std::string& password, std::string teamName, std::vector<std::string> wormsNames) {
    int userID = getID(username);
    if (userID == -1) {
        std::string sql;
        sql = "INSERT INTO Users VALUES (NULL, " + quoteSQL(username) + " , " + quoteSQL(password) +
              ", DATETIME('NOW'), '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');";
        userID = resultlessQuery(sql);
        sql = "INSERT INTO Teams (Name, UserID) VALUES (" + quoteSQL(teamName) + " , " + quoteSQL(std::to_string(userID)) + ");";
        int teamID = resultlessQuery(sql);
        for (unsigned int i = 0; i < 8; i++){ //Initialisation des worms
            sql = "INSERT INTO Worms (Name, TeamID) VALUES (" + quoteSQL(wormsNames[i]) + " , " + quoteSQL(std::to_string(teamID)) + ");";
            resultlessQuery(sql);
        }
    }
    return userID;
}

/*
 * \brief  checks if username and password corresponds to an existing user
 * \param  username, password
 * \return userID - userID of the given user if username and password corresponds to an existing user
 *                - -1 if the username doesn't exist or if it's not the correct password
 *                */
int Database::checkLogin(const std::string& username, const std::string& password) {
    int userID = getID(username);
    if (userID != -1) {
        std::string correctPassword;
        std::string sql = "SELECT Password FROM Users WHERE UserID = " + quoteSQL(std::to_string(userID)) + ";";
        singleStringQuery(sql,correctPassword);
        if (correctPassword != password){
            userID = -1;
        }
    }
    return userID;
}

/*
 * \brief  gets user's worms' names
 * \param  username
 *         wormsNames  - vector of strings meant to store the user's worms' names
 *         \return res - 0 if there is some results
 *                     - -1 if username is invalid
 *                     */
int Database::getWormsNames(const std::string& username, std::vector<std::string>& worms) {
    int res, userID = getID(username);
    if (userID != -1) {
        std::string sql = "SELECT W.Name FROM Worms W INNER JOIN Teams T ON W.TeamID = T.TeamID WHERE T.UserID = " + quoteSQL(std::to_string(userID)) + ";";
        res = singleVectorQuery(sql, worms,0);
    }
    else {
        res = -1;
    }
    return res;
}

/*
 * \brief  gets user's team's name
 * \param  username
 *         teamName  - string meant to store the user's team's name
 *         \return res - 0 if there is some results
 *                     - -1 if username is invalid
 *                     */
int Database::getTeamName(const std::string& username, std::string& teamName){
    int userID = getID(username);
    std::string sql = "SELECT Name FROM Teams WHERE UserID = " + quoteSQL(std::to_string(userID)) + ";";
    int res = singleStringQuery(sql, teamName);
    return res;
}

/*
 * \brief  updates user's worm's name
 * \param  username
 *         worm     - worm's current name
 *         newName  - worm's new name
 * \return res - 0 if the update has been correctly applied
 *             - -1 if the username is invalid
 *                     */
int Database::updateWormName(const std::string& username, const std::string& worm, const std::string& newName) {
    int res, userID = getID(username);
    if (userID != -1) {
        std::string sql = "SELECT W.WormID FROM Worms W INNER JOIN Teams T ON W.TeamID = T.TeamID WHERE T.UserID = " +
                          quoteSQL(std::to_string(userID)) + "AND W.Name = " + quoteSQL(worm) + ";";
        int wormID = singleNumberQuery(sql);
        sql = "UPDATE Worms SET Name = " + quoteSQL(newName) + "WHERE WormID = " + quoteSQL(std::to_string(wormID)) +
              ";";
        resultlessQuery(sql);
        res = 0;
    }
    else {
        res = -1;
    }
    return res;
}

int Database::updateTeamName(const std::string& username, const std::string& newName){
    int res, userID = getID(username);
    if (userID != -1) {
        std::string sql = "UPDATE Teams SET Name ="+ quoteSQL(newName) + "WHERE UserID =" + quoteSQL(std::to_string(userID)) + ";";
        resultlessQuery(sql);
        res = 0;
    }
    else {
        res = -1;
    }
    return res;
}

/*
 * \brief  return 100 first players of the ranking (with filter)
 * \param  ranking          - vector of vector of strings meant to store the 100 first players with their username,
 *                             created date time and their scores
 *         gamemode         - ranking's filter, 0 if none, 1 if standard mode and 2 if team's mode
 * \return res -  0 if there is some results
 *             - -1 if there is not any result or results are null
 *            */
int Database::getRanking(std::vector<std::vector<std::string>>& ranking, int gamemode) {
    std::string filter, matchesPlayed, matchesWon;
    switch (gamemode) {
        case 0:
            matchesPlayed = "MatchesPlayed";
            matchesWon = "MatchesWon";
            filter = "MatchesWinningRate";
            break;
        case 1:
            matchesPlayed = "StandardMatchesPlayed";
            matchesWon = "StandardMatchesWon";
            filter = "StandardMatchesWinningRate";
            break;
        case 2:
            matchesPlayed = "TeamMatchesPlayed";
            matchesWon = "TeamMatchesWon";
            filter = "TeamMatchesWinningRate";
            break;
    }
    std::string sql = "SELECT Username, CreatedDateTime, " + matchesPlayed + "," + matchesWon + "," + filter + " FROM Users ORDER BY " + filter + " DESC LIMIT 100;";
    int res = multipleResultsQuery(sql,ranking);
    return res;
}

/*
 * \brief  gets the unique friendship ID
 * \param  user1, user2 - users who want to chat
 * \return friendshipID - unique friendshipID or -1 if usernames are invalids
 * */
int Database::getFriendshipID(const std::string& user1, const std::string& user2) {
    int user1ID = getID(user1);
    int user2ID = getID(user2);
    std::string sql = "SELECT FriendshipID FROM Friendships WHERE RequesterID "
                      "IN (" + quoteSQL(std::to_string(user1ID)) + "," +
                      quoteSQL(std::to_string(user2ID)) + ") AND AddresseeID IN (" +
                      quoteSQL(std::to_string(user1ID)) + "," + quoteSQL(std::to_string(user2ID)) + ");";
    int friendshipID = singleNumberQuery(sql);
    return friendshipID;
}

/*
 * \brief  returns the last friendship interaction between two users
 * \param  user1ID, user2ID - users involved in the friend request
 *         flag - 0 if it has to be checked in both directions (user1D = requesterID and user2D = addresseeID and vice versa)
 *              - 1 if it only has to be checked in one way (user1D = requesterID and user2D = addresseeID)
 * \return friendshipStatus - 'A' for "accepted' if they are already friends
 *                          - 'D' for "declined" if the last interaction was a refused friend request
 *                          - 'R' for "requested" if there is a current pending request
 *                          - "C" for "cancelled" if the last interaction was a cancelled friendship or friend request
 *                          - 'N' for "null" if there is nothing to report
 *                         */
std::string Database::getFriendshipStatus(int user1ID, int user2ID, int flag){
    std::string sql,status;
    int res;

    if (flag == 0){
        sql = "SELECT Status FROM Friendships WHERE RequesterID "
              "IN (" + quoteSQL(std::to_string(user1ID)) + "," +
              quoteSQL(std::to_string(user2ID)) + ") AND AddresseeID IN (" +
              quoteSQL(std::to_string(user1ID)) + "," + quoteSQL(std::to_string(user2ID)) + ");";
    }
    else {
        sql = "SELECT Status FROM Friendships WHERE RequesterID "
              "= " + quoteSQL(std::to_string(user1ID)) + " AND AddresseeID = " + quoteSQL(std::to_string(user2ID)) + ";";
    }
    res = singleStringQuery(sql,status);
    if (res == -1) {
        status = "N";
    }
    return status;
}

/*
 * \brief  updates friendship status between two users
 * \param  friendshipID         - unique users' relation identifier
 *         requester, addressee - users involved in the friend request
 *         status - "R" for a new friend request
 *                - "D" for a declined friend request
 *                - "A" for an accepted friend request
 *                - "C" for a cancelled friendship or friend request
 * \return void
 * */
void Database::updateFriendshipStatus(int requesterID, int addresseeID, const std::string& status, int friendshipID) {
    std::string sql, currentStatus;

    if (friendshipID == 0){
        sql = "INSERT INTO Friendships (RequesterID, AddresseeID, CreatedDateTime, Status) VALUES (" + quoteSQL(std::to_string(requesterID)) + "," + quoteSQL(std::to_string(addresseeID)) + ", DATETIME('NOW'), " + quoteSQL(status) + ");";
    }
    else{
        sql = "UPDATE Friendships SET RequesterID = " + quoteSQL(std::to_string(requesterID)) + ", AddresseeID = " +
              quoteSQL(std::to_string(addresseeID)) + ", CreatedDateTime = DATETIME('NOW'), Status = " + quoteSQL(status) +
              " WHERE FriendshipID = " + quoteSQL(std::to_string(friendshipID)) + ";";
    }
    resultlessQuery(sql);
}

/*
 * \brief  inserts a new friend request
 * \param  requester, addressee - users involved in the friend request
 * \return res - 0 if the friend request has been applied
 *             - -1 if usernames are invalids, if they are already friends or if there is already a pending friend request
 * */
int Database::newFriendRequest(const std::string& requester, const std::string& addressee){
    int res;
    std::string currentStatus;
    int requesterID = getID(requester);
    int addresseeID = getID(addressee);

    if (addresseeID == -1 || requesterID == -1) {
        res = -1;
    }
    else {
        currentStatus = getFriendshipStatus(requesterID, addresseeID,0);
        if (currentStatus != "A"  && currentStatus != "R"){
            updateFriendshipStatus(requesterID, addresseeID, "R");
            res = 0;
        }
        else {
            res = -1;
        }
    }
    return res;
}

/*
 * \brief  update the friend request (accept, decline or cancel)
 * \param  requester, addressee - users involved in the friend request
 *         status - "D" for a declined friend request
 *                - "A" for an accepted friend request
 *                - "C" for a cancelled friend request
 * \return res - 0 if the friend request update has been applied
 *             - -1 if usernames are invalids or if there is not any pending friends request in this direction
 *             */
int Database::updateFriendRequest(const std::string& requester, const std::string& addressee, const std::string& status){
    int res;
    std::string currentStatus;
    int friendshipID = getFriendshipID(requester,addressee);
    int requesterID = getID(requester);
    int addresseeID = getID(addressee);

    if (addresseeID == -1 || requesterID == -1 || friendshipID == -1) {
        res = -1;
    }
    else {
        currentStatus = getFriendshipStatus(requesterID, addresseeID,1);
        if (currentStatus == "R"){
            updateFriendshipStatus(requesterID, addresseeID, status, friendshipID);
            res = 0;
        }
        else {
            res = -1;
        }
    }
    return res;
}

/*
 * \brief  deletes a friendship between two users
 * \param  requester, addressee - users involved in the friendship delete
 * \return res - 0 if the friendship delete has been applied
 *             - -1 if usernames are invalids or if they are not friends
 * */
int Database::deleteFriendShip(const std::string& requester, const std::string& addressee){
    int res;
    std::string currentStatus;
    int friendshipID = getFriendshipID(requester,addressee);
    int requesterID = getID(requester);
    int addresseeID = getID(addressee);

    currentStatus = getFriendshipStatus(requesterID, addresseeID,0);
    if (currentStatus == "A"){
        updateFriendshipStatus(requesterID, addresseeID, "C",friendshipID);
        res = 0;
    }
    else {
        res = -1;
    }
    return res;
}

/*
 * \brief  gets friends list of a given user
 * \param  username
 *         friendsList - vector of strings meant to store the user's friends
 * \return res  - 0 if there is some results
 *              - -1 if there is not any result or results are null
 *              */
int Database::getFriendsList(const std::string& username, std::vector<std::string>& friendsList) {
    int userID = getID(username);
    std::string sql = "SELECT U.Username FROM Users U INNER JOIN Friendships F ON U.UserID = F.addresseeID WHERE F.status = 'A' AND F.requesterID = " +
                      quoteSQL(std::to_string(userID)) +
                      " UNION SELECT U.Username FROM Users U INNER JOIN Friendships F ON U.UserID = F.requesterID WHERE F.status = 'A' AND F.addresseeID = " +
                      quoteSQL(std::to_string(userID)) + ";";
    int res = singleVectorQuery(sql,friendsList,0);
    return res;
}

/*
 * \brief  gets user's received friend requests
 * \param  username
 *         friendRequests - vector of vector of strings meant to store the user's received friend requests
 * \return res  - 0 if there is some results
 *              - -1 if there is not any result or results are null
 *              */
int Database::getReceivedFriendRequests(const std::string& username, std::vector<std::string>& friendRequests) {
    int userID = getID(username);
    std::string sql = "SELECT U.Username FROM Users U INNER JOIN Friendships F ON U.UserID = F.requesterID WHERE F.status = 'R' AND F.addresseeID = " +
                      quoteSQL(std::to_string(userID)) + ";";
    int res = singleVectorQuery(sql,friendRequests,0);
    return res;
}

/*
 * \brief  gets user's sent friend requests
 * \param  username
 *         friendRequests - vector of vector of strings meant to store the user's sent friend requests
 * \return res  - 0 if there is some results
 *              - -1 if there is not any result or results are null
 *              */
int Database::getSentFriendRequests(const std::string& username, std::vector<std::string>& friendRequests) {
    int userID = getID(username);
    std::string sql = "SELECT U.Username FROM Users U INNER JOIN Friendships F ON U.UserID = F.addresseeID WHERE F.status = 'R' AND F.requesterID = " +
                      quoteSQL(std::to_string(userID)) + ";";
    int res = singleVectorQuery(sql,friendRequests,0);
    return res;
}

/*
 * \brief  inserts a new match in the database and returns the unique match ID
 * \param  gamemode                                              - 1 if 1v1 match
 *                                                               - 2 if teams match
 *         nbWorms, packLifePoints, initLifePoints, maxRoundTime - match settings
 *         user1, user2, (user3, user4) - users involved in the match
 * \return id - matchID if the match has correctly been inserted
 *            - -1 if users are invalids
 *              */
int Database::newMatch(int gameMode, int nbWorms, int initLifePoints, int medKitLifePoints, int medKitFrequency, int maxRoundTime,  const std::string& user1, const std::string& user2){
    std::string sql;
    int user1ID = getID(user1);
    int user2ID = getID(user2);
    int id;
    if (user1ID == -1 || user2ID == -1) {
        id = -1;
    }
    else {
        sql = "INSERT INTO Matches (User1, User2, User3, User4, Mode, StartedDateTime, Status) VALUES ( " +
              quoteSQL(std::to_string(user1ID)) + ", " + quoteSQL(std::to_string(user2ID)) + ", '0', '0', " +
              quoteSQL(std::to_string(gameMode)) + ", DATETIME('NOW'), '-1');";
        id = resultlessQuery(sql);
        sql = "INSERT INTO MatchSettings VALUES ( " + quoteSQL(std::to_string(id)) + ", '2', " +
              quoteSQL(std::to_string(nbWorms)) + ", " + quoteSQL(std::to_string(initLifePoints)) + ", " +
              quoteSQL(std::to_string(medKitLifePoints)) + ", " + quoteSQL(std::to_string(medKitFrequency)) + ", " +
              quoteSQL(std::to_string(maxRoundTime)) + ");";
        resultlessQuery(sql);
    }
    return id;
}

int Database::newMatch(int gameMode, int nbWorms, int initLifePoints, int medKitLifePoints, int medKitFrequency, int maxRoundTime,  const std::string& user1, const std::string& user2, const std::string& user3) {
    std::string sql;
    int user1ID = getID(user1);
    int user2ID = getID(user2);
    int user3ID = getID(user3);
    int id;
    if (user1ID == -1 || user2ID == -1 || user3ID == -1) {
        id = -1;
    }
    else {
        sql = "INSERT INTO Matches (User1, User2, User3, User4, Mode, StartedDateTime, Status) VALUES ( " +
              quoteSQL(std::to_string(user1ID)) + ", " + quoteSQL(std::to_string(user2ID)) + ", " +
              quoteSQL(std::to_string(user3ID)) + ", '0', " + quoteSQL(std::to_string(gameMode)) + ", DATETIME('NOW'), '-1');";
        id = resultlessQuery(sql);
        sql = "INSERT INTO MatchSettings VALUES ( " + quoteSQL(std::to_string(id)) + ", '3', " +
              quoteSQL(std::to_string(nbWorms)) + ", " + quoteSQL(std::to_string(initLifePoints)) + ", " +
              quoteSQL(std::to_string(medKitLifePoints)) + ", " + quoteSQL(std::to_string(medKitFrequency)) + ", " +
              quoteSQL(std::to_string(maxRoundTime)) + ");";
        resultlessQuery(sql);
    }
    return id;
}

int Database::newMatch(int gameMode, int nbWorms, int initLifePoints, int medKitLifePoints, int medKitFrequency, int maxRoundTime, const std::string& user1, const std::string& user2, const std::string& user3, const std::string& user4){
    std::string sql;
    int user1ID = getID(user1);
    int user2ID = getID(user2);
    int user3ID = getID(user3);
    int user4ID = getID(user4);
    int id;
    if (user1ID == -1 || user2ID == -1 || user3ID == -1 || user4ID == -1) {
        id = -1;
    }
    else {
        sql = "INSERT INTO Matches (User1, User2, User3, User4, Mode, StartedDateTime, Status) VALUES ( " +
              quoteSQL(std::to_string(user1ID)) + ", " + quoteSQL(std::to_string(user2ID)) + ", " +
              quoteSQL(std::to_string(user3ID)) + ", " + quoteSQL(std::to_string(user4ID)) + ", " +
              quoteSQL(std::to_string(gameMode)) + ", DATETIME('NOW'), '-1');";
        id = resultlessQuery(sql);
        sql = "INSERT INTO MatchSettings VALUES ( " + quoteSQL(std::to_string(id)) + ", '4', " +
              quoteSQL(std::to_string(nbWorms)) + ", " + quoteSQL(std::to_string(initLifePoints)) + ", " +
              quoteSQL(std::to_string(medKitLifePoints)) + ", " + quoteSQL(std::to_string(medKitFrequency)) + ", " +
              quoteSQL(std::to_string(maxRoundTime)) + ");";
        resultlessQuery(sql);
    }
    return id;
}

/*
 * \brief  updates status of the match
 * \param  match ID - unique match id generated where newMatch() is called
 *         status  - -1 if the match is running
 *                 - 0 if it's a draw
 *                 - 1 if the user1 won (or team1 for teams match)
 *                 - 2 if the user2 won (of team2 for teams match)
 *                 - 3 if the user3 won
 *                 - 4 if the user4 won
 * \return res - 0 if the update has been correctly applied
 *             - -1 if the matchID is invalid
 *             */
int Database::updateMatchStatus(int matchID, int status){
    std::string sql, currentStatus;
    int res;
    res = getMatchStatus(matchID,currentStatus);
    if (res != -1 and currentStatus == "-1") {
        sql = "UPDATE Matches SET Status = " + quoteSQL(std::to_string(status)) + "WHERE MatchID = " +
              quoteSQL(std::to_string(matchID)) + ";";
        resultlessQuery(sql);
        std::vector<std::string> players;
        getPlayers(matchID,players);
        int gameMode = getGameMode(matchID);
        unsigned int unsignedStatus = (status -1);
        bool win;
        for (unsigned int player = 0; player < players.size(); player++){
            if (gameMode == 0) {
                win = (player == unsignedStatus);
            }
            else {
                win = ((player+unsignedStatus)%2 == 0);
            }
            updateRanking(std::stoi(players[player]),gameMode,win);
        }
        res = 0;
    }
    else {
        res = -1;
    }
    return res;
}

/*
 * \brief  gets users statistics
 * \param  userID
 *         stats - vector of strings meant to store the users stats (with or without filter) in order - MatchesPlayed, MatchesWon, MatchesWinningRate,
 *                                                                                                      [StandardMatchesPlayed, StandardMatchesWon, StandardMatchesWinningRate],
 *                                                                                                      [TeamMatchesPlayed, TeamMatchesWon, TeamMatchesWinningRate]
 *         mode  - stats filter, 0 if no filter (default value), 1 if standard mode and 2 if teams mode
 * \return res - 0 if there are some results
 *             - -1 if userID is invalid
 * */
int Database::getUserStats(int userID, std::vector<std::string>& stats, int mode) {
    std::string sql;
    switch(mode){
        case 0 :
            sql = "SELECT MatchesPlayed, MatchesWon, MatchesWinningRate, StandardMatchesPlayed, StandardMatchesWon, StandardMatchesWinningRate, TeamMatchesPlayed, TeamMatchesWon, TeamMatchesWinningRate FROM Users WHERE UserID = " + quoteSQL(std::to_string(userID)) + ";";
            break;
        case 1 :
            sql = "SELECT MatchesPlayed, MatchesWon, MatchesWinningRate, StandardMatchesPlayed, StandardMatchesWon, StandardMatchesWinningRate FROM Users WHERE UserID = " + quoteSQL(std::to_string(userID)) + ";";
            break;
        case 2 :
            sql = "SELECT MatchesPlayed, MatchesWon, MatchesWinningRate, TeamMatchesPlayed, TeamMatchesWon, TeamMatchesWinningRate FROM Users WHERE UserID = " + quoteSQL(std::to_string(userID)) + ";";
            break;
    }
    int res = singleVectorQuery(sql,stats,1);
    return res;
}

/*
 * \brief  updates user's ranking after a match
 * \param  userID
 *         mode - 1 if standard mode and 2 if teams mode
 *         win  - True if match was won, False if not
 * \return res - 0 if the update has been correctly applied
 *             - -1 if the userID is invalid
 * */
int Database::updateRanking(int userID, int mode, bool win) {
    std::vector<std::string> stats;
    float matchesPlayed, matchesWon, winningRate, gameModeMatchesPlayed, gameModeMatchesWon, gameModeMatchesWinningRate;
    int res = getUserStats(userID,stats,mode);
    if (res != -1){
        matchesPlayed = std::stof(stats[0])+1;
        matchesWon = (win) ? std::stof(stats[1])+1 : std::stof(stats[1]);
        winningRate = matchesWon/matchesPlayed*100;
        gameModeMatchesPlayed = std::stof(stats[3])+1;
        gameModeMatchesWon = (win) ? std::stof(stats[4])+1 : std::stof(stats[4]);
        gameModeMatchesWinningRate = gameModeMatchesWon/gameModeMatchesPlayed*100;
        std::string sql;
        switch(mode) {
            case 1:
                sql = "UPDATE Users SET MatchesPlayed = " + quoteSQL(std::to_string(matchesPlayed)) +
                      ", MatchesWon = " + quoteSQL(std::to_string(matchesWon)) + ", MatchesWinningRate =" +
                      quoteSQL(std::to_string(winningRate)) + ", StandardMatchesPlayed = " +
                      quoteSQL(std::to_string(gameModeMatchesPlayed)) + ", StandardMatchesWon = " +
                      quoteSQL(std::to_string(gameModeMatchesWon)) + " , StandardMatchesWinningRate = " +
                      quoteSQL(std::to_string(gameModeMatchesWinningRate)) + " WHERE UserID = " +
                      quoteSQL(std::to_string(userID)) + ";";
                break;
            case 2:
                sql = "UPDATE Users SET MatchesPlayed = " + quoteSQL(std::to_string(matchesPlayed)) +
                      ", MatchesWon = " + quoteSQL(std::to_string(matchesWon)) + ", MatchesWinningRate = " +
                      quoteSQL(std::to_string(winningRate)) + ", TeamMatchesPlayed =" +
                      quoteSQL(std::to_string(gameModeMatchesPlayed)) + ", TeamMatchesWon =" +
                      quoteSQL(std::to_string(gameModeMatchesWon)) + ", TeamMatchesWinningRate = " +
                      quoteSQL(std::to_string(gameModeMatchesWinningRate)) + " WHERE UserID = " +
                      quoteSQL(std::to_string(userID)) + ";";
                break;
        }
        resultlessQuery(sql);
        res = 0;
    }
    return res;
}

/*
 * \brief  returns match status
 * \param  match ID - unique match id generated where newMatch() is called
 *         status  - string meant to store the query's result :
 *                    - -1 if the match is still running
 *                    - 0 if it's a draw
*                     - 1 if the user1 won (or team1 for teams match)
*                     - 2 if the user2 won (of team2 for teams match)
*                     - 3 if the user3 won
*                     - 4 if the user4 won
 * \return res - 0 if there is a result
 *             - -1 if the matchID is invalids
 *            */
int Database::getMatchStatus(int matchID, std::string& status){
    std::string sql;
    sql = "SELECT Status FROM Matches WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
    int res = singleStringQuery(sql,status);
    return res;
}

/*
 * \brief  returns user's played matches (with or without filter) with a numeric form
 * \param  ranking  - vector of vector of strings meant to store the user's match with values in the order : MatchID - User1 - User2 - User3 (can be null) - User4 (can be null) - GameMode - Started date time
 *         nb       - numbers of matches to returns
 *         gamemode - matches filter, 0 if no filter (default value), 1 if standard mode and 2 if teams mode
 * \return res -  0 if there is some results
 *             - -1 if there is not any result or results are null
 *            */
int Database::getMatchs(const std::string& username, std::vector<std::vector<std::string>>& matches, int nb, int gameMode){
    std::string sql;
    int userID = getID(username);
    if (gameMode == 0){
        if (nb == 0) {
            sql = "SELECT MatchID, User1, User2, User3, User4, Mode, StartedDateTime, Status FROM Matches WHERE Status!= '-1' AND (User1 = " +
                  quoteSQL(std::to_string(userID)) + " OR User2  = " + quoteSQL(std::to_string(userID)) +
                  " OR User3 = " + quoteSQL(std::to_string(userID)) + " OR User4  = " +
                  quoteSQL(std::to_string(userID)) + ");";
        }
        else {
            sql = "SELECT MatchID, User1, User2, User3, User4, Mode, StartedDateTime, Status FROM Matches WHERE Status!= '-1' AND (User1 = " +
                  quoteSQL(std::to_string(userID)) + " OR User2  = " + quoteSQL(std::to_string(userID)) +
                  " OR User3 = " + quoteSQL(std::to_string(userID)) + " OR User4  = " +
                  quoteSQL(std::to_string(userID)) + ") ORDER BY StartedDateTime DESC LIMIT " +
                  quoteSQL(std::to_string(nb)) + ";";
        }
    }
    else {
        if (nb == 0) {
            sql = "SELECT MatchID, User1, User2, User3, User4, Mode, StartedDateTime, Status FROM Matches WHERE Status!= '-1' AND Mode = " +
                  quoteSQL(std::to_string(gameMode)) + " AND (User1 = " + quoteSQL(std::to_string(userID)) +
                  " OR User2  = " + quoteSQL(std::to_string(userID)) + " OR User3 = " +
                  quoteSQL(std::to_string(userID)) + " OR User4  = " + quoteSQL(std::to_string(userID)) + ");";
        }
        else {
            sql = "SELECT MatchID, User1, User2, User3, User4, Mode, StartedDateTime, Status FROM Matches WHERE Status!= '-1' AND Mode = " +
                  quoteSQL(std::to_string(gameMode)) + " AND (User1 = " + quoteSQL(std::to_string(userID)) +
                  " OR User2  = " + quoteSQL(std::to_string(userID)) + " OR User3 = " +
                  quoteSQL(std::to_string(userID)) + " OR User4  = " + quoteSQL(std::to_string(userID)) +
                  ") ORDER BY StartedDateTime DESC LIMIT " + quoteSQL(std::to_string(nb)) + ";";
        }
    }
    int res = multipleResultsQuery(sql,matches);
    return res;
}

/*
 * \brief  returns user's played matches with a printable user friendly form (with or without filter)
 * \param  ranking          - vector of vector of strings meant to store the user's match with values in the order : User1 - User2 - User3 (can be null) - User4 (can be null) - GameMode - Started date time
 *         nb               - numbers of matches to returns
 *         gamemode         - matches filter, 0 if no filter (default value), 1 if standard mode and 2 if teams mode
 * \return res -  0 if there is some results
 *
 *             - -1 if there is not any result or results are null
 *            */
int Database::getToStringMatchs(const std::string& username, std::vector<std::vector<std::string>>& matches, int nb, int gameMode){
    int res = getMatchs(username, matches, nb, gameMode);
    std::string player;
    for (unsigned int row = 0; row < matches.size(); row++) {
        for (unsigned int user = 1; user < 5; user++) {
            if (matches[row][user] != "0") {
                getUsername(std::stoi(matches[row][user]), player);
                matches[row][user] = player;
            }
        }
        int winner = std::stoi(matches[row][7]);
        matches[row][5] = (gameMode == 1) ? "Standard Mode" : "Teams Mode";
        switch (winner){
            case -1 :
                matches[row][7] = "Match en cours";
                break;
            case 0 :
                matches[row][7] = "Match nul";
                break;
            case 1 :
                if (gameMode == 1)
                {
                    matches[row][7] = matches[row][1]; //User1 if the winner
                }
                else {
                    matches[row][7] = "Team 1";
                }
                break;
            case 2 :
                if (gameMode == 1){
                    matches[row][7] = matches[row][2]; //User2 if the winner
                }
                else {
                    matches[row][7] = "Team 2";
                }
                break;
            case 3:
                matches[row][7] = matches[row][3]; //User2 if the winner
                break;
            case 4:
                matches[row][7] = matches[row][4]; //User2 if the winner
                break;
        }
    }
    return res;
}

/*
 * \brief  updates user status (playing, ready to play,...)
 * \param  username
 *         status - 0 if user is not playing or anything
 *                - 1 if user is waiting for friends to play a friend match
 *                - 2 if user is playing
 *         roomID - unique roomID (which actually is the ID of the creator of the room) if user's status is not 0 (default value = 0)
 * \return res - 0 if the update has been correctly applied
 *             - -1 if the username or status and roomID are invalids
 *            */
int Database::updateUserStatus(const std::string& username, int status, int roomID){
    std::string sql;
    int res, userID = getID(username);
    if ((userID != -1) && ((status == 0 && roomID == 0) || (status != 0 && roomID != 0))) { //checks valids arguments
        sql = "UPDATE Users SET Status = " + quoteSQL(std::to_string(status)) + ",Room = " + quoteSQL(std::to_string(roomID)) + "WHERE UserID = " + quoteSQL(std::to_string(userID)) + ";";
        resultlessQuery(sql);
        res = 0;
    }
    else {
        res = -1;
    }
    return res;
}

/*
 * \brief  returns list of friend's matches that user can join and the id of the room
 * \param  username
 *         match  - vector of vector of strings meant to store the user's friends he can join for a match and the room where they are
 * \return res -  0 if there is some results
 *             - -1 if username is invalid or if there is not any friend ready to play
 *            */
int Database::getAvailableFriendsMatchs(const std::string& username, std::vector<std::vector<std::string>>& friends) {
    std::string sql;
    int res;
    int userID = getID(username);
    if (userID != -1) {
        sql = "SELECT F.addresseeID, U.Room FROM Friendships F INNER JOIN Users U ON F.addresseeID = U.UserID WHERE U.Status = '1' AND F.requesterID = " +
              quoteSQL(std::to_string(userID)) +
              " AND F.status = 'A' UNION SELECT F.requesterID, U.Room FROM Friendships F INNER JOIN Users U ON F.requesterID = U.UserID WHERE U.Status = '1' AND F.addresseeID = " +
              quoteSQL(std::to_string(userID)) + " AND F.status = 'A' ;";
        res = multipleResultsQuery(sql,friends);
    }
    else {
        res = -1;
    }
    return res;
}

/*
 * \brief  inserts a new move in the database
 * \param  match ID - unique match id generated where newMatch() is called
 *         user     - player who's played this move
 *         turn     - move's turn number
 *         input    - user's input
 * \return res -  0 if the move has been correctly inserted
 *             - -1 if matchID is invalid
 *              */
int Database::newMove(int matchID, const std::string& username, int turn, std::string& input){
    std::string status;
    int userID = getID(username);
    int res = getMatchStatus(matchID, status);
    if (res == 0 and status == "-1") {
        std::string sql = "INSERT INTO Moves VALUES ( " + quoteSQL(std::to_string(matchID)) + ", " +
                          quoteSQL(std::to_string(userID)) + ", " + quoteSQL(std::to_string(turn)) +
                          ", DATETIME('NOW'), " + quoteSQL(input) + ");";
        resultlessQuery(sql);
    }
    else {
        res = -1;
    }
    return res;
}

/*
 * \brief  returns played move at given turn
 * \param  matchID  - unique match id generated where newMatch() is called
 *         turn     - move's turn number
 * \return res -  0 if there is a result
 *             - -1 if matchID or turn is invalid
 *              */
int Database::getMove(int matchID, int turn, std::string& input) {
    std::string sql = "SELECT Move FROM Moves WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + " AND Turn = " +
                      quoteSQL(std::to_string(turn)) + ";";
    int res = singleStringQuery(sql, input);
    return res;
}

/*
 * \brief  gets the nb of players of a match
 * \param  matchID
 * \return nbPlayers or -1 if matchID is invalid
 * */
int Database::getNbPlayers(int matchID){
    std::string sql;
    sql = "SELECT NbPlayers FROM MatchSettings WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
    int nbPlayers = singleNumberQuery(sql);
    return nbPlayers;
}

/*
 * \brief  gets the players id of a match
 * \param  matchID
 *         players - vector of strings meant to store the players id
 * \return res - 0 if there is some results
 *             - -1 if matchID is invalid
 * */
int Database::getPlayers(int matchID, std::vector<std::string>& players){
    int res, nbPlayers = getNbPlayers(matchID);
    std::string sql;
    switch(nbPlayers) {
        case 2:
            sql = "SELECT User1, User2 FROM Matches WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
            break;
        case 3:
            sql = "SELECT User1, User2, User3 FROM Matches WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
            break;
        case 4:
            sql = "SELECT User1, User2, User3, User4 FROM Matches WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
            break;
    }
    res = singleVectorQuery(sql,players,1);
    return res;
}

/*
 * \brief  gets the nb of players of a match
 * \param  matchID
 * \return nbPlayers or -1 if matchID is invalid
 * */
int Database::getNbWorms(int matchID){
    std::string sql;
    sql = "SELECT NbWorms FROM MatchSettings WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
    int nbPlayers = singleNumberQuery(sql);
    return nbPlayers;
}


/*
 * \brief  gets the init life points of a match
 * \param  matchID
 * \return initLifePoints or -1 if matchID is invalid
 * */
int Database::getInitlifePoints(int matchID){
    std::string sql;
    sql = "SELECT InitLifePoints FROM MatchSettings WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
    int nbPlayers = singleNumberQuery(sql);
    return nbPlayers;
}

/*
 * \brief  gets the medkit life points
 * \param  matchID
 * \return medKitLifePoints or -1 if matchID is invalid
 * */
int Database::getMedKitLifePoints(int matchID){
    std::string sql;
    sql = "SELECT MedKitLifePoints FROM MatchSettings WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
    int nbPlayers = singleNumberQuery(sql);
    return nbPlayers;
}

/*
 * \brief  gets the medkit frequency
 * \param  matchID
 * \return medKitFrequency or -1 if matchID is invalid
 * */
int Database::getMedKitFrequency(int matchID){
    std::string sql;
    sql = "SELECT MedKitFrequency FROM MatchSettings WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
    int nbPlayers = singleNumberQuery(sql);
    return nbPlayers;
}

/*
 * \brief  gets the max round time
 * \param  matchID
 * \return maxRoundTime MaxRoundTime
 * */
int Database::getMaxRoundTime(int matchID){
    std::string sql;
    sql = "SELECT MaxRoundTime FROM MatchSettings WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
    int nbPlayers = singleNumberQuery(sql);
    return nbPlayers;
}

/*
 * \brief  gets the game mode
 * \param  matchID
 * \return gameMode - 1 for standard mode and 2 for teams mode
 * */
int Database::getGameMode(int matchID){
    std::string sql;
    sql = "SELECT Mode FROM Matches WHERE MatchID = " + quoteSQL(std::to_string(matchID)) + ";";
    int nbPlayers = singleNumberQuery(sql);
    return nbPlayers;
}

/*
 * \brief  inserts a new chat message in the database
 * \param  chatRoom - unique chat room id
 *         message  - format sender:message
 * \return void
 * */
void Database::newMessage(int chatRoom, const std::string& message){
    std::string sql = "INSERT INTO Chats (RoomID, Message, CreatedDateTime) VALUES ( " + quoteSQL(std::to_string(chatRoom)) + ", " +
                      quoteSQL(message) + ", DATETIME('NOW'));";
    resultlessQuery(sql);
}

int Database::getChatMessages(int chatRoom, std::vector<std::string>& messages, int nb) {
    std::string sql = "SELECT Message FROM Chats WHERE RoomID = " + quoteSQL(std::to_string(chatRoom)) + " ORDER BY CreatedDateTime DESC LIMIT " + quoteSQL(std::to_string(nb)) + ";";
    int res = singleVectorQuery(sql,messages,0);
    return res;
}
