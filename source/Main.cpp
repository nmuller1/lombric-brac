#include "utilsSource/CONST.h"
#include "gameSource/Worm.h"
#include "gameSource/Game.h"
#include <string>
#include <ncurses.h>
#include <iostream>

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "./lombric map.txt" << std::endl;
        exit(1);
    }

    std::string map_txt = argv[1];

    initscr();

    if (has_colors() == FALSE) {
        endwin();
        std::cout << "Your terminal does not support colors.\n" << std::endl;
        exit(1);
    }

    cbreak();
    keypad(stdscr, true);
    scrollok(stdscr, FALSE);
    noecho();
    start_color();
    init_pair(COLOR_AIR, COLOR_WHITE, COLOR_CYAN);
    init_pair(COLOR_RED_PLAYER, COLOR_RED, COLOR_CYAN);
    init_pair(COLOR_YELLOW_PLAYER, COLOR_YELLOW, COLOR_CYAN);
    init_pair(COLOR_BLACK_PLAYER, COLOR_GREEN, COLOR_CYAN);
    init_pair(COLOR_BLUE_PLAYER, COLOR_BLUE, COLOR_CYAN);
    init_pair(COLOR_DIRT, COLOR_WHITE, COLOR_GREEN);
    init_pair(COLOR_ROCK, COLOR_WHITE, COLOR_BLACK);
    init_pair(COLOR_BULLET, COLOR_BLACK, COLOR_CYAN);

    Game *game = new Game(map_txt);
    game->StartGame();
    delete game;
    endwin();
    return 0;
}