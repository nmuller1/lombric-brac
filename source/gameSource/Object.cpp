#include "Object.h"
#include <utility> //for std::move in the cpp


Object::Object() : name("Default") {}

Object::Object(std::string name) : name(std::move(name)) {}
//std::move because IDE suggested it


const inline std::string &Object::getName() const {
    return name;
}