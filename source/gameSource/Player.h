#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <unordered_set>
#include "InventorySlot.h"
#include "../utilsSource/Team.h"
#include "../utilsSource/Ranking.h"

class Team;

class Player {
    std::string pseudo;
    std::string password;
    int colour{};
    bool state = true;
    std::unordered_set<Player *> *friends{}; //TODO : changer en vector<string> cf. compatibilité avec la BDD
    Team *team{};
    //Ranking* ranking;

public:
    Player();

    Player(std::string pseudo, int colour);

    ~Player();

    std::string getPseudo();

    Team *getTeam();

    int getColour();

    void kill();

    bool isAlive();
};

#endif //PLAYER_H
