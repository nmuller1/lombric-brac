#include "InventorySlot.h"

InventorySlot::InventorySlot(const char *name, const char *identifier, int initQuantity)
        : itemName(name), itemId(identifier), quantity(initQuantity) {}
