//
// Created by Aloïs Glibert (000393192) on 26/02/20 .
//

#ifndef GROUPE_8_STRAIGHTWEAPON_H
#define GROUPE_8_STRAIGHTWEAPON_H


#include "Weapon.h"

class StraightWeapon : public Weapon {
public:
    StraightWeapon(const std::string &name, unsigned short baseDamage, unsigned short spread);

    Projectile *shoot(Physics *shooterPhysics, Vector2d *cursorPosition, unsigned short modifier) override;

    ~StraightWeapon() final;
};


#endif //GROUPE_8_STRAIGHTWEAPON_H
