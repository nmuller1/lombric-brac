#include "Physics.h"

#include <cmath>

#include "../utilsSource/CONST.h"

Physics::Physics() {
    this->position = new Vector2d();
    this->speed = new Vector2d();
    this->acceleration = new Vector2d();
}

Physics::~Physics() {
    delete acceleration;
    delete speed;
    delete position;
}

Vector2d *Physics::getPosition() {
    return this->position;
}

void Physics::setPosition(double newPositionX, double newPositionY) {
    this->position->x = newPositionX;
    this->position->y = newPositionY;
}

/*void Physics::setPositionX(int newPositionX) {
    this->position->x = newPositionX;
}

void Physics::setPositionY(int newPositionY) {
    this->position->y = newPositionY;
}*/

void Physics::setSpeed(double newSpeedX, double newSpeedY) {
    this->speed->x = newSpeedX;
    this->speed->y = newSpeedY;
}

void Physics::setSpeedX(double newSpeedX) {
    this->speed->x = newSpeedX;
}

void Physics::setSpeedY(double newSpeedY) {
    this->speed->y = newSpeedY;
}

/*void Physics::setAcceleration(int newAccelerationX, int newAccelerationY) {
    this->acceleration->x = newAccelerationX;
    this->acceleration->y = newAccelerationY;
}

void Physics::setAccelerationX(int newAccelerationX) {
    this->acceleration->x = newAccelerationX;
}*/

void Physics::setAccelerationY(double newAccelerationY) {
    this->acceleration->y = newAccelerationY;
}

void Physics::updatePosition() { // Update follows MRUA formula.
    this->updateSpeed();
    this->setPosition(
            this->position->x + this->speed->x / FRAMERATE + this->acceleration->x / 2 * pow(1.0 / FRAMERATE, 2),
            this->position->y + this->speed->y / FRAMERATE + this->acceleration->y / 2 * pow(1.0 / FRAMERATE, 2));
}

void Physics::updateSpeed() {
    this->setSpeed(this->speed->x + this->acceleration->x / FRAMERATE,
                   this->speed->y + this->acceleration->y / FRAMERATE);
}

void Physics::decreasePositionY() {
    this->position->y--;
}

void Physics::decreasePositionX() {
    this->position->x--;
}

void Physics::increasePositionY() {
    this->position->y++;
}

void Physics::increasePositionX() {
    this->position->x++;
}

