//
// Created by Aloïs Glibert (000393192) on 26/02/20 .
//

#ifndef GROUPE_8_PROJECTILE_H
#define GROUPE_8_PROJECTILE_H


#include "Physics.h"

class Projectile {
    Physics *physics;
    unsigned short damage;
    unsigned short spread;
public:
    Projectile(Vector2d shooterPosition, unsigned short damage, unsigned short spread);

    void nullifyGravity();

    Physics *getPhysics() const;

    void setTrajectory(Vector2d *direction, int modifier);

    unsigned short getDamage() const;

    void move();

    Vector2d *getPosition();
};


#endif //GROUPE_8_PROJECTILE_H
