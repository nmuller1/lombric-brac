#include "GunCrate.h"

GunCrate::GunCrate() : Crate('$') {
    srand((unsigned) time(nullptr));
    this->content = static_cast<Content>(rand() % Content::SIZE);
}

GunCrate::GunCrate(Content content) : Crate('$'), content(content) {}

Content GunCrate::getContent() {
    return this->content;
}
