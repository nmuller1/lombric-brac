//
// Created by Aloïs Glibert (000393192) on 25/03/20 .
//

#include "Inventory.h"
#include "../utilsSource/CONST.h"

Inventory::Inventory() : pouches(new InventorySlot *[3]), size(3) {
    pouches[0] = new InventorySlot("Arme de Poing", HANDGUN_ID, 10);
    pouches[1] = new InventorySlot("Grenade", GRENADE_ID, 1);
    pouches[2] = new InventorySlot("Chalumeau", BLOWTORCH_ID, 1);
}

Inventory::~Inventory() {
    for (int i = 0; i < size; i++) delete pouches[i];
    delete[]  pouches;
}
