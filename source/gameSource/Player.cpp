#include "Player.h"
#include "../utilsSource/CONST.h"

#include <utility>

Player::Player() = default;

Player::Player(std::string ps, int colour) : pseudo(std::move(ps)), colour(colour), state(true), team(new Team()) {
    //this->ranking = new Ranking();
}

Team *Player::getTeam() {
    return team;
}

Player::~Player() = default;

std::string Player::getPseudo() {
    return pseudo;
}

int Player::getColour() {
    return colour;
}

void Player::kill() {
    this->state = false;
}

bool Player::isAlive() {
    return state;
}
