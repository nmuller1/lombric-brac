#ifndef PHYSICS_H
#define PHYSICS_H

#include "../utilsSource/Vector2d.h"

class Physics {
    Vector2d *position;
    Vector2d *speed;
    Vector2d *acceleration;

public:
    Physics();

    ~Physics();

    Vector2d *getPosition();

    void setPosition(double newPositionX, double newPositionY);

    /*void setPositionX(int newPositionX);

    void setPositionY(int newPositionY);*/

    void increasePositionX();

    void increasePositionY();

    void decreasePositionX();

    void decreasePositionY();

    void setSpeed(double newSpeedX, double newSpeedY);

    void setSpeedX(double newSpeedX);

    void setSpeedY(double newSpeedY);

    /*void setAcceleration(int newAccelerationX, int newAccelerationY);

    void setAccelerationX(int newAccelerationX);*/

    void setAccelerationY(double newAccelerationY);

    void updatePosition();

    void updateSpeed();
};

#endif //PHYSICS_H