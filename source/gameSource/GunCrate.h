#ifndef GUNCRATE_H
#define GUNCRATE_H

#include "Crate.h"
#include "Object.h"
#include <ctime>

enum Content {
    Handgun, Grenade, Blowtorch, SIZE
};

class GunCrate : public Crate {
    Content content;
public:
    GunCrate();

    explicit GunCrate(Content content);

    ~GunCrate() final = default;

    Content getContent();
};

#endif //GUNCRATE_H