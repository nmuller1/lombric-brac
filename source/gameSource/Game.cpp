#include <chrono>
#include <cstdlib>
#include <ctime>
#include <random>
#include <utility>
#include <zconf.h>

#include "../utilsSource/CONST.h"
#include "../utilsSource/DataExchange.h"
#include "Game.h"
#include "GunCrate.h"
#include "Medkit.h"
#include "Physics.h"
#include "../clientSource/ProtocolClient.h"
#include "Projectile.h"
#include "StraightWeapon.h"
#include "ThrownWeapon.h"
#include "Utility.h"


Game::Game(const std::string &map_txt) : ch(' '), chosenObjectNumber(0), map(new Map(map_txt)),
                                         userInventory(new Inventory()) {}

Game::~Game() {
    delete this->map;
    delete userInventory;
    delwin(this->invView);
    delwin(this->infoView);
    erase();
}

void Game::setRoom(Room newRoom) {
    this->room = std::move(newRoom);
}

void Game::StartGame(int playerCount, int wormCount, Team **allTeams) {
    // Laisse INFO_HEIGHT lignes en bas de fenêtre pour l'affichage des pv, INV_WIDTH colonnes à droite pour l'inventaire.
    wclear(stdscr);
    curs_set(0);
    this->map->getRenderer()->Resize((unsigned) (LINES - INFO_HEIGHT), (unsigned) (COLS - INV_WIDTH));
    this->Render(stdscr);
    invView = derwin(stdscr, LINES, INV_WIDTH, 0, (int) (COLS - INV_WIDTH));
    UpdateInv();
    infoView = derwin(stdscr, INFO_HEIGHT, (int) (COLS - INV_WIDTH), (int) (LINES - INFO_HEIGHT), 0);
    UpdateInfo(playerCount, wormCount, allTeams, 0);
    wmove(stdscr, 0, 0);
    refresh();

    //RunGame();
}

void Game::RunGame(Worm *worm, int socket_cible, int turnTime) {
    mvwprintw(stdscr, 0, 0, "MY TURN TO PLAY"); //debug
    wrefresh(stdscr); //debug

    // variables de time_over
    bool time_over = false;
    time_t current_time, start_time = time(nullptr);
    bool moveRes = true; //variable de retour du mouvement d'un joueur : gestion des noyades


    //variables d'envoi d'info
    int res = 0;
    char str_buffer[TAILLE_BUFFER];


    // variables spécifiques à l'utilisation des armes
    bool objectUsed = false;
    int saveX = 0, saveY = 0, modifier = 0;


    //Mainloop:
    while (!time_over && this->ch != 'p') {

        ch = getch();
        switch (this->ch) {
            case 'z':
                wrefresh(stdscr);
                moveRes = moveWorm(worm, 2);
                break;
            case 's':
                wrefresh(stdscr);
                break;
            case 'q':
                wrefresh(stdscr);
                moveRes = moveWorm(worm, 0);
                break;
            case 'd':
                wrefresh(stdscr);
                moveRes = moveWorm(worm, 1);
                break;
            case 'i':
                if (this->map->getRenderer()->start_vert > 0) {
                    this->map->getRenderer()->MoveVert(this->map->getRenderer()->start_vert - 1);
                }
                this->Render(stdscr);
                break;
            case 'k':
                // Tests done in Move method.
                this->map->getRenderer()->MoveVert(this->map->getRenderer()->start_vert + 1);
                this->Render(stdscr);
                break;
            case 'j':
                if (this->map->getRenderer()->start_horiz > 0) {
                    this->map->getRenderer()->MoveHoriz(this->map->getRenderer()->start_horiz - 1);
                }
                this->Render(stdscr);
                break;
            case 'l':
                // Tests done in Move method.
                this->map->getRenderer()->MoveHoriz(this->map->getRenderer()->start_horiz + 1);
                this->Render(stdscr);
                break;
            case KEY_RESIZE:
                getmaxyx(stdscr, this->wHeight, this->wWidth);
                this->map->getRenderer()->Resize(this->wHeight - INFO_HEIGHT, this->wWidth - INV_WIDTH);
                this->Render(stdscr);
                break;
            case 't': //acts with a selected object
                current_time = time(nullptr);
                if (current_time - start_time < turnTime) {//uses an object
                    objectUsed = true;

                    const char *objectID = chooseObject();
                    Object *usedObject = constructObject(objectID);

                    useObjectOnActorScreen(worm, usedObject, saveX, saveY, modifier);

                    sendUsageData(saveX, saveY, modifier, objectID, socket_cible);

                    delete usedObject;
                }
                moveRes = gravity(worm); //au cas où on tire sous ses pieds
                break;
            default:
                break;
        }

        if (ch != 116 || ch != 101) sendInt(ch, socket_cible, const_cast<char *>("client, cas général"));

        // check fin de tour
        current_time = time(nullptr);
        if (current_time - start_time > turnTime ||
            objectUsed) {// si on a utiliser une arme, on se dit que c'est comme si le temps étaient écoulé (pas besoin de 2 signaux différents)
            time_over = true;
            strcpy(str_buffer, "TIME");
            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Echec à l'envoi de l'information de fin de tour.(runGame)\n");
            }
        }
        if (!moveRes) { //un vers s'est noyé, son tour est fini
            time_over = true;
            strcpy(str_buffer, "TIME");
            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Impossible d'envoyer la fin d'un tour par noyade\n");
            }
        }
    }
}

void Game::useObjectOnActorScreen(Worm *worm, Object *usedObject, int &sentX, int &sentY, int &modifier) {
    auto weapon = dynamic_cast<Weapon *>(usedObject);

    Vector2d *aim;
    aim = viewFinder((int) worm->getPosition()->x, (int) worm->getPosition()->y);

    // sauvegarde des positions pour l'envoi des coordonnés après l'envoi de 'SHOOT'
    sentX = (int) aim->x;
    sentY = (int) aim->y;

    if (weapon) shootOnActorScreen(worm, weapon, aim, modifier);
    else {
        auto utility = dynamic_cast<Utility *>(usedObject);
        useUtility(worm, utility, aim);
        modifier = 0;
    }
}

const char *Game::chooseObject() {
    cursor[0] = 3;
    cursor[1] = 1;
    bool youChoseWisely = false;
    InventorySlot **pouches = userInventory->getPouches();
    while (!youChoseWisely) {
        moveCursor('\n', invView, userInventory->getSize() + 2, 3);
        chosenObjectNumber = cursor[0] - 3;
        if (pouches[chosenObjectNumber]->getQuantity() > 0) youChoseWisely = true;
        else {
            mvwaddstr(invView, userInventory->getSize() + 4, 1, "Choisir une poche pleine");
        }

    }
    const char *objectID = pouches[chosenObjectNumber]->getItemId().c_str();

    pouches[chosenObjectNumber]->deplete();
    UpdateInv();
    return objectID;
}

void Game::shootOnActorScreen(Worm *worm, Weapon *usedWeapon, Vector2d *aim, int &modifier) {

    modifier = powerChoose((int) worm->getPosition()->x, (int) worm->getPosition()->y);

    usingWeapon(worm, usedWeapon, aim, modifier);
}

void Game::printOnInvView(const std::string &message) const {
    wprintw(invView, message.c_str());
    wrefresh(invView);
}

void Game::usingWeapon(const Worm *worm, Weapon *weapon, Vector2d *aim, unsigned short modifier) {
    Projectile *bullet;
    bullet = weapon->shoot(worm->getPhysics(), aim, modifier);

    Vector2d *bulletPosition;
    bulletPosition = bullet->getPhysics()->getPosition();

    map->getMatrix()[(int) bulletPosition->y][(int) bulletPosition->x]->setProjectile(bullet);
    Render(stdscr);

    usleep(500000);
    bulletMover(bullet, bulletPosition, 0);
    delete bullet;
}

void Game::bulletMover(Projectile *bullet, const Vector2d *bulletPosition, unsigned short maxDist) {
    int maxHeight, maxWidth ;
    unsigned short traveled = 0;
    bool limitedTravel = maxDist;
    maxHeight = (int) map->getRenderer()->mapHeight;
    maxWidth = (int) map->getRenderer()->mapWidth;
    std::chrono::steady_clock::time_point startTime, endTime;
    double maxDuration;
    maxDuration = 1.0 / FRAMERATE;
    std::chrono::duration<double> timeSpan{};
    while (traveled <= maxDist) {
        startTime = std::chrono::steady_clock::now();
        int bulletStartingPositionY = (int) bulletPosition->y;
        int bulletStartingPositionX = (int) bulletPosition->x;
        Crate *cellCrate = map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->getCrate();
        Worm *cellWorm = map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->getWorm();
        CellType cellType = map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->getType();
        if (cellWorm) {
            cellWorm->modifyHealthBy(-(bullet->getDamage()));
            map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->setProjectile(nullptr);
            if (cellWorm->getHealth() <= 0) {
                map->getMatrix()[(int) bulletPosition->y][(int) bulletPosition->x]->getWorm()->kill();
                map->getMatrix()[(int) bulletPosition->y][(int) bulletPosition->x]->setWorm(nullptr);
            }
            Render(stdscr);
            break;
        }

        if (cellCrate){
            map->getMatrix()[(int) bulletPosition->y][(int) bulletPosition->x]->setCrate(nullptr);
            map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->setProjectile(nullptr);
            delete cellCrate;
            Render(stdscr);
            break;
        }

        if (cellType != AIR) {//todo add spread handling
            if (cellType == DIRT) map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->setType(AIR);
            map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->setProjectile(nullptr);
            Render(stdscr);
            break;
        }

        bullet->move();

        int bulletEndingPositionY = (int) bulletPosition->y;
        int bulletEndingPositionX = (int) bulletPosition->x;
        if (bulletEndingPositionY >= maxHeight || bulletEndingPositionY < 0 ||
            bulletEndingPositionX >= maxWidth || bulletEndingPositionX < 0) {
            map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->setProjectile(nullptr);
            Render(stdscr);
            break;
        } else {
            if (bulletEndingPositionX != bulletStartingPositionX || bulletEndingPositionY != bulletStartingPositionY) {
                map->getMatrix()[bulletStartingPositionY][bulletStartingPositionX]->setProjectile(nullptr);
                map->getMatrix()[bulletEndingPositionY][bulletEndingPositionX]->setProjectile(bullet);
                if (limitedTravel) traveled ++;
            }
            Render(stdscr);
            endTime = std::chrono::steady_clock::now();
            timeSpan = std::chrono::duration_cast<std::chrono::duration<double>>(endTime - startTime);
            if (timeSpan.count() < maxDuration) {
                usleep((int) ((maxDuration - timeSpan.count()) * 1000000));
            }
        }
    }
}

Vector2d *Game::viewFinder(int shooterX, int shooterY) {
    cursor[0] = shooterY;
    cursor[1] = shooterX;
    moveCursor('t', stdscr, 0, 0);
    auto aim = new Vector2d((int) cursor[1], (int) cursor[0]);
    return aim;
}

void Game::moveCursor(char endSig, WINDOW *winToMoveOn, unsigned short lowerLimit, unsigned short upperLimit) {
    wmove(winToMoveOn, (int) cursor[0], (int) cursor[1]);
    curs_set(1);
    wrefresh(winToMoveOn);
    if (!lowerLimit) lowerLimit = winToMoveOn->_maxy;
    while ((ch = getch()) != endSig) {
        switch (ch) {
            case KEY_UP:
                if (cursor[0] > upperLimit) cursor[0]--;
                break;
            case KEY_DOWN:
                if (cursor[0] < lowerLimit && cursor[0] < map->getRenderer()->rHeight - 1) cursor[0]++;
                break;
            case KEY_LEFT:
                if (cursor[1] > 0) cursor[1]--;
                break;
            case KEY_RIGHT:
                if (cursor[1] < map->getRenderer()->rWidth - 1)
                    cursor[1]++; //problem : does not hav the right limit for down/left
                break;
            default:
                break;
        }
        wmove(winToMoveOn, (int) cursor[0], (int) cursor[1]);
        wrefresh(winToMoveOn);
    }
    curs_set(0);
}

void Game::ExecuteInput(Worm *worm, int todo_input) {
    switch (todo_input) {
        case 'z':
            moveWorm(worm, 2);
            break;
        case 's':
            break;
        case 'q':
            moveWorm(worm, 0);
            break;
        case 'd':
            moveWorm(worm, 1);
            break;
        case 'i':
            if (this->map->getRenderer()->start_vert > 0) {
                this->map->getRenderer()->MoveVert(this->map->getRenderer()->start_vert - 1);
            }
            this->Render(stdscr);
            break;
        case 'k':
            // Tests done in Move method.
            this->map->getRenderer()->MoveVert(this->map->getRenderer()->start_vert + 1);
            this->Render(stdscr);
            break;
        case 'j':
            if (this->map->getRenderer()->start_horiz > 0) {
                this->map->getRenderer()->MoveHoriz(this->map->getRenderer()->start_horiz - 1);
            }
            this->Render(stdscr);
            break;
        case 'l':
            // Tests done in Move method.
            this->map->getRenderer()->MoveHoriz(this->map->getRenderer()->start_horiz + 1);
            this->Render(stdscr);
            break;
        case KEY_RESIZE:
            getmaxyx(stdscr, this->wHeight, this->wWidth);
            this->map->getRenderer()->Resize(this->wHeight - INFO_HEIGHT, this->wWidth - INV_WIDTH);
            this->Render(stdscr);
            break;
            // case 't': --> géré différemment, dans gameIsRunningWhatYouGonnaDo || appel à shootOnSpectatorsScreens
        default:
            break;
    }
}

void Game::shootOnSpectatorsScreens(Worm *worm, int x, int y, int modifier, Weapon *usedWeapon) {

    auto aim = new Vector2d(x, y);

    usingWeapon(worm, usedWeapon, aim, modifier);

    gravity(worm);//au cas où l'on tire sous ses pieds
    delete aim;
}

void Game::Render(WINDOW *win) {
    int oldCursor[2];
    getyx(win, oldCursor[0], oldCursor[1]);
    for (unsigned i = 0; i < this->map->getRenderer()->rHeight; i++) { // LINES
        for (unsigned j = 0; j < this->map->getRenderer()->rWidth; j++) { // COLUMNS
            unsigned int yCoord = i + map->getRenderer()->start_vert;
            unsigned int xCoord = j + map->getRenderer()->start_horiz;
            Cell *currentCell = map->getMatrix()[yCoord][xCoord];
            switch (this->map->getMatrix()[yCoord][xCoord]->getType()) {
                case AIR:
                    if (currentCell->getWorm()) {
                        wattron(win, COLOR_PAIR(currentCell->getWorm()->getColour()));
                        wattron(win, A_BOLD);
                        mvwaddch(win, i, j, currentCell->getWorm()->getNumber());
                        wattroff(win, A_BOLD);
                        wattroff(win, COLOR_PAIR(currentCell->getWorm()->getColour()));
                        break;
                    }

                    if (currentCell->getProjectile()) {
                        wattron(win, COLOR_PAIR(COLOR_BULLET));
                        mvwaddch(win, i, j, 'B');
                        wattroff(win, COLOR_PAIR(COLOR_BULLET));
                        break;
                    }

                    if (currentCell->getCrate()) {
                        wattron(win, COLOR_PAIR(COLOR_AIR));
                        wattron(win, A_BOLD);
                        mvwaddch(win, i, j, currentCell->getCrate()->getSymbol());
                        wattroff(win, A_BOLD);
                        wattroff(win, COLOR_PAIR(COLOR_AIR));
                        break;
                    }

                    wattron(win, COLOR_PAIR(COLOR_AIR));
                    mvwaddch(win, i, j, ' ');
                    wattroff(win, COLOR_PAIR(COLOR_AIR));
                    break;

                case DIRT:
                    wattron(win, COLOR_PAIR(COLOR_DIRT));
                    mvwaddch(win, i, j, ' ');
                    wattroff(win, COLOR_PAIR(COLOR_DIRT));
                    break;

                case ROCK:
                    wattron(win, COLOR_PAIR(COLOR_ROCK));
                    mvwaddch(win, i, j, ' ');
                    wattroff(win, COLOR_PAIR(COLOR_ROCK));
                    break;

                case WATER:
                    wattron(win, COLOR_PAIR(COLOR_WATER));
                    mvwaddch(win, i, j, ' ');
                    wattroff(win, COLOR_PAIR(COLOR_WATER));
                    break;
            }
        }
    }
    wmove(win, oldCursor[0], oldCursor[1]);
    wrefresh(win);
}

void Game::UpdateInv() {
    werase(invView);
    box(invView, 0, 0);
    char toPrint[INV_WIDTH - 2];
    mvwaddstr(invView, 1, 1, "Inventory:");
    for (unsigned short i = 0; i < userInventory->getSize(); i++) {
        InventorySlot *currentItem = userInventory->getPouches()[i];
        sprintf(toPrint, "- %s : %d", currentItem->getItemName().c_str(), currentItem->getQuantity());
        mvwaddstr(invView, 3 + i, 1, toPrint);
    }

    wrefresh(invView);
}

void Game::UpdateInfo(int playerCount, int wormCount, Team **allTeams, int timeLeft) {
    char str_life[5];
    werase(this->infoView);
    box(this->infoView, 0, 0);
    int nameLength = std::min<int>(32, (COLS - INV_WIDTH) / (playerCount + 1) - 6);
    int tmplife;

    for (int i = 0; i < playerCount; i++) {
        mvwaddstr(this->infoView, 1, 1 + i * ((COLS - INV_WIDTH) / (playerCount + 1)),
                  allTeams[i]->getTeamName().c_str());
        for (int j = 0; j < wormCount; j++) {
            mvwaddch(this->infoView, 3 + j, 3 + i * ((COLS - INV_WIDTH) / (playerCount + 1)), j + 1 + 0x30);
            mvwaddstr(this->infoView, 3 + j, 4 + i * ((COLS - INV_WIDTH) / (playerCount + 1)), ") ");
            mvwaddnstr(this->infoView, 3 + j, 6 + i * ((COLS - INV_WIDTH) / (playerCount + 1)),
                       allTeams[i]->getWorms()[j]->getName().c_str(), nameLength);
            wattron(this->infoView, COLOR_PAIR(allTeams[i]->getWorms()[j]->getColour() + 6));
            /*
            for (int k = 0; k < (int) round((allTeams[i]->getWorms()[j]->getHealth()) / 5.0); k++) {
                mvwaddch(this->infoView, 3 + j, i * (COLS - INV_WIDTH) / (playerCount + 1) + nameLength + k, ' ');
            }*/
            tmplife = allTeams[i]->getWorms()[j]->getHealth();
            if (tmplife > 0 && tmplife <= 2000) {
                sprintf(str_life, "%d", allTeams[i]->getWorms()[j]->getHealth());
            } else if (tmplife == 0) {
                sprintf(str_life, "DEAD");
            } else {
                sprintf(str_life, "ERR");
            }
            mvwaddstr(this->infoView, 3 + j, i * (COLS - INV_WIDTH) / (playerCount + 1) + nameLength, str_life);
            wattroff(this->infoView, COLOR_PAIR(allTeams[i]->getWorms()[0]->getColour() + 6));
        }
    }
    //Ligne de séparation après vie des vers.
    for (int i = 1; i < (int) (INFO_HEIGHT - 1); i++) {
        mvwaddch(this->infoView, i, 1 + playerCount * ((COLS - INV_WIDTH) / (playerCount + 1)), '|');
    }
    //Autres infos:
    char tmp[15];
    sprintf(tmp, "Time left: %d", timeLeft);
    mvwaddstr(this->infoView, 1, 3 + playerCount * ((COLS - INV_WIDTH) / (playerCount + 1)), tmp);

    wrefresh(this->infoView);
}

void Game::flood() {
    map->flood();
    Render(stdscr);
}

int Game::gravity(Worm *worm) {
    auto y = (unsigned) worm->getPosition()->y;
    auto x = (unsigned) worm->getPosition()->x;
    while (map->getMatrix()[y + 1][x]->getType() == AIR && map->getMatrix()[y + 1][x]->getType() != WATER &&
           !map->getMatrix()[y + 1][x]->getWorm()) {
        map->getMatrix()[y][x]->setWorm(nullptr);
        map->getMatrix()[y + 1][x]->setWorm(worm);
        auto crate = map->getMatrix()[y + 1][x]->getCrate();
        if (crate){
            getCrateContent(worm, crate);
            map->getMatrix()[y + 1][x]->setCrate(nullptr);
        }
        worm->setPosition(x, y + 1);
        Render(stdscr);
        y++;
    }

    // cas où le vers tombe dans l'eau
    if (map->getMatrix()[y + 1][x]->getType() == WATER) {
        map->getMatrix()[y][x]->getWorm()->kill();
        map->getMatrix()[y][x]->setWorm(nullptr);
        Render(stdscr);
        return 0; //le vers est mort -> implique fin du tour
    }
    return 1; // le vers est encore en vie
}

void Game::getCrateContent(Worm *worm, Crate *crate) {
    switch (crate->getSymbol()){
        case '%': //medkit
            {
                auto tmp = dynamic_cast<Medkit *>(crate);
                worm->modifyHealthBy(tmp->getHP());//todo updateinfo to get the right amount directly ?
                delete tmp;
            }
            break;
        case '$': //gunCrate
            bool canReceive = worm->getPlayerId() == playerID;
            auto tmp = dynamic_cast<GunCrate *>(crate);
            int which = -1;
            if (canReceive) {
                switch (tmp->getContent()) {
                    case Handgun:
                        which = 0;
                        break;
                    case Grenade:
                        which = 1;
                        break;
                    case Blowtorch:
                        which = 2;
                        break;
                    case SIZE:
                        break;
                }
                if (which != -1) userInventory->getPouches()[which]->refill();
                UpdateInv();
                mvwaddstr(invView, userInventory->getSize()+3,3, "Merci Papa Noël");
                wrefresh(invView);
            }
            delete tmp;
            break;
    }
}

int Game::moveWorm(Worm *worm, int flag) {
    /*
    Valeur de retour : un entier : 0 si un vers est mort (de la gravité (eau)), 1 si le vers est encore en vie
    */
    int res = 1;
    auto y = (unsigned) worm->getPosition()->y;
    auto x = (unsigned) worm->getPosition()->x;
    if (flag == 1 && x + 1 < map->getRenderer()->mapWidth && map->getMatrix()[y][x + 1]->getType() == AIR && !map->getMatrix()[y][x + 1]->getWorm()) {
        map->getMatrix()[y][x]->setWorm(nullptr);
        map->getMatrix()[y][x + 1]->setWorm(worm);
        auto crate = map->getMatrix()[y][x + 1]->getCrate();
        if (crate){
            getCrateContent(worm, crate);
            map->getMatrix()[y][x + 1]->setCrate(nullptr);
        }
        worm->setPosition(x + 1, y);
        res = gravity(worm);
    } else if (flag == 0 && x > 0 && map->getMatrix()[y][x - 1]->getType() == AIR &&
               !map->getMatrix()[y][x - 1]->getWorm()) {
        map->getMatrix()[y][x]->setWorm(nullptr);
        map->getMatrix()[y][x - 1]->setWorm(worm);
        auto crate = map->getMatrix()[y][x - 1]->getCrate();
        if (crate){
            getCrateContent(worm, crate);
            map->getMatrix()[y][x - 1]->setCrate(nullptr);
        }
        worm->setPosition(x - 1, y);
        res = gravity(worm);
    } else if (flag == 2 && y - 1 > 0 &&
               (map->getMatrix()[y + 1][x]->getType() != AIR || map->getMatrix()[y + 1][x]->getWorm()) &&
               map->getMatrix()[y - 1][x]->getType() == AIR && !map->getMatrix()[y-1][x]->getWorm()) {
        map->getMatrix()[y][x]->setWorm(nullptr);
        map->getMatrix()[y - 1][x]->setWorm(worm);
        auto crate = map->getMatrix()[y - 1][x]->getCrate();
        if (crate){
            getCrateContent(worm, crate);
            map->getMatrix()[y - 1][x]->setCrate(nullptr);
        }
        worm->setPosition(x, y - 1);
    }
    Render(stdscr);

    return res;
}

void Game::generateWorms(Team *team, int nbre_worms, int x_coord_tab[], int index) {

    for (int j = 0; j < nbre_worms; j++) {

        // seed in nanoseconds
        struct timespec ts{};
        clock_gettime(CLOCK_MONOTONIC, &ts);
        srand((time_t) ts.tv_nsec);

        bool redo, wellGenerated = false;
        int x, y;
        while (!wellGenerated) {
            redo = false;
            x = rand() % map->getRenderer()->mapWidth;
            y = map->getRenderer()->mapHeight - 1;

            while ((map->getMatrix()[y][x]->getType() != AIR) && !redo) {
                y--;
                if (y < 0) {
                    redo = true; // on doit alors regénérer un nombre
                    printf("on atteint le toit de la carte.\n");
                }
                if (!redo) wellGenerated = true;
            }

            x_coord_tab[j + index] = x;

            team->getWorms()[j]->setPosition(x, y);
            map->getMatrix()[y][x]->setWorm(team->getWorms()[j]);

            gravity(team->getWorms()[j]);
        }

        Render(stdscr);
    }
}

void Game::placeWorms(int nOfWorms, int current_player_nb, int allPos[32], Team *team) {
    for (int i = 0; i < nOfWorms; i++) {
        int x = allPos[i + current_player_nb * nOfWorms];
        int y = (int) map->getRenderer()->mapHeight - 1;

        int vertical_shift = 0;
        while (map->getMatrix()[y + vertical_shift][x]->getType() != AIR ||
               map->getMatrix()[y + vertical_shift][x]->getWorm()) {
            vertical_shift--;
        }

        team->getWorms()[i]->setPosition(x, y + vertical_shift);
        map->getMatrix()[y + vertical_shift][x]->setWorm(team->getWorms()[i]);
        gravity(team->getWorms()[i]);
    }
}

int Game::powerChoose(int x, int y) {
    int ret = 0;
    char const *outChar;
    char inChar = ' ';
    halfdelay(6);
    while (ret != 5 && inChar != 't') {
        ret++;
        outChar = std::to_string(ret).c_str();
        mvwaddch(stdscr, y - 1, x + 1, *outChar);
        inChar = getch();
    }
    mvwaddch(stdscr, y - 1, x + 1, ' ');
    cbreak();
    return ret * 20;
}

void Game::placeCrate(int spawnX, Crate *crate) {
    int i = 1;
    bool done;
    map->getMatrix()[0][spawnX]->setCrate(crate);
    done = (map->getMatrix()[1][spawnX]->getType() == DIRT) || (map->getMatrix()[1][spawnX]->getType() == ROCK);
    Render(stdscr);

    while (!done) {
        map->getMatrix()[i - 1][spawnX]->setCrate(nullptr);
        map->getMatrix()[i][spawnX]->setCrate(crate);
        Render(stdscr);
        usleep(250000); // quart de seconde entre chaque déplacement;

        switch (map->getMatrix()[i + 1][spawnX]->getType()) {
            case AIR:
                if (map->getMatrix()[i + 1][spawnX]->getCrate()) {
                    done = true;
                }
                else if (map->getMatrix()[i + 1][spawnX]->getWorm()) {
                    getCrateContent(map->getMatrix()[i + 1][spawnX]->getWorm(), crate);
                    map->getMatrix()[i][spawnX]->setCrate(nullptr);//todo charactère ^ apparait une fois le worm parti
                }
                break;

            case WATER:
                map->getMatrix()[i][spawnX]->setCrate(nullptr);
                delete crate;
                done = true;
                break;

            case DIRT:
            case ROCK:
                done = true;
                break;
        }
        i++;
    }
    Render(stdscr);
}

int Game::getMapWidth() {
    return (int) map->getRenderer()->mapWidth;
}

Cell ***Game::getMatrix() {
    return map->getMatrix();
}

Object *Game::constructObject(const char *objectID) {
    Object *retWeapon;
    if (!strncmp(objectID, HANDGUN_ID, 3)) retWeapon = new StraightWeapon("Handgun", WEAPON_DAMAGE, 1);
    else if (!strncmp(objectID, GRENADE_ID, 3))retWeapon = new ThrownWeapon("Grenade", 100, 1);
    else retWeapon = new Utility("BlowTorch", 3); //if (!strncmp(objectID, BLOWTORCH_ID, 3))
    return retWeapon;
}

void Game::sendUsageData(int saveX, int saveY, int modifier, const char *objectID, int socket_cible) {
    //ENVOI de SHOOT
    sendChar(const_cast<char *>(SHOOT), socket_cible, const_cast<char *>("SHOOT dans tir"));

    // ENVOI DE X (curseur)
    sendInt(saveX, socket_cible, const_cast<char *>("curseur.x dans tir"));

    // ENVOI DE Y (curseur)
    sendInt(saveY, socket_cible, const_cast<char *>("curseur.y dans tir"));

    //ENVOI DE MODIFIER (puissance)
    sendInt(modifier, socket_cible, const_cast<char *>("modifier dans tir"));

    //ENVOI DU TYPE D'ARME
    sendChar(const_cast<char *>(objectID), socket_cible, const_cast<char *>("type d'arme dans tir"));
}

void Game::useUtility(Worm *worm, Utility *usedUtility, Vector2d *aim) {
    Vector2d *direction = (*worm->getPosition() - *aim);// vector that can be negative
    for (unsigned short i = 1; i <= usedUtility->getPotency(); i++) {
        auto flame = new Projectile(*worm->getPosition(), 0, 1);
        flame->setTrajectory(direction, 100);
        flame->nullifyGravity();
        map->getMatrix()[(int) flame->getPosition()->y][(int) flame->getPosition()->x]->setProjectile(flame);
        Render(stdscr);
        bulletMover(flame, flame->getPosition(), usedUtility->getPotency());
    }
}

void Game::useObjectOnSpectatorsScreens(Worm *worm, Object *usedObject, int recX, int recY, int recModifier) {
    auto weapon = dynamic_cast<Weapon *>(usedObject);

    if (weapon) shootOnSpectatorsScreens(worm, recX,recY,recModifier,weapon);
    else {
        auto aim = new Vector2d(recX,recY);
        auto utility = dynamic_cast<Utility *>(usedObject);
        useUtility(worm, utility, aim);
    }
}
