//
// Created by Aloïs Glibert (000393192) on 26/02/20 .
//

#include "Projectile.h"
#include "../utilsSource/CONST.h"

Projectile::Projectile(Vector2d shooterPosition, unsigned short damage, unsigned short spread) : damage(damage),
                                                                                                 spread(spread) {
    physics = new Physics();
    physics->setPosition(shooterPosition.x, shooterPosition.y);
    physics->setAccelerationY(GRAVITY);
}

void Projectile::nullifyGravity() {
    physics->setAccelerationY(0);
}

void Projectile::setTrajectory(Vector2d *direction, int modifier) {
    direction->normalizeTo(60);
    direction->operator*=((double) modifier / 100);
    physics->setSpeed(0, 0);
    if (direction->x < 0) {
        physics->increasePositionX();
        physics->setSpeedX(std::abs(direction->x));
    } else if (direction->x > 0) {
        physics->decreasePositionX();
        physics->setSpeedX(-direction->x);
    }
    if (direction->y < 0) {
        physics->increasePositionY();
        physics->setSpeedY(std::abs(direction->y));
    } else if (direction->y > 0) {
        physics->decreasePositionY();
        physics->setSpeedY(-direction->y);
    }
}

Physics *Projectile::getPhysics() const {
    return physics;
}

void Projectile::move() {
    physics->updatePosition();
}

unsigned short Projectile::getDamage() const {
    return damage;
}

Vector2d *Projectile::getPosition() {
    return physics->getPosition();
}
