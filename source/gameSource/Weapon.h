//
// Created by Aloïs Glibert (000393192) on 25/02/20 .
//

#ifndef GROUPE_8_WEAPON_H
#define GROUPE_8_WEAPON_H

#include "Object.h"
#include "Projectile.h"

class Weapon : public Object { //todo SRD : the whole weapon system
protected:
    unsigned short baseDamage;
    unsigned short spread;
public:
    Weapon(const std::string &name, unsigned short baseDamage, unsigned short spread)
            : Object(name), baseDamage(baseDamage), spread(spread) {}

     ~Weapon() override = default ;

    virtual Projectile *shoot(Physics *shooterPhysics, Vector2d *cursorPosition, unsigned short modifier) {
        unsigned short realDamage; // modifier le vecteur en vector 2d ?
        realDamage = baseDamage * (unsigned short) (1 + (double) modifier / 100);
        auto shooterPosition = shooterPhysics->getPosition();
        Vector2d *aim = (*shooterPosition - *cursorPosition);// vector that can be negative
        auto *bullet = new Projectile(*shooterPosition, realDamage, spread);
        bullet->setTrajectory(aim, modifier);
        return bullet;
    };
};


#endif //GROUPE_8_WEAPON_H
