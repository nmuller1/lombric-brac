//
// Created by Aloïs Glibert (000393192) on 26/02/20 .
//

#include "StraightWeapon.h"


StraightWeapon::StraightWeapon(const std::string &name, unsigned short baseDamage, unsigned short spread)
        : Weapon(name, baseDamage, spread) {}

Projectile *StraightWeapon::shoot(Physics *shooterPhysics, Vector2d *cursorPosition, unsigned short modifier) {
    auto bullet = Weapon::shoot(shooterPhysics, cursorPosition, modifier);
    bullet->nullifyGravity();
    return bullet;
}

StraightWeapon::~StraightWeapon() = default;
