#include <string>

#include "Map.h"
#include "../utilsSource/CONST.h"


Map::Map() {
    // Init matrix.
    matrix = new Cell **[MAP_DISPLAY_SIZE_VERT];
    for (unsigned i = 0; i < MAP_DISPLAY_SIZE_VERT; i++) {
        // Init each row of the matrix.
        matrix[i] = new Cell *[MAP_DISPLAY_SIZE_HORIZ];
        for (unsigned j = 0; j < MAP_DISPLAY_SIZE_HORIZ; j++) {
            // Init each cell in the row.
            matrix[i][j] = new Cell(AIR);
        }
    }
    renderer = new Renderer(0, 0, 0, 0);
}

Map::Map(const std::string &map) {
    std::ifstream file(map);
    std::string str;
    int h, w;

    // Read map height and width (#lines & #columns).
    file >> h >> w;
    mHeight = h + 3;
    mWidth = w;

    // Get rid of first line.
    getline(file, str);

    // Init matrix.
    matrix = new Cell **[mHeight];
    for (int i = 0; i < mHeight-3; i++) {
        // Init each row of the matrix.
        matrix[i] = new Cell *[mWidth];
        getline(file, str);
        for (int j = 0; j < mWidth; j++) {
            // Init each cell in the row.
            switch (str[j]) {
                case ' ':
                    matrix[i][j] = new Cell(AIR);
                    break;
                case '#':
                    matrix[i][j] = new Cell(DIRT);
                    break;
                case '=':
                    matrix[i][j] = new Cell(ROCK);
                    break;
            }
        }
    }
    file.close();
    for (int i = mHeight-3; i < mHeight; i++){
        matrix[i] = new Cell*[mWidth];
        for(int j = 0; j < mWidth; j++){
            matrix[i][j] = new Cell(WATER);
        }
    }
    renderer = new Renderer(0, 0, 0, 0, (unsigned) mHeight, (unsigned) mWidth);
}

Map::~Map() {
    for (int i = 0; i < mHeight; i++) {
        for (int j = 0; j < mWidth; j++) {
            // Delete each column's cells in row.
            delete matrix[i][j];
        }
        // Delete the emptied row.
        delete matrix[i];
    }
    // Delete the emptied matrix.
    delete matrix;
}

Cell ***Map::getMatrix() {
    return matrix;
}

Renderer *Map::getRenderer() {
    return renderer;
}

void Map::GenerateMap() {

}

void Map::flood(){
    this->waterLevel++;
    for (int i = 0; i < this->mWidth; i++){
        this->matrix[this->mHeight-this->waterLevel][i]->setType(WATER);
        //check if crate above
        if (this->matrix[this->mHeight-waterLevel-1][i]->getCrate()){
            delete this->matrix[this->mHeight-waterLevel-1][i]->getCrate();
            this->matrix[this->mHeight-waterLevel-1][i]->setCrate(nullptr);
        }
        //check if worm above
        if (this->matrix[this->mHeight-waterLevel-1][i]->getWorm()){
            this->matrix[this->mHeight-waterLevel-1][i]->getWorm()->kill();
            this->matrix[this->mHeight-waterLevel-1][i]->setWorm(nullptr);
        }
    }
}