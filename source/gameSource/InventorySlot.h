#ifndef INVENTORYSLOT_H
#define INVENTORYSLOT_H

#include "Object.h"

class InventorySlot {
    std::string itemName;
    std::string itemId;
    unsigned quantity;
public:
    InventorySlot(const char *name, const char *identifier, int initQuantity);

    ~InventorySlot() = default;

    inline const std::string &getItemName() const { return itemName; }

    inline const std::string &getItemId() const { return itemId; }

    inline unsigned int getQuantity() const { return quantity; }

    inline void deplete() { quantity--; }

    inline void refill() {quantity++;};
};

#endif // INVENTORYSLOT_H