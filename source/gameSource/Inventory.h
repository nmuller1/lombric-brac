//
// Created by Aloïs Glibert (000393192) on 25/03/20 .
//

#ifndef GROUPE_8_INVENTORY_H
#define GROUPE_8_INVENTORY_H


#include "InventorySlot.h"

class Inventory {
        InventorySlot** pouches;
        unsigned short size; // max size = 65535
public:
    Inventory();

    virtual ~Inventory();

    InventorySlot **getPouches() const {
        return pouches;
    }

    unsigned short getSize() const {
        return size;
    }

};


#endif //GROUPE_8_INVENTORY_H
