#ifndef WORM_H
#define WORM_H

#include <string>
#include "Physics.h"
#include "../utilsSource/Team.h"
#include "Weapon.h"
#include "Player.h"

class Player;

class Team;

class Worm {
private:
    int health, colour, playerId;
    char number;
    bool living;
    Physics *physics;
    std::string name = "Default";
    Team *team;
    int inGameTeamNbre;

public:
    Worm() = default;

    Worm(std::string name, char number, int colour, int health, int playerID);

    ~Worm();

    Worm(Worm &&worm);

    Worm &operator=(Worm &&worm);

    int getPlayerId() const;

    void setPlayerId(int newPlayerId);

    int getHealth();

    Vector2d *getPosition();

    Team *getTeam();

    std::string getName();

    char getNumber();

    int getColour();

    void setPosition(unsigned int x, unsigned int y);

    void setHealth(int newHealth);

    void setName(std::string newName);

    void setNumber(char number);

    void setColour(int colour);

    Physics *getPhysics() const;

    void modifyHealthBy(int numberReceived);

    void kill();

    bool isAlive();

    void setInGameTeamNbre(int nbre);

    int getInGameTeamNumber();
};

#endif //WORM_H
