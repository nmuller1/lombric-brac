#include "Worm.h"
#include <utility>


Worm::Worm(std::string name, char number, int colour, int health, int playerID) : health(health), colour(colour),
                                                                                  playerId(playerID), number(number),
                                                                                  living(true), physics(new Physics()),
                                                                                  name(std::move(name)) {
}

Worm::~Worm() {
    delete physics;
    //delete team;
}

Worm::Worm(Worm &&worm) { *this = std::move(worm); }

Worm &Worm::operator=(Worm &&worm) {
    if (this != &worm) {
        this->health = worm.health;
        this->name = worm.name;
        this->colour = worm.colour;
        this->number = worm.number;

        delete this->physics;
        this->physics = worm.physics;
        worm.physics = nullptr;

        delete this->team;
        this->team = worm.team;
        worm.team = nullptr;
    }
    return *this;
}

int Worm::getHealth() {
    return this->health;
}

Vector2d *Worm::getPosition() {
    return this->physics->getPosition();
}


std::string Worm::getName() {
    return this->name;
}

int Worm::getColour() {
    return this->colour;
}

Physics *Worm::getPhysics() const {
    return physics;
}

char Worm::getNumber() {
    return this->number;
}

Team *Worm::getTeam() {
    return this->team;
}

void Worm::setNumber(char newNumber) {
    this->number = newNumber;
}

void Worm::setHealth(int newHealth) {
    this->health = newHealth;
}

void Worm::setName(std::string newName) {
    this->name = std::move(newName);
}

void Worm::setColour(int newColour) {
    this->colour = newColour;
}

void Worm::setPosition(unsigned int x, unsigned int y) {
    this->physics->setPosition(x, y);
}

void Worm::modifyHealthBy(int numberReceived) {//can be used both for healing and damaging
    health += numberReceived;
}

void Worm::kill() {
    this->living = false;
    this->health = 0;
}

bool Worm::isAlive() {
    return living;
}

void Worm::setInGameTeamNbre(int nbre) {
    this->inGameTeamNbre = nbre;
}

int Worm::getInGameTeamNumber() {
    return this->inGameTeamNbre;
}

int Worm::getPlayerId() const {
    return playerId;
}

void Worm::setPlayerId(int newPlayerId) {
    playerId = newPlayerId;
}
