#ifndef MEDKIT_H
#define MEDKIT_H

#include "Crate.h"

class Medkit : public Crate {
    int hp;
public:
    explicit Medkit(int hp);

    ~Medkit() final = default;

    int getHP();
};

#endif //MEDKIT_H