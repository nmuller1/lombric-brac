#include "Crate.h"

Crate::Crate() : symbol(' ') {}

Crate::Crate(char symbol) : symbol(symbol) {}

char Crate::getSymbol() {
    return this->symbol;
}