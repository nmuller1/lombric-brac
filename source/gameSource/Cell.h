#ifndef CELL_H
#define CELL_H


#include "CellType.h"
#include "Crate.h"
#include "Worm.h"
#include "Projectile.h"

class Cell {
    CellType type;
    Crate *crate;
    Worm *worm;
    Projectile *projectile;
public:
    Projectile *getProjectile() const;

    void setProjectile(Projectile *newProjectile);

    explicit Cell(CellType type);

    ~Cell() = default;

    CellType getType();

    Worm *getWorm();

    Crate *getCrate();

    void setWorm(Worm *data);

    void setCrate(Crate *data);

    bool isBreakable();

    void setType(CellType newType);
};

#endif //CELL_H
