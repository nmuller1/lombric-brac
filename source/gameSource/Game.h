#ifndef GAME_H
#define GAME_H

#include <ncurses.h>

#include "Map.h"
#include "../utilsSource/Room.h"
#include "Worm.h"
#include "Inventory.h"
#include "Utility.h"

class Game {
    WINDOW *invView, *infoView;
    int ch, playerID;
    unsigned cursor[2] = {0, 0};//is written in y,x form
    unsigned short chosenObjectNumber;
    unsigned wHeight, wWidth;
    Map *map;
    Room room;
    Inventory *userInventory;
    bool isActor;
public:
    explicit Game(const std::string &map_txt);

    ~Game();

    void setPlayerId(int playerId) {
        playerID = playerId;
    }

    int getMapWidth();

    Cell ***getMatrix();

    void setRoom(Room newRoom);

    void StartGame(int playerCount, int wormCount, Team **allTeams);

    void RunGame(Worm *worm, int socket_cible, int turnTime);

    void ExecuteInput(Worm *worm, int todo_input);

    void shootOnSpectatorsScreens(Worm *worm, int x, int y, int modifier, Weapon *usedWeapon);

    void Render(WINDOW *win);

    int gravity(Worm *worm);

    void flood();

    void getCrateContent(Worm *worm, Crate *crate);

    int moveWorm(Worm *worm, int lor);

    Vector2d *viewFinder(int shooterX, int shooterY);

    void bulletMover(Projectile *bullet, const Vector2d *bulletPosition, unsigned short maxDist);

    void generateWorms(Team *team, int nbre_worms, int x_coord_tab[], int index);

    void UpdateInv();

    void UpdateInfo(int playerCount, int wormCount, Team **allTeams, int timeLeft);

    void placeWorms(int nOfWorms, int current_player_nb, int allPos[32], Team *team);

    static int powerChoose(int x, int y);

    void placeCrate(int spawnX, Crate *crate);

    void usingWeapon(const Worm *worm, Weapon *weapon, Vector2d *aim, unsigned short modifier);

    void printOnInvView(const std::string &message) const;

    void shootOnActorScreen(Worm *worm, Weapon *usedWeapon, Vector2d *aim, int &modifier);

    static Object *constructObject(const char *objectID);

    void moveCursor(char endSig, WINDOW *winToMoveOn, unsigned short lowerLimit, unsigned short upperLimit);

    static void sendUsageData(int saveX, int saveY, int modifier, const char *objectID, int socket_cible);

    const char *chooseObject();

    void useUtility(Worm *worm, Utility *usedUtility, Vector2d *aim);

    void useObjectOnActorScreen(Worm *worm, Object *usedObject, int &sentX, int &sentY, int &modifier);

    void useObjectOnSpectatorsScreens(Worm *worm, Object *usedObject, int recX, int recY, int recModifier);

    void setActor(bool actorState) { isActor = actorState; }
};

#endif // GAME_H
