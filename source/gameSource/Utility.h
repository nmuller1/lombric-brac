//
// Created by Aloïs Glibert (000393192) on 25/02/20 .
//

#ifndef GROUPE_8_UTILITY_H
#define GROUPE_8_UTILITY_H

#include "Object.h"

class Utility : public Object {
    unsigned short potency;
public:
    unsigned short getPotency() const {
        return potency;
    }

    Utility(const std::string &name, int potency);
};


#endif //GROUPE_8_UTILITY_H
