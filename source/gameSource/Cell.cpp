#include "Cell.h"

Cell::Cell(CellType type) {
    this->type = type;
    this->crate = nullptr;
    this->worm = nullptr;
    this->projectile = nullptr;
}

CellType Cell::getType() {
    return this->type;
}

Worm *Cell::getWorm() {
    return this->worm;
}

Crate *Cell::getCrate() {
    return this->crate;
}

void Cell::setWorm(Worm *data) {
    this->worm = data;
}

void Cell::setCrate(Crate *data) {
    this->crate = data;
}

bool Cell::isBreakable() {
    return (this->type == DIRT);
}

Projectile *Cell::getProjectile() const {
    return projectile;
}

void Cell::setProjectile(Projectile *newProjectile) {
    Cell::projectile = newProjectile;
}

void Cell::setType(CellType newType) {
    this->type = newType;
}