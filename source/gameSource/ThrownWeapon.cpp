//
// Created by Aloïs Glibert (000393192) on 26/02/20 .
//

#include "ThrownWeapon.h"


ThrownWeapon::ThrownWeapon(const std::string &name, unsigned short baseDamage, unsigned short spread) : Weapon(name, baseDamage, spread) {}

Projectile *ThrownWeapon::shoot(Physics *shooterPhysics, Vector2d *cursorPosition, unsigned short modifier) {
    return Weapon::shoot(shooterPhysics, cursorPosition, modifier);
}
