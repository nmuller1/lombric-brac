#ifndef OBJECT_H
#define OBJECT_H

#include <string>

class Object {
private:
    std::string name;
public:
    Object();

    explicit Object(std::string name);

    const inline std::string &getName() const;

    virtual ~Object() = default;
};

#endif // OBJECT_H
