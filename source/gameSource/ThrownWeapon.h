//
// Created by Aloïs Glibert (000393192) on 26/02/20 .
//

#ifndef GROUPE_8_THROWNWEAPON_H
#define GROUPE_8_THROWNWEAPON_H

#include "Weapon.h"

class ThrownWeapon : public Weapon {
public:
    ThrownWeapon(const std::string &name, unsigned short baseDamage, unsigned short spread);

    ~ThrownWeapon() final = default;

    Projectile *shoot(Physics *shooterPhysics, Vector2d *cursorPosition, unsigned short modifier) override;
};


#endif //GROUPE_8_THROWNWEAPON_H
