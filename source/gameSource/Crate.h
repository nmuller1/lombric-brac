#ifndef CRATE_H
#define CRATE_H

class Crate {
protected:
    char symbol;

public:

    Crate();

    explicit Crate(char symbol);

    virtual char getSymbol() final;

    virtual ~Crate() = default;
};

#endif //CRATE_H
