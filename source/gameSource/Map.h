#ifndef MAP_H
#define MAP_H

#include <iostream>
#include "Cell.h"

#include "../utilsSource/Renderer.h"
#include <fstream>

class Map {
    Cell ***matrix;
    Renderer *renderer;
    int mHeight, mWidth, waterLevel=3;
public:
    Map();

    explicit Map(const std::string &map);

    ~Map();

    Cell ***getMatrix();

    Renderer *getRenderer();

    void LoadMap(std::string map);

    void GenerateMap();

    void flood();
};

#endif //MAP_H