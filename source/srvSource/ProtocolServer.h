#ifndef GROUPE_8_PROTOCOL_SERVER_H
#define GROUPE_8_PROTOCOL_SERVER_H

#include "../utilsSource/CONST.h"
#include "../dBSource/Database.h"
#include "../utilsSource/DataExchange.h"
#include "../gameSource/Game.h"
#include "../utilsSource/GameParameters.h"
#include "../utilsSource/Interface.h"
#include "../gameSource/Player.h"
#include "../utilsSource/Room.h"

#include <pthread.h>
#include <string>
#include <iostream>
#include "../utilsSource/GameParameters.h"
#include "../utilsSource/Room.h"

typedef struct { //Info sur les joueur pour le stockage des joueur connecter au chat
    char _username[USERNAME_SIZE];
    int _socket;
    bool significant = false;
} userInfo;

int disconnect(char username[16], char connected[50][16], pthread_mutex_t mutex);

//int isAvailable(char username[USERNAME_SIZE], FILE *userID);

void getTeamNames(char worms[8][USERNAME_SIZE], char user[USERNAME_SIZE]);

int alreadyExist(char username[USERNAME_SIZE], char connected[MAX_NB_PLAYER][USERNAME_SIZE]);

int grantAccess(char username[USERNAME_SIZE], char connected[MAX_NB_PLAYER][USERNAME_SIZE], int socket_cible);

int denyAccess(int socket_cible);

int connectPlayer(char username[USERNAME_SIZE], char connected[MAX_NB_PLAYER][USERNAME_SIZE], int socket_cible,
                  int nb_connected);

int disconnect(char username[16], char connected[50][16], pthread_mutex_t mutex);

int manageConnexion(int socket_cible, char user[USERNAME_SIZE], Database &database);

int manageRegister(int socket_cible, Database &database);

GameParameters managePlayGame(int socket_cible, Room room_tab[], char clientName[], int *current_room);

GameParameters manageCreateGame(int socket_cible, Room room_tab[], char clientName[], int *current_room);

int manageJoinGame(int socket_cible, Room room_tab[], char clientName[], int *current_room);

int sendRanking(int socket_cible);

Ranking_filters recvRankingFilters(int socket_cible);

int getValidUsername(int socket_cible, Database &database);

void addToRoom(Room *room, const Player &player);

int start_a_game(Room room, GameParameters GP, Database db);

void
generateCrate(int crateSpawnRate, Game *game, Room *room, bool replayOn, int currentMatchID, int replayID, Database db);

bool isTimeOver(time_t start_time);

int connectToChat(char username[USERNAME_SIZE], int socket_client, userInfo connectedToChat[MAX_NB_PLAYER]);

int disconectToChat(char username[USERNAME_SIZE], userInfo connectedToChat[MAX_NB_PLAYER]);

int getPlayersSocket(char username[USERNAME_SIZE], userInfo connectedToChat[MAX_NB_PLAYER]);

int sendFriendList(int socket_client, char clientName[USERNAME_SIZE], Database &database);

int sendFriendRequest(int socket_client, char clientName[USERNAME_SIZE], Database &database);

int sendFriendRequestList(int socket_client, char clientName[USERNAME_SIZE], Database &database);

#endif //GROUPE_8_PROTOCOL_SERVER_H
