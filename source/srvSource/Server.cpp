#include "Server.h"

typedef struct { // struct de passage de paramètre à un thread
    int _socket;
    pthread_mutex_t _mutex;
} myData;

typedef struct { //struct de passage de paramètre au manageChat
    int _socket_cli1;
    char _username[USERNAME_SIZE];
    pthread_mutex_t _mutex;
} chatInfo;

typedef struct { // struct de passage de paramètre à un thread
    pthread_mutex_t _mutex;
    GameParameters GP;
} myGameData;

//variables globales
Database database("DB_v1");
char connected[MAX_NB_PLAYER][USERNAME_SIZE]; // tableau qui mémorise qui est connecté au serveur
userInfo connectedToChat[MAX_NB_PLAYER];
unsigned nb_connected = 0;
Room room_tab[16];
int current_room = 0;

// déclaration fct


int main(int argc, char **argv) {
    struct sockaddr_in adresse_serveur;
    int res;
    long port;
    int local_socket;
    pthread_t new_thread;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

    // ======================= Initialisation des structures ========================

    bzero(&adresse_serveur, sizeof(struct sockaddr_in));

    // ======================= Processing des paramètres ============================

    // test du nombre de paramètre
    if (argc != 2) {
        fprintf(stderr, "Paramètre attendu : port\n");
        return EXIT_FAILURE;
    }

    // Port
    errno = 0;
    port = strtol(argv[1], nullptr, 10);
    if (errno != 0 && port == 0) {
        perror("Impossible de convertir le port <%s>");
        return EXIT_FAILURE;
    }

    adresse_serveur.sin_port = htons(port);
    adresse_serveur.sin_family = AF_INET;
    adresse_serveur.sin_addr.s_addr = htonl(INADDR_ANY);


    // ================================= Préparation socket local =================================

    errno = 0;
    local_socket = socket(PF_INET, SOCK_STREAM, 0);
    if (local_socket == -1) {
        perror("Impossible d'ouvrir le socket: ");
        return EXIT_FAILURE;
    }

    errno = 0;
    res = bind(local_socket, (struct sockaddr *) &adresse_serveur, sizeof(struct sockaddr_in));
    if (res == -1) {
        perror("Impossible de lier le socket et la structure d'adresse: ");
        close(local_socket);
        return EXIT_FAILURE;
    }


    // ====================== Connexion ====================

    // Du côté serveur, on initialise une écoute active
    errno = 0;
    res = listen(local_socket, 20);
    if (res == -1) {
        perror("Impossible de se mettre en écoute: ");
        close(local_socket);
        return EXIT_FAILURE;
    }


    // accepte les connexions
    while (1) {
        int socket_client;
        struct sockaddr_in adresse_client;
        socklen_t taille_struct_addr_client = sizeof(adresse_client);

        bzero(&adresse_client, sizeof(struct sockaddr_in));

        errno = 0;
        socket_client = accept(local_socket, (struct sockaddr *) &adresse_client, &taille_struct_addr_client);
        if (socket_client == -1) {
            perror("Connexion impossible: ");
            continue;
        }
        myData sendToThread = {socket_client, mutex};
        pthread_create(&new_thread, nullptr, manage_client, &sendToThread);
    }

    //FIXME: problème de valgrind venant du fait que le programme se ferme pas correctement après un ctrl+C ?
    close(local_socket);
    return EXIT_SUCCESS;
}


void *manage_client(void *data) {

    bool quit = false;

    char str_buffer[TAILLE_BUFFER];
    myData rcvByMain = *((myData *) data);
    int socket_client = rcvByMain._socket;
    pthread_mutex_t mutex = rcvByMain._mutex;
    //pthread_t new_thread;
    pthread_t chat_thread;

    int res;
    char clientName[USERNAME_SIZE]; // servira à mettre à jour la liste des joueurs connectés

    while (!quit) {
        bool done = false;
        // RECEPTION DU MODE : LOGIN / REGISTER / QUIT
        while (!done) {
            res = recvData(str_buffer, socket_client);
            if (res == -1) {
                printf("Erreur à la réception de données(mode de login).");
                quit = true;
                done = true;
                close(socket_client);
                pthread_exit(nullptr);
            }

            if (!strcmp(str_buffer, "1")) { // LOGIN

                res = manageConnexion(socket_client, clientName, database);
                if (res == -1) {
                    quit = true;
                    done = true;
                    close(socket_client);
                    pthread_exit(nullptr);
                }
                if (res != -25) { // not return value
                    // DEBUG
                    printf("==========================================================\n");
                    printf("Liste des joueurs connectés (avant la nouvelle connexion): \n");
                    for (int i = 0; i < MAX_NB_PLAYER; i++) {
                        if (!strcmp(connected[i], "")) {
                            break;
                        }
                        printf("-%s\n", connected[i]);
                    }
                    printf("==========================================================\n\n");

                    // =========== MISE A JOUR JOUEURS CONNECTES =================

                    // mutex sur l'accès à la liste des joueurs connectés
                    res = pthread_mutex_lock(&mutex); //FIXME: actions de mutex peut-être un peu long
                    if (res != 0) {
                        printf("Mutex impossible, impossible de continuer.\n");
                        quit = true;
                        done = true;
                        close(socket_client);
                        pthread_exit(nullptr);
                    }

                    res = connectPlayer(clientName, connected, socket_client, (int) nb_connected);
                    if (res == -1) {
                        quit = true;
                        done = true;
                        close(socket_client);
                        pthread_mutex_unlock(&mutex);
                        pthread_exit(nullptr);
                    }
                    if (res == 0) { // access has been denied
                        printf("Impossible de connecter le joueur : %s\n", clientName);
                        quit = true;
                        done = true;
                        close(socket_client);
                        pthread_mutex_unlock(&mutex);
                        pthread_exit(nullptr);
                    }
                    done = true;
                    pthread_mutex_unlock(&mutex);
                }

            }
            if (!strcmp(str_buffer, "2")) {// REGISTER
                // il faudra s'assurer de ne pas avoir une redondance de pseudo | double vérification de mot de passe | demander 8 noms de vers
                // création d'un compte
                res = manageRegister(socket_client, database);
                if (res == -1) {
                    quit = true;
                    done = true;
                    close(socket_client);
                    pthread_exit(nullptr);
                }
                if (res != -25) {
                    // après avoir créer le compte, le joueur doit réentrer ses informations à travers une connexion classique
                    res = manageConnexion(socket_client, clientName, database);
                    if (res == -1) {
                        quit = true;
                        done = true;
                        close(socket_client);
                        pthread_exit(nullptr);
                    }

                    // DEBUG
                    printf("==========================================================\n");
                    printf("Liste des joueurs connectés (avant la nouvelle connexion): \n");
                    for (int i = 0; i < MAX_NB_PLAYER; i++) {
                        if (!strcmp(connected[i], "")) {
                            break;
                        }
                        printf("-%s\n", connected[i]);
                    }
                    printf("==========================================================\n\n\n");


                    // =========== MISE A JOUR JOUEURS CONNECTES =================

                    res = pthread_mutex_lock(&mutex); // mutex sur l'accès à la liste des joueurs connectés
                    if (res != 0) {
                        printf("Mutex impossible, impossible de continuer.\n");
                        quit = true;
                        done = true;
                        close(socket_client);
                        pthread_exit(nullptr);
                    }

                    res = connectPlayer(clientName, connected, socket_client, (int) nb_connected);
                    if (res == -1) {
                        quit = true;
                        done = true;
                        close(socket_client);
                        pthread_mutex_unlock(&mutex);
                        pthread_exit(nullptr);
                    }
                    if (res == 0) {
                        printf("Impossible de connecter le joueur : %s\n", clientName);
                        quit = true;
                        done = true;
                        close(socket_client);
                        pthread_mutex_unlock(&mutex);
                        pthread_exit(nullptr);
                    }
                    done = true;
                    pthread_mutex_unlock(&mutex);
                }
            }

            if (!strcmp(str_buffer,
                        "3")) { // l'utilisateur a décidé que en fait il avait mieux à faire de sa journée (shame)
                printf("Fin d'exécution d'un thread.\n");
                quit = true;
                done = true;
                close(socket_client);
                pthread_exit(nullptr);
            }
        }


        // ============================================================================


        // =====================================================
        //
        //     ON EST MAINTENANT DANS LE MENU PRINCIPAL
        //
        // =====================================================

        // RECEPTION DU CHOIX DU MENU PRINCIPAL
        bool disc = false;
        while (!disc) {
            res = recvData(str_buffer, socket_client);
            if (res == -1) {
                printf("Erreur à la réception de données(choix menu principal).\n");
                disc = true;
                quit = true;
                disconnect(clientName, connected, mutex);
                //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
                close(socket_client);
                pthread_exit(nullptr);
            }


            if (!strcmp(str_buffer, "1")) { // JOUER UNE PARTIE
                printf("Jouer une partie\n"); //debug
                GameParameters GP;
                GP = managePlayGame(socket_client, room_tab, clientName, &current_room);
                if (GP.error == -1) {
                    printf("Impossible de connecter à une partie.\n");
                    disc = true;
                    quit = true;
                    disconnect(clientName, connected, mutex);
                    //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
                    close(socket_client);
                    pthread_exit(nullptr);
                }

                // DEBUG
                printf("Paramètres recus : \n");
                printf("-mode de jeu : %d\n", GP.game_mode);
                printf("-nbre de joueurs : %d\n", GP.no_of_players);
                printf("-nbre de worms : %d\n", GP.no_of_worms);
                printf("-temps de tour : %d\n", GP.max_round_time);
                printf("-points de vie : %d\n", GP.init_life_pts);


                while (room_tab[GP.room_index]._current_nbre_player != GP.no_of_players) {
                    sleep(2);
                }

                start_a_game(room_tab[GP.room_index], GP, database);


                //Création du thread de partie !!
                //myGameData sendToThread = {mutex, GP};
                ///pthread_create(&new_thread, nullptr, manage_game, &sendToThread);


                // Si (et seulement si) managePlayGame a été poursuivi par "créer une partie", GP contient des paramètres utilisables pour lancer une partie TODO: tester si les paramètres sont bien setup (typiquement les mettre tous à 0 de base)
                //FIXME: il manque les joueurs
                // envoyer les paramètres de la partie + les joueurs concernés à un thread qui va prendre en charge la gestion de la partie FIXME: aucune putain d'idée de comment ça marche
            }


            if (!strcmp(str_buffer, "2")) { // CONSULTER LE CLASSEMENT
                printf("Consulter le classement\n"); //debug

                res = sendRanking(socket_client);
                if (res == -1) {
                    printf("Impossible d'envoyer le classement.\n");
                    disc = true;
                    quit = true;
                    disconnect(clientName, connected, mutex);
                    //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
                    close(socket_client);
                    pthread_exit(nullptr);
                }
            }


            if (!strcmp(str_buffer, "3")) {
                // CONSULTER LE PROFIL //TODO: potentiellement faire une fonction qui reprend le tout mais autant vérifer que ça marche d'abord
                // ETAPE 1 : RECEVOIR UN NOM DE PROFIL VALIDE
                res = getValidUsername(socket_client, database);
                if (res == -1) {
                    disc = true;
                    quit = true;
                    disconnect(clientName, connected, mutex);
                    //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
                    close(socket_client);
                    pthread_exit(nullptr);
                }

                // ETAPE 2 : envoyer les noms des vers associés au nom du joueur
                char worms[8][USERNAME_SIZE];
                getTeamNames(worms,
                             clientName); // modifie la valeur du paramètre worms pour qu'il contienne la liste des worms du joueur
                for (int i = 0; i < 8; i++) {
                    res = sendData(worms[i], socket_client);
                    if (res == -1) {
                        printf("Dans la consultation de profil : impossible d'envoyer un nom de ver.\n");
                        disc = true;
                        quit = true;
                        disconnect(clientName, connected, mutex);
                        //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
                        close(socket_client);
                        pthread_exit(nullptr);
                    }
                }
            }
            if (!strcmp(str_buffer, "4")) { // GERER LE PROFIL

                res = recvData(str_buffer, socket_client);
                if (res == -1) {
                    printf("Dans la gestion du profil : impossible de recevoir....\n");
                    disconnect(clientName, connected, mutex);
                    //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
                    close(socket_client);
                    pthread_exit(NULL);
                }
                if (!strcmp(str_buffer, "3")) { //Gerer la liste d'ami
                    //1: consulter la liste d'ami, 2: Ajouter un ami

                    res = recvData(str_buffer, socket_client);
                    if (res == -1) {
                        printf("Dans la gestion du profil : impossible de recevoir....\n");
                        disconnect(clientName, connected, mutex);
                        //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
                        close(socket_client);
                        pthread_exit(NULL);
                    }
                    if (!strcmp(str_buffer, "1")) {
                        res = sendFriendList(socket_client, clientName, database);
                        if (res == -1) {
                            printf("Erreur d'envoie de la liste d'ami\n");
                            close(socket_client);
                            pthread_exit(NULL);
                        }
                    } else if (!strcmp(str_buffer, "2")) {
                        res = sendFriendRequest(socket_client, clientName, database);
                        if (res == -1) {
                            printf("Erreur d'envoie de la demande d'ami\n");
                            close(socket_client);
                            pthread_exit(NULL);
                        }
                    } else if (!strcmp(str_buffer, "3")) {
                        res = sendFriendRequestList(socket_client, clientName, database);
                        if (res == -1) {
                            printf("Erreur d'envoie de la liste des demandes d'amis\n");
                            close(socket_client);
                            pthread_exit(NULL);
                        }
                    }
                }
            }
            if (!strcmp(str_buffer, "5")) { // CHAT
                pthread_mutex_t mutex_chat = PTHREAD_MUTEX_INITIALIZER;

                pthread_mutex_lock(&mutex_chat);
                res = connectToChat(clientName, socket_client, connectedToChat);
                pthread_mutex_unlock(&mutex_chat);

                if (res) {
                    //DEBUG
                    printf("Connecté %s au chat réussi socket n° %d\n", clientName, socket_client);
                } else {
                    printf("Impossible de connecter %s en ce moment\n", clientName);
                    close(socket_client);
                    pthread_exit(nullptr);
                }
                //CLION tells me that if we use nbr_friends ça va pas, aucune idée de pourquoi -> utilisation de 2
                //TODO: recevoir la list d'amis (BDD)
                //TODO: filtrer cette liste

                res = sendFriendList(socket_client, clientName, database);
                if(res == -1){
                    perror("Impossible d'envoyer la liste d'amis\n");
                    pthread_mutex_lock(&mutex_chat);
                    disconectToChat(clientName, connectedToChat);
                    pthread_mutex_unlock(&mutex_chat);
                    close(socket_client);
                    pthread_exit(NULL);
                }


                chatInfo chat_data;
                chat_data._socket_cli1 = socket_client;
                chat_data._mutex = mutex_chat;
                strcpy(chat_data._username, clientName);

                pthread_create(&chat_thread, nullptr, manage_chat, &chat_data);
                printf("Lancé le thread du chat\n");

                pthread_join(chat_thread, nullptr);

                printf("Out of loop\n");
                pthread_mutex_lock(&mutex_chat);
                disconectToChat(clientName, connectedToChat);
                pthread_mutex_unlock(&mutex_chat);
                disconnect(clientName, connected, mutex);
                close(socket_client);
                printf("Fin d'exécution d'un thread (joueur).\n");
                pthread_exit(nullptr);
                return nullptr;
            }
            if (!strcmp(str_buffer, "6")) {
                printf("Joueur déconnecté : %s\n", clientName);
                disc = true;
                disconnect(clientName, connected, mutex);
            }
            if (!strcmp(str_buffer, "7")) {
                printf("Joueur déconnecté : %s\n", clientName);
                disc = true;
                quit = true;
                disconnect(clientName, connected, mutex);
                //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
                close(socket_client);
                pthread_exit(nullptr);
            }
        }
    }








    // on oublie pas de déconnecter le client qui a fermé son terminal comme un sac
    disconnect(clientName, connected, mutex);
    //FIXME: besoin de prendre la valeur de retour ? Si y a un plantage dans un plantage je sais pas vraiment comment gérer ça
    disconectToChat(clientName, connectedToChat);
    close(socket_client);
    printf("Fin d'exécution d'un thread (joueur).\n");
    pthread_exit(nullptr);
    return nullptr;
}


void* manage_chat(void* data){

    char clientname[USERNAME_SIZE];
    char destname[USERNAME_SIZE];

    chatInfo info = *((chatInfo *) data);
    int socket_client1 = info._socket_cli1;
    int socket_client2;
    pthread_mutex_t mutex_chat = info._mutex;
    strcpy(clientname, info._username);

    int res;
    bool exit_loop = false;
    printf("Entré dans le thread du chat\n");

    errno = 0;
    char str_buffer[TAILLE_BUFFER];
    char str_parser[TAILLE_BUFFER];

    while (!exit_loop){

        /*
        TODO: recvData et sendData ne sont pas fait pour l'envoie des long msg
            il faut soit les modifier ou alors créer d'autre fonctions
        */

        //Reçois un message du client1 et envoyer au client2
        res = recvData(destname, socket_client1);
        if(res == -1){
            fprintf(stderr, "Erreur de reception du msg du client1\n");
            strcpy(str_buffer, "Serveur: Erreur de reception du msg");
            sendData(str_buffer, socket_client1); //Si ça marche pas tant pis
            break;
        }
        printf("En communication avec %s\n", destname);

        //Trouver le socket correspondent
        res = recvData(str_parser, socket_client1);
        if(res == -1){
            fprintf(stderr, "Erreur de reception du msg du client1\n");
            strcpy(str_buffer, "Serveur: Erreur de reception du msg");
            sendData(str_buffer, socket_client1); //Si ça marche pas tant pis
            break;
        }

        printf("Message reçu %s\n", str_buffer);
        strcpy(str_buffer, clientname);
        strcat(str_buffer, ": ");
        strcat(str_buffer, str_parser);



        pthread_mutex_lock(&mutex_chat);
        socket_client2 = getPlayersSocket(destname, connectedToChat);
        pthread_mutex_unlock(&mutex_chat);

        if(socket_client2 == -1){
            printf("Serveur: Le client n'est pas connecté en ce moment\n");
            strcpy(str_buffer, "Serveur: Le client n'est pas connecté en ce moment");
            sendData(str_buffer, socket_client1);
            continue;
        }

        //Le client en question est connecté
        //Envoie du msg

        res = sendData(str_buffer, socket_client2);
        if(res == -1){
            fprintf(stderr, "Erreur d'envoie d'un msg au client2\n");
            strcpy(str_buffer, "Serveur: Erreur d'envoi de votre message");
            sendData(str_buffer, socket_client1);
            break;
        }


    }
    close(socket_client1);
    close(socket_client2);
    pthread_exit(nullptr);
    free(str_buffer);
    return nullptr;
}
