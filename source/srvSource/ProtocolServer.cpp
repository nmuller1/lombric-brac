#include "ProtocolServer.h"
#include "../gameSource/GunCrate.h"
#include <string>
#include <vector>
#include <iostream>


int TURN = 0;
int REPLAY_TURN = 0;

/*


int isAvailable(char username[USERNAME_SIZE], FILE *userID) { // 2eme parametre est temporaire
    Fonction qui renvoie:
        1 si le pseudo n'est pas présent dans la bdd
        0 sinon

    //TODO: remplacer l'utilisation du fichier texte par l'acces bdd
    char tmpUser[USERNAME_SIZE];
    unsigned short found = 0;
    rewind(userID);
    while (fscanf(userID, "%s", tmpUser) != EOF) {
        if (!strcmp(username, tmpUser)) {  // Test de match entre pseudo et mdp
            found = 1;
            break; //pas besoin d'aller au bout du fichier
        }
    }

    return !found;
}*/

void getTeamNames(char worms[8][USERNAME_SIZE], char user[USERNAME_SIZE]) {
    // TODO: interaction bdd : renvoyer les noms de worms du joueur "user"


    //DEBUG (en attendant la bdd)
    printf("User : %s\n", user); // pour ne pas avoir un paramètre inutilisé :)
    for (int i = 0; i < 8; i++) {
        strcpy(worms[i], "Michel");
    }
}

int alreadyExist(char username[USERNAME_SIZE], char connected[MAX_NB_PLAYER][USERNAME_SIZE]) {
    int res = 0;
    for (unsigned i = 0; i < MAX_NB_PLAYER; i++) { // c'est très bourrain, mais ça fait le taf
        if (!strcmp(username, connected[i])) {
            res = 1;
            break;
        }
    }
    return res;
}

int grantAccess(char username[USERNAME_SIZE], char connected[MAX_NB_PLAYER][USERNAME_SIZE], int socket_cible) {
    /* C'est fait de façon dégueulasse mais pour un MAX_NB_PLAYER petit, ça ne devrait pas avoir de conséquences */

    char str_buffer[TAILLE_BUFFER];
    int res;

    printf("OPEN THE GATE (access granted)\n"); //debug

    int i;
    for (i = 0; i <
                MAX_NB_PLAYER; i++) { //boucle qui se positionne sur la prochaine place libre dans la liste des joueurs connectés
        if (!strcmp(connected[i], "")) break;
    }
    strcpy(connected[i], username); // la liste des joueurs connectés à été mise à jour.

    // notifie au client que l'accès a été authorisé.
    strcpy(str_buffer, "1");
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer la réponse de grantAccess.\n");
        return -1;
    }

    return 1;
}

int denyAccess(int socket_cible) {
    char str_buffer[TAILLE_BUFFER];
    int res;

    printf("HODOR (acces denied)\n"); //debug

    // notifie au client que l'accès a été refusé
    strcpy(str_buffer, "0");
    res = sendData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible d'envoyer la réponse de denyAccess.\n");
        return -1;
    }

    return 0;
}

int connectPlayer(char username[USERNAME_SIZE], char connected[MAX_NB_PLAYER][USERNAME_SIZE], int socket_cible,
                  int nb_connected) {
    /*
    Fonction retourne -1 en cas d'erreur d'exécution, 0 si l'accès est refusé à l'utilisateur, 1 si l'accès est authorisé à l'utilisateur.
    */
    int res;
    if (!alreadyExist(username, connected)) {
        if (nb_connected < MAX_NB_PLAYER) {
            res = grantAccess(username, connected, socket_cible);
            if (res == -1) {
                printf("Impossible de garantir la connexion entre le client et le serveur.(grantAccess returned -1).\n");
                return -1;
            }
        } else {
            printf("Connexion refusée : server too busy.\n");
            return denyAccess(socket_cible);
        }

    } else {
        printf("Connexion refusée : user already logged in.\n");
        return denyAccess(socket_cible);
    }

    printf("%s vient de se connecter.\n", username);
    return 1;
}

int disconnect(char username[16], char connected[50][16], pthread_mutex_t mutex) {

    //DEBUG
    printf("==================================================\n");
    printf("Liste des joueurs connectés (avant déconnexion):\n");
    for (int i = 0; i < MAX_NB_PLAYER; i++) {
        if (!strcmp(connected[i], "")) break;
        printf("-%s\n", connected[i]);
    }
    printf("==================================================\n\n\n");

    int res;
    res = pthread_mutex_lock(&mutex);
    if (res != 0) {
        printf("Mutex impossible, impossible de continuer.\n");
        return -1;
    }
    for (int i = 0; i < MAX_NB_PLAYER; i++) {
        if (!strcmp(connected[i], username)) {
            strcpy(connected[i], ""); //reset la place pour une future connexion
            break;
        }
    }
    pthread_mutex_unlock(&mutex);

    // DEBUG
    printf("==================================================\n");
    printf("Liste des joueurs connectés (après déconnexion):\n");
    for (int i = 0; i < MAX_NB_PLAYER; i++) {
        if (!strcmp(connected[i], "")) break;
        printf("-%s\n", connected[i]);
    }
    printf("==================================================\n\n\n");

    return -1;
}

int manageConnexion(int socket_cible, char user[USERNAME_SIZE], Database &database) { //userID paramètre temporaire
    char str_buffer[TAILLE_BUFFER];
    char username[USERNAME_SIZE]; //taille fixée à 16, voir si ça convient à tout le monde
    char password[PW_SIZE];

    int res;
    while (true) { // s'achève quand il y a match entre pseudo et mdp

        // check si le client retourne au menu principal
        res = recvData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Erreur à la réception des données.(pseudo)\n");
            return -1;
        }
        if (!strcmp(str_buffer, "$ret")) {
            return -25; // return value code
        }

        res = recvData(username, socket_cible);
        if (res == -1) {
            printf("Erreur à la réception des données.(pseudo)\n");
            return -1;
        }

        res = recvData(password, socket_cible);
        if (res == -1) {
            printf("Erreur à la réception des données.(mdp)\n");
            return -1;
        }

        // ~~~~~~~~ COMPARAISON AVEC FICHIER TEXTE ~~~~~~~~ TODO: bdd
        /*
        char tmpUser[USERNAME_SIZE], tmpPW[PW_SIZE];
        unsigned found = 0;
        rewind(userID); // avant chaque lecture, on revient au top du fichier
        while (fscanf(userID, "%s %s", tmpUser, tmpPW) != EOF) {
            if (!strcmp(username, tmpUser) && !strcmp(password, tmpPW)) {  // Test de match entre pseudo et mdp
                found = 1;
                break; // on a trouvé le match
            }
        }*/

        //BDD
        int accepted;

        accepted = database.checkLogin(username, password);
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        if (accepted != -1) {
            strcpy(str_buffer, "1");
            res = sendData(str_buffer, socket_cible); // notifie 1 au client (connection valide)
            if (res == -1) {
                printf("Impossible d'envoyer le résultat de l'authentification au client.\n");
                return -1;
            }
            strcpy(user,
                   username); // met à jour le paramètre qui sert au serveur à garder une trace de qui est connecté
            break; // authentification accomplie, on quitte le while(true)
        } else {
            strcpy(str_buffer, "0");
            res = sendData(str_buffer,
                           socket_cible); // notifie 0 au client (connection invalide : on a parcouru tout le fichier sans trouver de correspondance)
            if (res == -1) {
                printf("Impossible d'envoyer le résultat de l'authentification au client.\n");
                return -1;
            }
        }
    }

    return 1;
}

int manageRegister(int socket_cible, Database &database) {

    char str_buffer[TAILLE_BUFFER];
    char username[USERNAME_SIZE]; //taille fixée à 16, voir si ça convient à tout le monde
    char password[PW_SIZE];
    std::string bUsername;
    std::string bPassword;
    int res;

    while (true) { // sort quand on est sur qu'on traite un pseudo disponible

        // ============== ETAPE 0 : check si le client retourne au menu principal ===============

        res = recvData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Erreur à la réception des données.(pseudo)\n");
            return -1;
        }
        if (!strcmp(str_buffer, "$ret")) {
            return -25; // return value code
        }

        // ============== ETAPE 1 : check si le pseudo est disponible ===============

        res = recvData(username, socket_cible);
        if (res == -1) {
            printf("Erreur à la réception des données.(pseudo)\n");
            return -1;
        }

        if (database.getID(username) == -1) {
            strcpy(str_buffer, "1");
            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Erreur à l'envoi du message.(validation disponibilité pseudo");
                return -1;
            }
            break; // car un pseudo valide a été trouvé
        } else { //un peu redondant mais bon
            strcpy(str_buffer, "0");
            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Erreur à l'envoi du message.(validation disponibilité pseudo");
                return -1;
            }
        }


    }

    // ============ ETAPE 2 : réception du mot de passe ================== //TODO: faire une double confirmation de mot de passe

    res = recvData(password, socket_cible);
    if (res == -1) {
        printf("Erreur à la réception des données.(mdp)\n");
        return -1;
    }
    // ============= ETAPE 3 : nom des lombrics ==================


    std::vector<std::string> wormsNames;// on se dit que la taille maximale de nom est identique à celle d'un pseudo
    for (unsigned short i = 0; i != 8; i++) {
        char wormName[USERNAME_SIZE];
        //réception du nom du ième lombric
        receiveChar(wormName, socket_cible, const_cast<char *>("Nom des lombrics"));
        wormsNames.emplace_back(wormName);
    }
    printf("Enregistrement dans la BDD\n");
    database.newUser(username, password, "Pauline",
                     wormsNames); //Pas besoin de verifier la valeur de retour car getId le fait deja

    //TODO:
    // ~~~~~~~~~~~~~~~~~ Ajouter une nouvelle entrée à la bdd ~~~~~~~~~~~~~~~~~~~~~~~

    // les variables username, password et worms_names contiennent les infos relatives à un joueur à ajouter à la bdd

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    return 1;
}

GameParameters managePlayGame(int socket_cible, Room room_tab[], char clientName[], int *current_room) {
    /* fonction parrallèle à playGame (côte client) */

    std::cout << "ManagePlayGame - flag" << std::endl;

    char str_buffer[TAILLE_BUFFER];
    int res;
    GameParameters GP{};

    // on reçoit le mode (action suivante)
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Erreur à la réception du mode (managePlayGame).\n");
        GP.error = -1;
        return GP;
    }

    if (!strcmp(str_buffer, "1")) { // CREER UNE PARTIE
        GP = manageCreateGame(socket_cible, room_tab, clientName, current_room);
        if (GP.error == -1) {
            return GP;
        }
    }
    if (!strcmp(str_buffer, "2")) { // REJOINDRE UNE PARTIE
        manageJoinGame(socket_cible, room_tab, clientName, current_room);
        if (GP.error == -1) {
            return GP;
        }
    }
    return GP;
}

GameParameters manageCreateGame(int socket_cible, Room room_tab[], char clientName[], int *current_room) {
    int res;
    char str_buffer[TAILLE_BUFFER];
    GameParameters GP;
    Database db("bdd.db");

    std::cout << "ManageCreateGame - flag" << std::endl;

    // RECEPTION DE PARAMETRE : friend game ? (FRIENDS)
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Dans manageCreateGame : Erreur à la réception du paramètre : friend_game.\n");
        GP.error = -1;
        return GP;
    }
    GP.friend_game = (int) strtol(str_buffer, nullptr, 10);


    // RECEPTION DE PARAMETRE : mode de jeu //TODO: pas dans la v1
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Dans manageCreateGame : Erreur à la réception du paramètre : mode de jeu.\n");
        GP.error = -1;
        return GP;
    }
    GP.game_mode = (int) strtol(str_buffer, nullptr, 10);

    // RECEPTION DE PARAMETRE : nombre de joueurs dans la partie
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Dans manageCreateGame : Erreur à la réception du paramètre : nombre de joueurs dans la partie.\n");
        GP.error = -1;
        return GP;
    }
    GP.no_of_players = (int) strtol(str_buffer, nullptr, 10);

    // MME désactivé pour un nombre impair de joueurs
    if (GP.game_mode == 2 && GP.no_of_players % 2 != 0) {
        GP.game_mode = 1;
    }

    // RECEPTION DE PARAMETRE : nombre de vers par joueur
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Dans manageCreateGame : Erreur à la réception du paramètre : nombre de vers par joueur.\n");
        GP.error = -1;
        return GP;
    }
    GP.no_of_worms = (int) strtol(str_buffer, nullptr, 10);

    // RECEPTION DE PARAMETRE : temps d'un tour
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Dans manageCreateGame : Erreur à la réception du paramètre : temps d'un tour.\n");
        GP.error = -1;
        return GP;
    }
    GP.max_round_time = (int) strtol(str_buffer, nullptr, 10);

    // RECEPTION DE PARAMETRE : points de vie initiaux
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Dans manageCreateGame : Erreur à la réception du paramètre : points de vie initiaux.\n");
        GP.error = -1;
        return GP;
    }
    GP.init_life_pts = (int) strtol(str_buffer, nullptr, 10);

    // RECEPTION DE PARAMETRE : crate spawnrate
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Dans manageCreateGame : Erreur à la réception du paramètre : crate spawnrate.\n");
        GP.error = -1;
        return GP;
    }
    GP.packs_frequence = (int) strtol(str_buffer, nullptr, 10);

    // RECEPTION DE PARAMETRE : points de vie par medkit
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Dans manageCreateGame : Erreur à la réception du paramètre : points de vie par medkit.\n");
        GP.error = -1;
        return GP;
    }
    GP.pts_per_life_pack = (int) strtol(str_buffer, nullptr, 10);

    Room player_room; //= {tab, GP};
    player_room._GP = GP;
    player_room._current_nbre_player = 0;
    player_room._creatorID = db.getID(clientName);
    if (GP.friend_game == 2) {
        player_room._friendRoom = GP.friend_game;
        db.updateMatchStatus(player_room._creatorID, 1);
        // (FRIENDS)
    }
    addToRoom(&player_room, Player(clientName, player_room._current_nbre_player + 2));
    player_room._sockets_tab[player_room._current_nbre_player - 1] = socket_cible;
    room_tab[*current_room] = player_room;
    GP.room_index = *current_room;

    return GP;
}

int manageJoinGame(int socket_cible, Room room_tab[], char clientName[], int *current_room) {
    /* Fonction parralèle à joinGame (côté client) */

    std::cout << "ManageJoinGame - flag" << std::endl;

    char str_buffer[TAILLE_BUFFER];
    int res;

    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Impossible de recevoir le parametre friend_game (manageJoinGame)");
    }
    if (!strcmp(str_buffer, "1")) {
        std::cout << "join res == 1" << std::endl;
        bool flag = true;
        for (int i = 0; i < 16; i++) {
            if (room_tab[i]._current_nbre_player != room_tab[i]._GP.no_of_players) {
                addToRoom(&room_tab[i], Player(clientName, room_tab[i]._current_nbre_player + 1));
                room_tab[i]._sockets_tab[room_tab[i]._current_nbre_player - 1] = socket_cible;
                flag = false;
            }
        }
        if (flag) {
            std::cout << "Aucune partie n'est disponible" << std::endl;
            manageCreateGame(socket_cible, room_tab, clientName, current_room);
        }
    } else if (!strcmp(str_buffer, "2")) {
        // (FIRENDS)
        int roomID;
        std::cout << "join res == 2" << std::endl;
        res = recvData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Impossible de recevoir l'id de la room choisie (manageJoinGame)");
            return -1;
        }
        roomID = (int) strtol(str_buffer, nullptr, 10);
        for (int i = 0; i < 16; i++) {
            if (room_tab[i]._creatorID == roomID) {
                addToRoom(&room_tab[res], Player(clientName, room_tab[res]._current_nbre_player + 1));
            }
        }
    }

    return 1;
}

int sendRanking(int socket_cible) { //FIXME: WIP
    /*
    Fonction qui envoie le classement au client selon des filtres choisis.
    */

    // Ranking_filters RF;
    // RF = recvRankingFilters(socket_cible);
    // if (RF.error == -1) {
    //     printf("Erreur à la réception des filtres du classement.\n");
    //     return -1;
    // }
    int res;
    char int_to_str[10];
    char *buff = nullptr;
    //BDD: appelle de fonction ici
    std::vector<std::vector<std::string>> ranking;
    ranking.push_back({"user1", "01", "58"});
    ranking.push_back({"user2", "02", "65"});
    ranking.push_back({"user3", "03", "25"});
    ranking.push_back({"user4", "04", "85"});
    ranking.push_back({"user5", "05", "14"});
    ranking.push_back({"user6", "06", "20"});

    int nbrOfPlayers = ranking.size();
    int attr_nb = ranking[0].size();
    sprintf(int_to_str, "%d", nbrOfPlayers);
    res = sendData(int_to_str, socket_cible);
    if (res == -1) {
        printf("Erreur envoie du nombre de lignes du classement\n");
        return -1;
    }

    sprintf(int_to_str, "%d", attr_nb);
    res = sendData(int_to_str, socket_cible);
    if (res == -1) {
        printf("Erreur envoie du nombre d'attributs du classement\n");
        return -1;
    }

    for (int i = 0; i < nbrOfPlayers; i++) {
        for (int j = 0; j < attr_nb; j++) {
            //first, conversion du string en char*
            const int N = (int) ranking[i][j].length() + 1;
            buff = new char[N];
            strcpy(buff, ranking[i][j].c_str());

            res = sendData(buff, socket_cible);
            if (res == -1) {
                printf("Erreur envoie du classement\n");
                return -1;
            }
            printf("Sending: %s\n", buff);
        }

    }
    delete[] buff;
    //Version simplifié, on a pas utiliser la classe Ranking

    return 1;
}

Ranking_filters recvRankingFilters(int socket_cible) {
    int res;
    char str_buffer[TAILLE_BUFFER];
    Ranking_filters RF;

    // RECOIT FILTRE : DUREE CLASSEMENT
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Erreur réception filtre (durée classement).\n");
        RF.error = -1;
        return RF;
    }
    RF.time = (int) strtol(str_buffer, nullptr, 10);

    // RECOIT FILTRE : MODE DE JEU
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Erreur réception filtre (mode de jeu).\n");
        RF.error = -1;
        return RF;
    }
    RF.gamemode = (int) strtol(str_buffer, nullptr, 10);

    // RECOIT FILTRE : SYSTEME D'ÉVALUATION
    res = recvData(str_buffer, socket_cible);
    if (res == -1) {
        printf("Erreur réception filtre (système d'évalutation).\n");
        RF.error = -1;
        return RF;
    }
    RF.evaluationSystem = (int) strtol(str_buffer, nullptr, 10);

    return RF;
}

int getValidUsername(int socket_cible, Database &database) { //FIXME: param 2 is temporary
    int res;
    char str_buffer[TAILLE_BUFFER];

    while (true) {
        // on va recevoir des noms d'utilisateur jusqu'à en trouver un qui existe
        res = recvData(str_buffer, socket_cible);
        if (res == -1) {
            printf("Impossible de recevoir le nom du profl à consulter.\n");
        }

        // envoi "1" si le pseudo est celui d'un joueur existant, "0" sinon.
        if (database.getID(str_buffer)) {
            strcpy(str_buffer, "1");
            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Dans la consultation de profil : impossible d'envoyer la réponse sur l'existence du pseudo cherché.\n");
            }
            break;
        } else {
            strcpy(str_buffer, "0");
            res = sendData(str_buffer, socket_cible);
            if (res == -1) {
                printf("Dans la consultation de profil : impossible d'envoyer la réponse sur l'existence du pseudo cherché.\n");
            }
        }
    }
    return 1;
}

int getNextAlive(int current_player, int nbre_joueur, const bool *alivePlayer) {
    int offset = 0;
    while (!alivePlayer[(current_player + offset) % nbre_joueur]) offset++;
    return (current_player + offset) % nbre_joueur;
}

void addToRoom(Room *room, const Player &player) {
    room->_current_nbre_player++;
    room->_players_tab[room->_current_nbre_player - 1] = player;
}

int start_a_game(Room room, GameParameters GP, Database db) {
    std::string map_txt = "default.map";
    Game *game = new Game(map_txt);
    game->setRoom(room);
    int res;
    char str_buffer[TAILLE_BUFFER];
    char current_worm_number[2];
    Worm *tmp_worm_tab[4][8];

    bool replayOn = false;
    int replayID = -1;

    int currentMatchID = 0;

    std::string tmp; // servira à passer des des char[] en parametre à db.newMove

    std::string userNamesTab[4];
    for (int i = 0; i < GP.no_of_players; i++) {
        userNamesTab[i] = room._players_tab[i].getPseudo();
    }

    for (int i = 0; i < GP.no_of_players; i++) {
        // Envoi du nombre de joueur

        sprintf(str_buffer, "%d", GP.no_of_players);
        res = sendData(str_buffer, room._sockets_tab[i]);
        if (res == -1) {
            printf("Erreur à l'envoi du nombre de joueurs (start_a_game).\n");
            return -1;
        }

        sprintf(str_buffer, "%d", i);
        res = sendData(str_buffer, room._sockets_tab[i]);
        if (res == -1) {
            printf("Erreur à l'envoi du nombre de l'id (start_a_game).\n");
            return -1;
        }

        // Envoi du nombre de worms
        sprintf(str_buffer, "%d", GP.no_of_worms);
        res = sendData(str_buffer, room._sockets_tab[i]);
        if (res == -1) {
            printf("Erreur à l'envoi du nombre de vers (start_a_game).\n");
            return -1;
        }

        //Envoi du nombre de hp par medkit
        sprintf(str_buffer, "%d", GP.pts_per_life_pack);
        res = sendData(str_buffer, room._sockets_tab[i]);
        if (res == -1) {
            printf("Erreur à l'envoi du nombre hp par medkit (start_a_game).\n");
            return -1;
        }

        //Envoi du nombre de hp d'un vers
        sprintf(str_buffer, "%d", GP.init_life_pts);
        res = sendData(str_buffer, room._sockets_tab[i]);
        if (res == -1) {
            printf("Erreur à l'envoi du nombre hp d'un vers (start_a_game).\n");
            return -1;
        }

        // envoi temps d'un tour
        sprintf(str_buffer, "%d", GP.max_round_time);
        res = sendData(str_buffer, room._sockets_tab[i]);
        if (res == -1) {
            printf("Erreur à l'envoi du temps d'un tour (start_a_game).\n");
            return -1;
        }

        // envoi mode de jeu
        sprintf(str_buffer, "%d", GP.game_mode);
        res = sendData(str_buffer, room._sockets_tab[i]);
        if (res == -1) {
            printf("Erreur à l'envoi du mode de jeu (start_a_game).\n");
            return -1;
        }
    }



    // réception mode replay
    res = recvData(str_buffer, room._sockets_tab[0]); //réception sur le socket du créateur uniquement
    if (res == -1) {
        printf("Erreur à la réception de replay/no replay.\n");
        return -1;
    }


    if (!strcmp(str_buffer, "REPLAY")) {
        replayOn = true;


        //TODO: envoi des parties choisies pour l'affichage
        std::vector<std::vector<std::string>> matchs;
        db.getToStringMatchs(userNamesTab[0], matchs, 10, GP.game_mode);

        std::cout << "alive" << std::endl;
        std::cout << matchs[0].size() << std::endl; //debug FIXME: cette ligne segfault le server

        sprintf(str_buffer, "%lu", matchs.size());
        res = sendData(str_buffer, room._sockets_tab[0]);
        if (res == -1) {
            printf("Erreur à l'envoi de matchs.size (envoi replay list)\n");
            return -1;
        }

        sprintf(str_buffer, "%lu", matchs[0].size());
        res = sendData(str_buffer, room._sockets_tab[0]);
        if (res == -1) {
            printf("Erreur à l'envoi de matchs[i].size (envoi replay list)\n");
            return -1;
        }

        //envoi de l'historique de partie d'un joueur
        for (auto &match : matchs) {
            for (auto &j : match) {
                strcpy(str_buffer, j.data());
                res = sendData(str_buffer, room._sockets_tab[0]);
                if (res == -1) {
                    printf("Erreur à l'envoi d'une ligne d'historique de partie dans la sélection de la partie à replay\n");
                    return -1;
                }
            }
        }

        // réception du replay id choisi
        res = recvData(str_buffer, room._sockets_tab[0]);
        if (res == -1) {
            printf("Erreur à la réception de replayID.\n");
            return -1;
        }
        replayID = (int) strtol(str_buffer, nullptr, 10);

        //paramètres de partie
        //GP.no_of_players = db.getNbPlayers(replayID); on va partir du principe que l'utilisateur choisit le bon nombre de joueur, j'ai pas de temps à perdre
        GP.no_of_worms = db.getNbWorms(replayID);
        GP.init_life_pts = db.getInitlifePoints(replayID);
        GP.pts_per_life_pack = db.getMedKitLifePoints(replayID);
        GP.packs_frequence = db.getMedKitFrequency(replayID);
        GP.max_round_time = db.getMaxRoundTime(replayID);
        GP.game_mode = db.getGameMode(replayID);

        //envoi de tous les parametre (sauf packs_frequence)
        char temp[100];

        for (int i = 0; i < GP.no_of_players; i++) {
            sprintf(temp, "%d", GP.no_of_worms);
            res = sendData(temp, room._sockets_tab[i]);
            if (res == -1) {
                printf("Erreur à l'envoi d'un parametre de partie (mode replay)\n");
                return -1;
            }
            sprintf(temp, "%d", GP.init_life_pts);
            res = sendData(temp, room._sockets_tab[i]);
            if (res == -1) {
                printf("Erreur à l'envoi d'un parametre de partie (mode replay)\n");
                return -1;
            }
            sprintf(temp, "%d", GP.pts_per_life_pack);
            res = sendData(temp, room._sockets_tab[i]);
            if (res == -1) {
                printf("Erreur à l'envoi d'un parametre de partie (mode replay)\n");
                return -1;
            }
            sprintf(temp, "%d", GP.max_round_time);
            res = sendData(temp, room._sockets_tab[i]);
            if (res == -1) {
                printf("Erreur à l'envoi d'un parametre de partie (mode replay)\n");
                return -1;
            }
            sprintf(temp, "%d", GP.game_mode);
            res = sendData(temp, room._sockets_tab[i]);
            if (res == -1) {
                printf("Erreur à l'envoi d'un parametre de partie (mode replay)\n");
                return -1;
            }
        }
    }
    std::cout << "Replay ? : " << replayOn << std::endl; //Debug
    std::cout << "ReplayID : " << replayID << std::endl; //debug

    if (GP.no_of_players == 2) {
        currentMatchID = db.newMatch(GP.game_mode, GP.no_of_worms, GP.init_life_pts, GP.pts_per_life_pack,
                                     GP.packs_frequence, GP.max_round_time, userNamesTab[0], userNamesTab[1]);
    }
    if (GP.no_of_players == 3) {
        currentMatchID = db.newMatch(GP.game_mode, GP.no_of_worms, GP.init_life_pts, GP.pts_per_life_pack,
                                     GP.packs_frequence, GP.max_round_time, userNamesTab[0], userNamesTab[1],
                                     userNamesTab[2]);
    }
    if (GP.no_of_players == 4) {
        currentMatchID = db.newMatch(GP.game_mode, GP.no_of_worms, GP.init_life_pts, GP.pts_per_life_pack,
                                     GP.packs_frequence, GP.max_round_time, userNamesTab[0], userNamesTab[1],
                                     userNamesTab[2], userNamesTab[3]);
    }
    std::cout << currentMatchID << std::endl;//debug






    // =============== ENVOI DU TOP DEPART A TOUS LES SOCKETS ==============

    for (int i = 0; i < GP.no_of_players; i++) {
        std::cout << "Top départ : " << i << std::endl;
        strcpy(str_buffer, "1");
        res = sendData(str_buffer, room._sockets_tab[i]);
        if (res == -1) {
            printf("Erreur à l'envoi du top départ.\n");
            return -1;
        }

        // =================== RECEPTION DES NOMS DES LOMBRICS D'UN JOUEUR (pour chaque joueur) ====================

        for (int j = 0; j < 8; j++) {
            res = recvData(str_buffer, room._sockets_tab[i]);
            if (res == -1) {
                printf("Erreur à la réception d'un nom de ver.\n");
                return -1;
            }

            sprintf(current_worm_number, "%d", j);
            tmp_worm_tab[i][j] = new Worm(str_buffer, current_worm_number[0], i + 2, GP.init_life_pts,
                                          i); //TODO: new sans delete
        }
        std::cout << "worms names well received" << std::endl;

        room._players_tab[i].getTeam()->setWroms(tmp_worm_tab[i]);
    }

    std::cout << "Top départ envoyé" << std::endl;

    //Creation equipe si game_mode == MME
    if (GP.game_mode == 2) {
        int equipe_1_index = 0;
        int equipe_2_index = 1;
        int equipe_1_colour = 2;
        int equipe_2_colour = 5;
        while (equipe_1_index <= GP.no_of_players - 1 && equipe_2_index <= GP.no_of_players - 1) {
            for (int i = 0; i < GP.no_of_worms; i++) {
                room._players_tab[equipe_1_index].getTeam()->getWorms()[i]->setInGameTeamNbre(equipe_1_colour);
                room._players_tab[equipe_1_index].getTeam()->getWorms()[i]->setColour(equipe_1_colour);
                std::cout << "Equipe 1 couleur : "
                          << room._players_tab[equipe_1_index].getTeam()->getWorms()[i]->getColour() << std::endl;
                room._players_tab[equipe_2_index].getTeam()->getWorms()[i]->setInGameTeamNbre(equipe_2_colour);
                room._players_tab[equipe_2_index].getTeam()->getWorms()[i]->setColour(equipe_2_colour);
                std::cout << "Equipe 1 couleur : "
                          << room._players_tab[equipe_2_index].getTeam()->getWorms()[i]->getColour() << std::endl;
            }
            equipe_1_index += 2;
            equipe_2_index += 2;
        }
    }

    // ============ GENERATE WORMS + envoi la position des vers de tous les joueurs ================

    int *x_coord_tab = new int[GP.no_of_worms * GP.no_of_players];
    if (!replayOn) {
        for (int i = 0; i < GP.no_of_players; i++) {
            game->generateWorms(room._players_tab[i].getTeam(), GP.no_of_worms, x_coord_tab, i * GP.no_of_worms);
        }

        bool stocked = false;
        for (int j = 0; j < GP.no_of_players; j++) {

            for (int k = 0; k < GP.no_of_worms * GP.no_of_players; k++) {
                sprintf(str_buffer, "%d", x_coord_tab[k]);
                res = sendData(str_buffer, room._sockets_tab[j]);
                if (res == -1) {
                    printf("Erreur à l'envoi d'une coord x.\n");
                    return -1;
                }
                if (!stocked) {
                    tmp = std::string(str_buffer);
                    db.newMove(currentMatchID, "", TURN, tmp);
                    TURN++;
                }
            }
            strcpy(str_buffer, "-1");
            res = sendData(str_buffer, room._sockets_tab[j]);
            if (res == -1) {
                printf("Erreur à l'envoi du -1 de fin.\n");
                return -1;
            }
            if (!stocked) {
                tmp = std::string(str_buffer);
                db.newMove(currentMatchID, "", TURN, tmp);
                TURN++;
            }
            stocked = true; // évite les doublons liés à l'envoi à plusieurs joueurs
        }
    } else {
        bool stocked = false;
        for (int i = 0; i < GP.no_of_players; i++) {
            for (int j = 0; j < (GP.no_of_worms * GP.no_of_players) + 1; j++) {
                db.getMove(replayID, REPLAY_TURN, tmp);
                REPLAY_TURN++;

                strcpy(str_buffer, tmp.data());
                res = sendData(str_buffer, room._sockets_tab[j]);
                if (res == -1) {
                    printf("Erreur à l'envoi d'une coord x.\n");
                    return -1;
                }

                if (!stocked) {

                    db.newMove(currentMatchID, "", TURN, tmp);
                    TURN++;
                }
            }
            stocked = true;
        }
    }

    delete[] x_coord_tab;

    // ============== Envoi les team de tous les joueurs, à tous les joueurs =============

    //FIXME: il faudra peut etre stocker quelque chose ici dans la bdd aussi

    for (int i = 0; i < GP.no_of_players; i++) { //for qui itère pour envoyer à tous les sockets
        for (int j = 0; j < GP.no_of_players; j++) { //for qui itère pour envoyer tous les joueurs
            for (int k = 0; k < GP.no_of_worms; k++) { //for qui envoie, pour chaque joueur, sa team de vers
                strcpy(str_buffer,
                       room._players_tab[j].getTeam()->getWorms()[k]->getName().data()); //cette ligne gagne le concours des lignes les plus immondes jamais écrite
                res = sendData(str_buffer, room._sockets_tab[i]);
                if (res == -1) {
                    printf("Je sais même plus quoi penser, il est tard\n");
                    return -1;
                }
                if (GP.game_mode == 2) {
                    snprintf(str_buffer, TAILLE_BUFFER, "%d",
                             room._players_tab[j].getTeam()->getWorms()[k]->getInGameTeamNumber());
                    res = sendData(str_buffer, room._sockets_tab[i]);
                    if (res == -1) {
                        printf("Je sais même plus quoi penser, il est tard\n");
                        return -1;

                    }
                }
            }
        }
    }

    std::cout << "a game has started" << std::endl;


    // RECEPTION DES INPUTS (la partie a déjà commencé)
    int current_player = 0;
    time_t start_time = time(nullptr);
    bool *alivePlayer = new bool[GP.no_of_players], skipped = false, firstAction = true;
    std::fill(alivePlayer, alivePlayer + GP.no_of_players,
              true); // toutes les composantes d'alivePlayer est initié à true


    // ===================== Skip du joueur ou prise d'input =================

    while (true) { // break à la condition de fin de partie atteinte
        if (firstAction) {
            generateCrate(GP.packs_frequence, game, &room, replayOn, currentMatchID, replayID, db);
            firstAction = false;
            std::cout << "crate spawned" << std::endl; //debug
        }
        if (!replayOn) {
            res = recvData(str_buffer, room._sockets_tab[current_player]);
            if (res == -1) {
                std::cout << "Erreur à la réception d'un input (serv)" << std::endl;
                return -1;
            }
            tmp = std::string(str_buffer);
            db.newMove(currentMatchID, "", TURN, tmp);
            TURN++;
        } else {
            db.getMove(replayID, REPLAY_TURN, tmp);
            REPLAY_TURN++;

            db.newMove(currentMatchID, "", TURN, tmp);
            TURN++;
        }


        if (!strcmp(str_buffer, "SKIP")) {
            std::cout << "demande de skip recue" << std::endl; //debug //FIXME: on y arrive jamais
            strcpy(str_buffer, "TIME"); // on simule ainsi la fin d'un tour
            skipped = true;
        } else {
            std::cout << "Transmiting.. - " << str_buffer << " by player : " << current_player << std::endl;

            //  boucle qui sert de passerelles d'inputs à tous les clients
            for (int i = 0; i < room._GP.no_of_players; i++) {
                if (replayOn || i !=
                                current_player) { // si ce n'est pas le joueur courant, on balance l'input pour l'appliquer en local
                    res = sendData(str_buffer, room._sockets_tab[i]);
                    if (res == -1) {
                        std::cout << "Erreur à l'envoi d'un input (serv)" << std::endl;
                    }
                }
            }
        }



        // =========================================================================


        //un tour était en fait fini, signal obligatoire
        if (!strcmp(str_buffer, "TIME")) {
            std::cout << "TIME vient de passer" << std::endl; //debug

            // le joueur qui devait jouer est-il mort ?
            std::cout << "en attente de réception de vie/mort" << std::endl; //debug
            if (!replayOn) {
                res = recvData(str_buffer, room._sockets_tab[current_player]);
                if (res == -1) {
                    printf("Impossible de recevoir l'état de vie au de mort d'un joueur.\n");
                    return -1;
                }
                tmp = std::string(str_buffer);
                db.newMove(currentMatchID, "", TURN, tmp);
                TURN++;
            } else { // mode replay
                db.getMove(replayID, REPLAY_TURN, tmp);
                REPLAY_TURN++;

                db.newMove(currentMatchID, "", TURN, tmp);
                TURN++;
            }

            // ######################### CONDITION D'ARRET DE PARTIE ! ###############################

            int tmpCount = 0;
            int winner = 0;
            if (!strcmp(str_buffer, "DEAD") || skipped) {
                skipped = false;
                std::cout << "player : " << current_player << " -> dead" << std::endl; //debug
                alivePlayer[current_player] = false;

                // ====== évaluation de la condition d'arrêt d'une partie =====
                if (GP.game_mode == 1) {
                    for (int i = 0; i < GP.no_of_players; i++) {
                        if (alivePlayer[i]) {
                            tmpCount++;
                            winner = i; // on ne s'en sert que si tmpCount == 1
                        }
                    }
                } else if (GP.game_mode == 2) {
                    int tmpInt;
                    int count_team;
                    int count_alive;
                    for (int i = 0; i < GP.no_of_players; i++) {
                        tmpInt = room._players_tab[i].getTeam()->getWorms()[0]->getInGameTeamNumber();
                        count_alive = 0;
                        count_team = 0;
                        for (int j = 0; j < GP.no_of_players; j++) {
                            if (alivePlayer[j]) {
                                count_alive++;
                                if (room._players_tab[j].getTeam()->getWorms()[0]->getInGameTeamNumber() == tmpInt) {
                                    count_team++;
                                }
                            }
                        }
                        if (count_alive == count_team) tmpCount = 1;
                    }
                }

                if (tmpCount == 1) { // la partie s'achève

                    // ETAPE 1 : dire que la partie est finie à tous les joueurs
                    strcpy(str_buffer, "END"); // la partie est finie
                    std::cout << "game is over" << std::endl; //debug
                    for (int i = 0; i < GP.no_of_players; i++) {
                        res = sendData(str_buffer, room._sockets_tab[i]);
                        if (res == -1) {
                            printf("Impossible d'envoyer le call 'la partie est finie', c'est vraiment pas de chance.\n");
                            return -1;
                        }
                    }
                    tmp = std::string(str_buffer);
                    db.newMove(currentMatchID, "", TURN, tmp);
                    TURN++;

                    // ETAPE 2 : dire à tous les joueurs leur status WIN/LOSE
                    for (int i = 0; i < GP.no_of_players; i++) {
                        if (i == winner) {
                            strcpy(str_buffer, "WIN");
                            res = sendData(str_buffer, room._sockets_tab[i]);
                            if (res == -1) {
                                printf("Impossible d'envoyer le call WIN, c'est vraiment pas de chance.\n");
                                return -1;
                            }
                            db.updateMatchStatus(currentMatchID, i +
                                                                 1); //notifie la victoire seulement (+1 car le 0 réservé aux matchs nuls)
                        } else {
                            strcpy(str_buffer, "LOSE");
                            res = sendData(str_buffer, room._sockets_tab[i]);
                            if (res == -1) {
                                printf("Impossible d'envoyer le call LOSE, c'est vraiment pas de chance.n");
                                return -1;
                            }
                        }
                    }
                    break; // sort du while(true)
                } else { // la partie continue : on doit aussi le dire à tous les joueurs

                    if (!replayOn) {
                        if (isTimeOver(start_time)) strcpy(str_buffer, "FLOOD"); // le niveau d'eau doit monter
                        else strcpy(str_buffer, "0"); //message sans signification, la partie continue
                        tmp = std::string(str_buffer);
                        db.newMove(currentMatchID, "", TURN, tmp);
                        TURN++;
                    } else {
                        db.getMove(replayID, REPLAY_TURN, tmp);
                        REPLAY_TURN++;

                        db.newMove(currentMatchID, "", TURN, tmp);
                        TURN++;
                    }

                    for (int i = 0; i < GP.no_of_players; i++) {
                        res = sendData(str_buffer, room._sockets_tab[i]);
                        if (res == -1) {
                            printf("Impossible d'envoyer le call 'la partie continue' (après mort d'un joueur)\n");
                            return -1;
                        }
                    }
                }
            } else { // message reçu != "DEAD"
                // la partie continue

                if (!replayOn) {
                    if (isTimeOver(start_time)) strcpy(str_buffer, "FLOOD"); // le niveau d'eau doit monter
                    else strcpy(str_buffer, "0"); //message sans signification, la partie continue
                    tmp = std::string(str_buffer);
                    db.newMove(currentMatchID, "", TURN, tmp);
                    TURN++;
                } else {
                    db.getMove(replayID, REPLAY_TURN, tmp);
                    REPLAY_TURN++;

                    db.newMove(currentMatchID, "", TURN, tmp);
                    TURN++;
                }

                for (int i = 0; i < GP.no_of_players; i++) {
                    res = sendData(str_buffer, room._sockets_tab[i]);
                    if (res == -1) {
                        printf("Impossible d'envoyer le call 'la partie continue' (après mort d'un joueur)\n");
                        return -1;
                    }
                }
            }
            // evolution theorique du prochain joueur
            current_player++;
            current_player = current_player % GP.no_of_players; //modulo pour rester en range

            // envoie aux clients, soit la position en X de la crate, soit -1 si il devait ne pas y en avoir de généré

            //reset
            firstAction = true;
        }
    }


    std::cout << "I'm out" << std::endl; // debug
    delete[] alivePlayer;
    return 1;
}


void generateCrate(int crateSpawnRate, Game *game, Room *room, bool replayOn, int currentMatchID, int replayID,
                   Database db) {


    srand((unsigned) time(nullptr));
    int spawnX = 0, type = 0, crateType =0, test, res;

    char str_spawnX[TAILLE_BUFFER];
    char str_type[TAILLE_BUFFER];
    char str_crateType[TAILLE_BUFFER];

    std::string tmp;


    if (!replayOn) {
        test = rand() % 100; // génère une val entre 0 et 99;
        if (test < crateSpawnRate) {
            //réussi
            do {
                spawnX = rand() % game->getMapWidth();
            } while (game->getMatrix()[0][spawnX]->getType() != AIR);
            type = rand() % 2; //medkit ou guncrate ?
            if (type) {
                //guncrate
                crateType = rand() % Content::SIZE;
            }
        } else {
            //échec
            spawnX = -1;
        }
    } else { // MODE REPLAY
        db.getMove(replayID, REPLAY_TURN, tmp);
        REPLAY_TURN++;

        db.newMove(currentMatchID, "", TURN, tmp);
        TURN++;
        if (strcmp(str_spawnX, "-1") != 0) {
            //attention pas le ! devant, strcmp renvoi 0 si la valeur == "-1" (c'est ce qu'on veut)
            db.getMove(replayID, REPLAY_TURN, tmp);
            REPLAY_TURN++;

            db.newMove(currentMatchID, "", TURN, tmp);
            TURN++;
            if (strcmp(str_type, "0") != 0) { //on a autre chose que "0"
                db.getMove(replayID, REPLAY_TURN, tmp);
                REPLAY_TURN++;

                db.newMove(currentMatchID, "", TURN, tmp);
                TURN++;
            }
        }
    }

    printf("spawnX: %d, test: %d, crateType: %d\n", spawnX, type, crateType);//debug


    //  ~~~~~~~~~~~~~~~~ ENVOI ~~~~~~~~~~~~~~~~~~
    sprintf(str_spawnX, "%d", spawnX);
    sprintf(str_type, "%d", type);
    sprintf(str_crateType, "%d", crateType);

    for (int i = 0; i < room->_GP.no_of_players; i++) {

        res = sendData(str_spawnX, room->_sockets_tab[i]);
        if (res == -1) {
            std::cout << "Erreur à l'envoi de la coord X (generate crate)" << std::endl;
        }
        tmp = std::string(str_spawnX);
        db.newMove(currentMatchID, "", TURN, tmp);
        TURN++;

        if (spawnX >= 0) {
            res = sendData(str_type, room->_sockets_tab[i]);
            if (res == -1) {
                std::cout << "Erreur à l'envoi de medkit/guncrate (generate crate)" << std::endl;
            }

            tmp = std::string(str_type);
            db.newMove(currentMatchID, "", TURN, tmp);
            TURN++;

            if (type) { // envoi nécessaire en cas de guncrate seulement (medkit == 0 don en valide pas)
                res = sendData(str_crateType, room->_sockets_tab[i]);
                if (res == -1) {
                    std::cout << "Erreur à l'envoi du type de guncrate (generate crate)" << std::endl;
                }
                tmp = std::string(str_crateType);
                db.newMove(currentMatchID, "", TURN, tmp);
                TURN++;
            }
        }
    }
}

bool isTimeOver(time_t start) {
    time_t current_time = time(nullptr);
    return (current_time - start) > GAME_TIME_MAX;
}

int connectToChat(char username[USERNAME_SIZE], int socket_client, userInfo connectedToChat[MAX_NB_PLAYER]) {
    //TODO: verifier que le client n'est pas deja connecté
    userInfo user;
    strcpy(user._username, username);
    user._socket = socket_client;
    user.significant = true;
    int i;
    for (i = 0; i < MAX_NB_PLAYER; i++) {
        if (!connectedToChat[i].significant) {
            connectedToChat[i] = user;
            return 1;
        }
    }
    //Atteint le nombre max de client
    return 0;
}

int disconectToChat(char username[USERNAME_SIZE], userInfo connectedToChat[MAX_NB_PLAYER]) {
    int i;
    for (i = 0; i < MAX_NB_PLAYER; i++) {
        if (!strcmp(connectedToChat[i]._username, username)) {
            connectedToChat[i].significant = false;
            return 1;
        }
    }
    //L'utilisateur n'etait pas connecté
    return 0;
}

int getPlayersSocket(char username[USERNAME_SIZE], userInfo connectedToChat[MAX_NB_PLAYER]) {
    int i;
    for (i = 0; i < MAX_NB_PLAYER; i++) {
        if (!strcmp(connectedToChat[i]._username, username)) {
            return connectedToChat[i]._socket;
        }
    }
    //L'utilisateur n'etait pas connecté
    return -1;

}

int sendFriendList(int socket_client, char clientName[USERNAME_SIZE], Database &database) {
    int res;
    int size;
    char str_buffer[USERNAME_SIZE];
    uint32_t packet_size;
    std::vector<std::string> friendList;

    database.getFriendsList(clientName, friendList);
    size = friendList.size();
    packet_size = htonl(size);

    printf("Reçu la liste d'amis\n");
    res = send(socket_client, &packet_size, sizeof(uint32_t), 0);
    if (res == -1) {
        return -1;
    }
    for (int i = 0; i < size; i++) {
        strcpy(str_buffer, friendList[i].c_str());
        res = sendData(str_buffer, socket_client);
        if (res == -1) {
            return -1;
        }
    }

    return 1;
}

int sendFriendRequest(int socket_client, char clientName[USERNAME_SIZE], Database &database) {

    //TODO: boucler tant que l'utilisateur n'abandone pas ou le newFriendRequest marche
    int res;
    char buff[10];
    char addressee[USERNAME_SIZE];

    res = recvData(addressee, socket_client);
    if (res == -1) {
        return -1;
    }

    res = database.newFriendRequest(clientName, addressee);

    sprintf(buff, "%d", res);
    res = sendData(buff, socket_client);
    if (res == -1) {
        return -1;
    }

    return 1;

}

int sendFriendRequestList(int socket_client, char clientName[USERNAME_SIZE], Database &database) {

    int res;
    int size;
    char mode[10];
    uint32_t packet_size;
    char str_buffer[USERNAME_SIZE];

    std::vector<std::string> requestsList;

    res = database.getReceivedFriendRequests(clientName, requestsList);

    if (res != 0) size = 0;
    else size = requestsList.size();

    packet_size = htonl(size);

    res = send(socket_client, &packet_size, sizeof(uint32_t), 0);
    if (res == -1) {
        return -1;
    }

    for (int i = 0; i < size; i++) {
        strcpy(str_buffer, requestsList[i].c_str());
        printf("Sending: %s\n", str_buffer);
        res = sendData(str_buffer, socket_client);
        if (res == -1) {
            return -1;
        }
    }

    if (size > 0) {
        res = recvData(mode, socket_client);
        if (res == -1) {
            return -1;
        }

        res = recvData(str_buffer, socket_client);
        if (res == -1) {
            return -1;
        }

        printf("Reçu mode:%s %s\n", mode, str_buffer);
        /*mode == 1: accepter demande
          mode == 2: refuser
          mode == 3: on fait rien*/
        if (!strcmp(mode, "1")) {
            res = database.updateFriendRequest(str_buffer, clientName, "A");
            if (res == 0) printf("<%s> et <%s> sont desormais amis\n", clientName, str_buffer);
            else printf("Pas reussi a accepter la demande\n");
        } else if (!strcmp(mode, "2")) {
            res = database.updateFriendRequest(str_buffer, clientName, "D");
            if (res == 0) printf("<%s> a refusé la demande d'ami de <%s>\n", clientName, str_buffer);
        }

    }
    printf("FIN DE LA FONCTION\n");
    return 1;
}
