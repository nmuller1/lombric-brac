//
// Created by Aloïs Glibert (000393192) on 4/03/20 .
//

#ifndef GROUPE_8_SERVER_H
#define GROUPE_8_SERVER_H


#define    _DEFAULT_SOURCE

#include "ProtocolServer.h"
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <iostream>
#include <zconf.h>

void* manage_client (void *data);
void* manage_chat(void *data);

#endif //GROUPE_8_SERVER_H
