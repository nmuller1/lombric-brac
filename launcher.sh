#!/bin/bash

if [ "$1" == "" ]; then
  gnome-terminal -q -e 'bash -c "./Server 4444 ; read line"'
  for ((i = 0; i < 2; i++)); do
    gnome-terminal -q --maximize -t "Joueur $i" -e 'bash -c "./Client 127.0.0.1 4444; read line"'
  done
  exit
fi

[[ "$1" != [0-4] ]] && echo "#client [make] [srv]" && exit 0

if [ "$2" == "make" ]; then
    make -j || exit 0
fi

[ "$3" == "srv" ] && gnome-terminal -q -e 'bash -c "./Server 4444 ; read line"'

for ((i = 0; i < "$1"; i++)); do
  gnome-terminal -q --maximize -t "Joueur $i" -e 'bash -c "./Client 127.0.0.1 4444; read line"'
done
